<div class="modal fade" id="modal-ch">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
                <button style="opacity: .7" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="font-weight:700;color:black;">X</span></button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>

            <div id="modal-body" class="modal-body">

            </div>

            <div class="modal-footer">

            </div>

        </div>

    </div>
    <input type="hidden" id="hdd_mensaje_login" value="${mensaje}" />
</div>


<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- Redirect  -->
<script src="${pageContext.request.contextPath}/jq/jquery.redirect.js" type="text/javascript"></script>
<!-- Keyboard -->
<script src="${pageContext.request.contextPath}/jq/keyboard.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/controlador.js" type="text/javascript"></script>
<script>

    $(document).ready(function () {

        if (check_so() === "iOS") {

            setTimeout(cerrar_ios, 300000);

        } else if (check_so() === "Android") {

            setTimeout(cerrar_android, 300000);

        }

    });

    function cerrar_android() {
        Android.mensaje_tiempo_salida();
    }

    function cerrar_ios() {
        in_cerrar_session("${pageContext.request.contextPath}/LoginServlet?accion=LOGOUT");
    }

    function logout() {

        window.location.href = "${pageContext.request.contextPath}/LoginServlet?accion=LOGOUT";

    }

    //btn borrar

    $(".btn-borrar").click(function () {

        $(this).parent().prev().val('');

    });

    $(".inhabilitado").click(function () {

        message_req('Opcion Inhabilitada', "Esta opci�n no se puede acceder");

    });

    $(".proximamente").click(function () {

        message_req('Proximamente', "Esta opci�n estar� disponible proximamente!");

    });



    //Focus
    $("input").not("input[type='checkbox']").click(function () {
        var $this = $(this),
                $toElement = $this,
                $focusElement = $this,
                $offset = 700,
                $speed = 500;

//        $('html, body').animate({
//            scrollTop: $($toElement).offset().top + $offset
//        }, $speed);


        if ($focusElement) {
            $($focusElement).focus();
        }


    });

    function message_req(titulo, cuerpo) {
        $("#modal-title").text(titulo);
        $("#modal-body").html(cuerpo);

        $("#modal-ch").modal();
    }


    $("body").on("keydown", ".validar-numero-3", function (event) {

        if (this.value.length === 3) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#coordenada_clave_dinamica").blur();

                $("#clave").blur();

                return false;

            }

        }

    });

    $("body").on("keyup", ".validar-numero-3", function (event) {

        if (this.value.length === 3) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#coordenada_clave_dinamica").blur();

                $("#clave").blur();

                return false;

            }

        }

    });


    $("body").on("keydown", ".validar-numero-6", function (event) {

        if (this.value.length === 6) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#coordenada_clave_dinamica").blur();

                $("#clave").blur();

                return false;

            }

        }

    });

    $("body").on("keyup", ".validar-numero-6", function (event) {

        if (this.value.length === 6) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#coordenada_clave_dinamica").blur();

                $("#clave").blur();

                return false;

            }

        }

    });

    $("body").on("keydown", "#ingresar_contacto_telefono", function (event) {

        if (this.value.length === 9) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                return false;

            }

        }

    });

    $("body").on("keyup", "#ingresar_contacto_telefono", function (event) {

        if (this.value.length === 9) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                return false;

            }

        }

    });


    $("body").on("keydown", ".validar-numero-16", function (event) {

        if (this.value.length === 16) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#numero_tarjeta").blur();

                return false;

            }

        }

    });

    $("body").on("keyup", ".validar-numero-16", function (event) {

        if (this.value.length === 16) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#numero_tarjeta").blur();

                return false;

            }

        }

    });

    $("body").on("keydown", ".validar-numero-18", function (event) {

        if (this.value.length === 18) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#cuenta_destino").blur();

                return false;

            }

        }

    });

    $("body").on("keyup", ".validar-numero-18", function (event) {

        if (this.value.length === 18) {

            if (event.keyCode !== 229 && event.keyCode !== 46 && event.keyCode !== 8) {

                $("#cuenta_destino").blur();

                return false;

            }

        }

    });


    $("body").on("keyup", ".salto", function (e) {

        var maximo = 0;

        if ($(this).hasClass('campo-3')) {
            maximo = 3;
        }

        if ($(this).hasClass('campo-10')) {
            maximo = 10;
        }

        if ($(this).hasClass('campo-12')) {
            maximo = 12;
        }

        if ($(this).hasClass('campo-2')) {
            maximo = 2;
        }

        if ($(this).val().length === maximo) {

            $(this).next().focus();

        }

        if ($(this).val().length === 0 && e.keyCode === 8) {

            $anterior = $(this).prev();

            $anterior.focus();

        }

    });






    function funcion_solo_numeros(evt) {
        if (window.event) {//asignamos el valor de la tecla a keynum
            keynum = evt.keyCode; //IE
        } else {
            keynum = evt.which; //FF
        }
        //comprobamos si se encuentra en el rango num�rico y que teclas no recibir�.
        if ((keynum > 47 && keynum < 58) || keynum === 8 || keynum === 13 || keynum === 6) {
            return true;
        } else {
            return false;
        }
    }

    //Ocultar teclado al darle go o done
    $("input").keyup(function (event) {

        if (event.keyCode === 13) {

            document.activeElement.blur();

        }

    });



    function ip_movil() {

        var ip_actual = '';

        if (check_so() === "iOS") {

            ip_mov = IP_DISPOSITIVO_iOS;

            ip_sep = ip_mov.split(".");

            ip_actual = ip_sep[0] + ":" + ip_sep[1] + ":" + ip_sep[2] + ":" + ip_sep[3];

        } else if (check_so() === "Android") {

            ip_mov = Android.get_ip_cell_phone();

            ip_sep = ip_mov.split(".");

            ip_actual = ip_sep[0] + ":" + ip_sep[1] + ":" + ip_sep[2] + ":" + ip_sep[3];

        } else {

            ip_actual = "10:10:10:10";

        }


        if (ip_actual === '' || ip_actual === null) {

            ip_actual = '${IP_iOS}';

        }

        return ip_actual;

    }

    jQuery.fn.forceNumeric = function () {

        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.which || e.keyCode;

                if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                        // numbers   
                        key >= 48 && key <= 57 ||
                        // Numeric keypad
                        key >= 96 && key <= 105 ||
                        // comma, period and minus, . on keypad
                        key === 190 || key === 188 || key === 109 || key === 110 ||
                        // Backspace and Tab and Enter
                        key === 8 || key === 9 || key === 13 ||
                        // Home and End
                        key === 35 || key === 36 ||
                        // left and right arrows
                        key === 37 || key === 39 ||
                        // Del and Ins
                        key === 46 || key === 45)
                    return true;

                return false;
            });
        });

    };

    $(".solo-numeros").forceNumeric();

    function validar_email(email) {

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);

    }

    function crearForm(value) {
        var myform = document.createElement("form");
        myform.action = "cs";
        myform.method = "post";
        var accion = document.createElement("input");
        accion.name = "accion";
        accion.value = value;
        myform.appendChild(accion);
        document.body.appendChild(myform);
        myform.submit();
    }


</script>