<div class="row" style="margin: 20px 8px;">

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10%">

        <img src="${pageContext.request.contextPath}/img/i-tarjeta-configurar.png" alt="" style="max-width: 100px;width: 100%">

    </div>

        
        
        
    <div class="col-xs-12 col-md-12" style="margin-top: 20px">
        <p class="text-center" style="font-weight: 300;font-size: 20px">
            Modifica tu contrase�a
        </p>
    </div>

    <div class="col-xs-12 col-md-12"  style="margin-top: 20px">

        <div class="form-group has-feedback">

            <input  id="numero_tarjeta" maxlength="16" type="tel" class="form-control" placeholder="Ingresa el n�mero de tu tarjeta" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-candado-gray.png">

        </div>

        <div class="form-group has-feedback">

            <input readonly="readonly" maxlength="4" id="clave" type="password" class="form-control keyboard2" placeholder="Ingresa la clave de 4 d�gitos de tu tarjeta" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-candado-gray.png">


        </div>

    </div>



    <div class="col-xs-12 col-md-12"  style="margin-top: 20px">

        <div class="form-group has-feedback">

            <input readonly="readonly" maxlength="6" id="nueva_clave" type="password" class="form-control keyboard3" placeholder="Ingresa una nueva clave de 6 d�gitos" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-candado-gray.png">

        </div>

        <div class="form-group has-feedback">

            <input readonly="readonly" maxlength="6" id="confirmar_clave" type="password" class="form-control keyboard4" placeholder="Confirma tu clave de 6 d�gitos" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-check-gray.png">


        </div>

    </div>

    <div class="col-xs-12 col-md-12  text-center" style="margin-top:60px">

        <button onclick="js_validar_clave();" class="btn btn-block btn-caja tam1 cambiar_pagina" style="border-radius: 20px">Continuar</button>

    </div>


</div>





