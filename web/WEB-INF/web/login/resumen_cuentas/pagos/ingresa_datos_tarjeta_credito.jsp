<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="ingresa_datos_tarjeta_credito/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="ingresa_datos_tarjeta_credito/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />
        
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="ingresa_datos_tarjeta_credito/script.jsp" />


    </body
</html>