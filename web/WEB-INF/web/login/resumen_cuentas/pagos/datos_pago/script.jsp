<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');


        function mon_tip() {

            var tipo_mon = document.getElementById("tipo_mon_ori").value;

            if (tipo_mon === "1")
            {
                $("#dolares").removeClass("btn-elegido");
                $("#soles").addClass("btn-elegido");
                document.getElementById("monto").placeholder = "S/        ";

            } else if (tipo_mon === "2") {
                $("#dolares").addClass("btn-elegido");
                $("#soles").removeClass("btn-elegido");
                document.getElementById("monto").placeholder = "USD       ";

            }

        }
        ;
        mon_tip();


        var parametro = "${inputs_AP04[6]}";

        if (parametro.length !== 0) {
            $("#numero_tarjeta").val(parametro.trim());


            switch ("${inputs_AP04[4]}") {

                case "1":
                    $("#dolares").removeClass("btn-elegido");
                    $("#soles").addClass("btn-elegido");

                    $("#monto").attr("placeholder", "S/");
                    break;

                case "2":
                    $("#soles").removeClass("btn-elegido");
                    $("#monto").attr("placeholder", "USD");
                    $("#dolares").addClass("btn-elegido");
                    break;

            }



        }


    });

    function validar_campos() {
        var bancos = $("#bancos").val();
        var numero_tarjeta = $("#numero_tarjeta").val();
        var Moneda = elegir_moneda();
        var monto = $("#monto").val();

        var result = [];
        var mensaje = "<ul>";
        var flag = true;

        if (bancos.length === 0) {
            mensaje += "<li>Seleccione banco</li>";
            flag = false;
        }
        if (numero_tarjeta.length === 0) {
            mensaje += "<li>Ingrese n�mero tarjeta</li>";
            flag = false;
        }
        if (Moneda.length === 0) {
            mensaje += "<li>Seleccione moneda</li>";
            flag = false;
        }

        if (monto.length === 0) {
            mensaje += "<li>Ingrese monto</li>";
            flag = false;

        } else {
            var reg = /^\d+(?:\.\d{1,2})?$/;
            if (!reg.test(monto)) {
                mensaje += "<li>Ingrese monto correcto</li>";
                flag = false;
            }

        }

        mensaje += "</ul>";
        result = [flag, mensaje];
        return result;

    }
    function js_validar_pago_tarjeta() {

        var array = validar_campos();
        if (array[0] === true) {
            funcion_confirmar_pago_tarjeta();
        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function funcion_confirmar_pago_tarjeta() {
        var array_bancos = $("#bancos").val().split("%%%");
        var banco = array_bancos[0];
        var agencia = array_bancos[1];
        var nombre_banco = $("#bancos option:selected").text();
        var numero_tarjeta = $("#numero_tarjeta").val();
        var moneda = elegir_moneda();
        var monto = $("#monto").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/RegistroPagoCreditoServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'registar_abono',
                banco: banco,
                numero_tarjeta: numero_tarjeta,
                moneda: moneda,
                monto: monto,
                agencia: agencia

            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function elegir_moneda() {
        var codigo_moneda = "";

        if ($("#soles").hasClass("btn-elegido")) {
            codigo_moneda = "1";//conseguir la monedasoles

        } else if ($("#dolares").hasClass("btn-elegido")) {
            codigo_moneda = "2";  //dolares
        } else {
            codigo_moneda = "";
        }
        return codigo_moneda;
    }


</script>