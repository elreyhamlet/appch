<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>
<script>

    $(function () {
        
        $("#cuenta_part_uno").bind("paste", function(e){
            // access the clipboard using the api
            var pastedData = e.originalEvent.clipboardData.getData('text');
            
            if( pastedData.length === 20){
                
                var uno = pastedData.slice(0,3);
                var dos = pastedData.slice(3,6);
                var tres = pastedData.slice(6,18);
                var cuatro = pastedData.slice(18,20);
                
            }
            
            $("#cuenta_part_uno").val(uno);
            $("#cuenta_part_dos").val(dos);
            $("#cuenta_part_tres").val(tres);
            $("#cuenta_part_cuatro").val(cuatro);
            
            
        } );


        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');

        function mon_tip() {

            var tipo_mon = document.getElementById("tipo_mon_ori").value;

            if (tipo_mon === "1")
            {
                $("#dolares").removeClass("btn-elegido");
                $("#soles").addClass("btn-elegido");
                document.getElementById("monto_propia_trans").placeholder = "S/        ";

            } else if (tipo_mon === "2") {
                $("#dolares").addClass("btn-elegido");
                $("#soles").removeClass("btn-elegido");
                document.getElementById("monto_propia_trans").placeholder = "USD       ";

            }

        }
        ;
        mon_tip();
        
        $("body").on("click",".elegir-titular",function(){
            
            if( $(this).attr('id') === "titular_si" ){
                
                $("#titular_no").removeClass("btn-elegido");
                $("#titular_si").addClass("btn-elegido");

            }
            else if ( $(this).attr('id') === "titular_no" ){
                
                $("#titular_no").addClass("btn-elegido");
                $("#titular_si").removeClass("btn-elegido");
                
            }
            
        });
        
        var interbancario = "${inputs_AP03[6]}";

        if (interbancario.length !== 0) {
            var inter = interbancario.trim();
            var char1 = inter.substring(0, 3);
            var char2 = inter.substring(3, 6);
            var char3 = inter.substring(6, 18);
            var char4 = inter.substring(18, 20);

            $("#cuenta_part_uno").val(char1);
            $("#cuenta_part_dos").val(char2);
            $("#cuenta_part_tres").val(char3);
            $("#cuenta_part_cuatro").val(char4);


        }


        $('.onoffswitch-checkbox').on('change', function () {
            var isChecked = $(this).is(':checked');

            console.log('isChecked: ' + isChecked);
            if (isChecked === true) {
                document.getElementById("estado_titular").value = "true";
                $('#ope_frecuente').removeAttr("readonly");
            } else {
                document.getElementById("estado_titular").value = "false";
                $('#ope_frecuente').attr("readonly", "true");
            }

        });

        function setSwitchState(el, flag) {
            el.attr('checked', flag);
        }

        setSwitchState($('.myonoffswitch'), true);
    });
    
    function validarCampos() {
     
        var result = [];
        var mensaje = "<ul>";
        var flag = true;
        monto = document.getElementById("monto_propia_trans").value;
        cci1 = document.getElementById("cuenta_part_uno").value;
        cci2 = document.getElementById("cuenta_part_dos").value;
        cci3 = document.getElementById("cuenta_part_tres").value;
        cci4 = document.getElementById("cuenta_part_cuatro").value;
        
        if (monto.length === 0) {
            mensaje += "<li>Ingrese un monto</li>";
            flag = false;
        }
        if( cci1.length < 3 || cci2.length < 3 || cci3.length < 12 || cci4.length < 2 ){
            
            mensaje += "<li>Ingrese cci v�lido</li>";
            flag = false;
            
        }
        if( !$("#titular_si").hasClass("btn-elegido") && !$("#titular_no").hasClass("btn-elegido") ){
        
            mensaje += "<li>Escoja si es titular de la cuenta Destino </li>";
            flag = false;
            
        }
        
        
        mensaje += "</ul>";
        result = [flag, mensaje];
        return result;
    }


    function js_confir_tran_otro_banco() {

//        var estado_titular = document.getElementById("estado_titular").value;

        var estado_titular = false;
        
        if( $("#titular_si").hasClass("btn-elegido") ){
        
            estado_titular = true;
            
        }
        if( $("#titular_no").hasClass("btn-elegido") ){
            
            estado_titular = false;
            
        }
        
        var monto = document.getElementById("monto_propia_trans").value;
        var tipo_mon_ori = document.getElementById("tipo_mon_ori").value;

        var cuenta_part_uno = document.getElementById("cuenta_part_uno").value;
        var cuenta_part_dos = document.getElementById("cuenta_part_dos").value;
        var cuenta_part_tres = document.getElementById("cuenta_part_tres").value;
        var cuenta_part_cuatro = document.getElementById("cuenta_part_cuatro").value;

        var cuenta_destino_interbancaria = cuenta_part_uno.concat(cuenta_part_dos).concat(cuenta_part_tres).concat(cuenta_part_cuatro);
        
        var validar = validarCampos();
        
        if(validar[0] === false){
            
            message_req("Mensaje", validar[1]);
            return;
        }

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaOtroBancoServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'registrar_abono',
                tipo_mon_ori: tipo_mon_ori,
                monto: monto,
                cuenta_destino_interbancaria: cuenta_destino_interbancaria,
                estado_titular: estado_titular

            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                         crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });
    }

    function cancelar_transferencia() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>