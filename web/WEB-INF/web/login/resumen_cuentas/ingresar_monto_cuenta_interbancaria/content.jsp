<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
                Solicitud de Transferencia Interbancaria

            </p>
            <p class="text-center negrita">Ingrese el monto y el CCI de la cuenta</p>

        </div>
    </div>

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10px">

        <span class="negrita">Moneda</span><br>
        <div class="btn-group" role="group" >



            <button onclick="" id="soles" type="button" class="btn btn-secondary elegir-moneda" > Soles</button>

            <button onclick="" id="dolares" type="button" class="btn btn-secondary elegir-moneda" > Dolares </button>


        </div>
        <p class="text-center" style="font-size: 12px;margin-bottom:5px">Tipo de Cambio: Compra <span>${tipo_cambio[0]}</span> Venta <span>${tipo_cambio[1]}</span></p>

    </div>


    <div class="col-xs-12 text-center">

        <span class="negrita">Monto</span> <br>

        <div class="input-group">
            <input type="hidden" id="codigoMoneda" value="S/">
            <input type="hidden" id="tipo_mon_ori" value='${tipo_mon_ori}'>
            <input type="tel" class="form-control  solo-numeros" placeholder="S/" id="monto_propia_trans">

            <div class="input-group-btn">   
                <button class="btn btn-borrar" type="submit">
                    <i style="color:#CC3333" class="fas fa-times-circle"></i>
                </button>
            </div>
        </div>

    </div>


    <div class="col-xs-12 text-center">
        
        <span class="negrita">Ingrese cuenta interbancaria de destino</span> <br>

        <div class="form-group" style="margin-bottom:0px">

            <input id="cuenta_part_uno" type="tel" class="solo-numeros form-control campo-3 salto text-center" maxlength="3" size="5" style="width:auto;display:inline-block;padding:6px;border-radius: 4px 0px 0px 4px">
            <input id="cuenta_part_dos" type="tel" class="solo-numeros form-control campo-3 salto text-center" maxlength="3" size="5" style="width:auto;display:inline-block;padding:6px;margin-left:-5px;border-radius:0px">
            <input id="cuenta_part_tres" type="tel" class="solo-numeros form-control campo-12 salto text-center" maxlength="14" size="18" style="width:auto;display:inline-block;padding:6px;margin-left:-6px;border-radius:0px">
            <input id="cuenta_part_cuatro" type="tel" class="solo-numeros form-control campo-2 salto text-center" maxlength="2" size="4" style="width:auto;display:inline-block;padding:6px;margin-left:-5px;border-radius: 0px 4px 4px 0px;">

        </div>

        <div style="margin-bottom:10px">
            <span> �Es titular de la cuenta destino? </span> 
<!--            <span style="display:inline-block;height: 16px;padding-left: 10px;">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                    <input type="hidden" id="estado_titular" value="true">
                </div>
            </span>-->
            <br>
            <div class="btn-group" role="group" >
                <button onclick="" id="titular_si" type="button" class="btn elegir-moneda elegir-titular" > Si</button>
                <button onclick="" id="titular_no" type="button" class="btn elegir-moneda elegir-titular" > No</button>
            </div>

        </div>


    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a href="transferencias.jsp" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a onclick="js_confir_tran_otro_banco();" class="btn btn-caja btn-block">Siguiente</a>
        </div>

    </div>


</div>

