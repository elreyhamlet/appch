<div class="row" style="margin: 0px;padding: 10px;margin-top: 100px">

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja text-center" style="padding: 8px; margin-bottom: 0px">

            <p style="font-size: 18px;text-align: center;margin-bottom: 0px;font-weight: 600"> 
                <img class="pull-left" src="${pageContext.request.contextPath}/img/i-tarjeta-check.png" style="width: 30px"> 
                CUENTA DE ${descripcion_credito}
                
                <a onclick="elegir_cuenta_cargo('${lista_credito_cta[0][5]}');"><img class="pull-right" src="${pageContext.request.contextPath}/img/i-right.png" style="width: 20px">
                </a>
            </p>

            <p>

                <span class="text-center" style="font-size: 12px">Cuenta: <span>${lista_detalle_credito_cta[0]}</span></span> 
                <br>
                <span class="text-center" style="font-size: 12px">Saldo Pendiente: <span class="formato_monto">${lista_detalle_credito_cta[1]}</span></span>

            </p>

            <p><span class="sombreado">Fecha de pago: <span class="sombreado">${lista_detalle_credito_cta[2]}</span></span> <span class="sombreado">Cuota: <span class="sombreado formato_monto">${lista_detalle_credito_cta[3]}</span></span></p>


        </div>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <div class="col-xs-8 nopad">
                <span class="sombreado">Tipo de cuota</span>
            </div>
            <div class="col-xs-4 nopad text-center">
                <span>${lista_detalle_credito_cta[4]}</span>
            </div>

        </div>

        <div class="col-xs-12 item-list-caja" >

            <div class="col-xs-8 nopad">
                <span class="sombreado">Fecha de desembolso</span>
            </div>
            <div class="col-xs-4 nopad text-center">
                <span>${lista_detalle_credito_cta[5]}</span>
            </div>

        </div>

        <div class="col-xs-12 item-list-caja" >

            <div class="col-xs-8 nopad">
                <span class="sombreado">Pago pendiente</span>
            </div>
            <div class="col-xs-4 nopad text-center">
                <span class="formato_monto" >${lista_detalle_credito_cta[3]}</span>
            </div>

        </div>

        <div class="col-xs-12 item-list-caja" >

            <div class="col-xs-8 nopad">
                <span class="sombreado">Cuota Pendiente</span>
            </div>
            <div class="col-xs-4 nopad text-center">
                <span>${lista_detalle_credito_cta[6]}</span>
            </div>

        </div>







    </div>






</div>