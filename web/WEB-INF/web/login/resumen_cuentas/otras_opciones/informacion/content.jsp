<div class="row cuerpo">
    <%session.setAttribute("origen", "login");%>

    <div class="col-xs-12 col-md-12" style="margin-top: 20px">
        <p class="text-center" style="font-weight: 900;font-size: 20px">
            Información
        </p>
    </div>

    <div class="col-xs-12 col-md-12"  >


        <a href="${pageContext.request.contextPath}/PreguntasFrecuentesServlet?accion=listar_preguntas">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span>Preguntas frecuentes</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>

        </a>

        <a href="${pageContext.request.contextPath}/PreguntasFrecuentesServlet?accion=listar_redes_sociales">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span>Novedades</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>


        </a>

        <div id="terminos_condiciones_app">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span>Términos y Condiciones</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>


        </div>





    </div>

</div>

<div class="modal fade" id="terminos_condiciones_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">
            
            <div class="modal-header" >
                <p style="text-align: center;"><strong><span style="font-family: 'Arial Narrow',sans-serif;font-weight:700">T&Eacute;RMINOS Y CONDICIONES DE USO Y PRIVACIDAD</span></strong></p>
            </div>

            <div class="modal-body" style="overflow-y:scroll;height:400px">

                <p align="center">

                
                <p style="text-align: justify;"><strong><span style="font-family: 'Arial Narrow',sans-serif;font-weight:600">AVISO LEGAL</span></strong></p>
                <p style="text-align: justify;"><span style="font-family: 'Arial Narrow',sans-serif;">Las presentes condiciones regulan el uso de los servicios ofrecidos por el CAJA HUANCAYO (en adelante &ldquo;LA CAJA&rdquo;) </span><span style="font-family: 'Arial Narrow',sans-serif;">a trav&eacute;s de su aplicativo m&oacute;vil (en adelante el &ldquo;App&rdquo;). Quien use el App adquiere la condici&oacute;n de usuario del mismo y se entiende que ha le&iacute;do, conoce y acepta los t&eacute;rminos que aqu&iacute; se describen (EL CLIENTE). LA CAJA podr&aacute; hacer modificaciones a estos t&eacute;rminos y con su publicaci&oacute;n en el App se entender&aacute;n efectivas.</span></p>
                <p style="margin-right: .75pt; text-align: justify;"><strong><span style="font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;font-weight:600">CONDICIONES GENERALES DE AFILIACI&Oacute;N</span></strong></p>
                <ol>
                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">El CLIENTE afilia su Tarjeta de D&eacute;bito al App para efectuar las operaciones definidas por LA CAJA, las cuales ser&aacute;n comunicadas a trav&eacute;s de su p&aacute;gina web.</span></li>
                </ol>
                <p style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">&nbsp;</span></p>
                <ol start="2">
                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">El CLIENTE declara conocer que para acceder al Servicio deber&aacute; contar con una Tarjeta de D&eacute;bito activa y un tel&eacute;fono inteligente con una tecnolog&iacute;a que permita la instalaci&oacute;n del App, para realizar operaciones en el App se debe seguir las instrucciones del mismo.</span></li>
                </ol>
                <p style="text-align: justify; margin: 0cm 0cm .0001pt 36.0pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">&nbsp;</span></p>
                <ol start="3">
                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">Una vez que LA CAJA haya procesado la operaci&oacute;n solicitada por el CLIENTE a trav&eacute;s del Servicio, LA CAJA le enviar&aacute; una comunicaci&oacute;n electr&oacute;nica confirmando la operaci&oacute;n, la cual se considerar&aacute; efectuada, v&aacute;lida y aceptada por el CLIENTE. El CLIENTE es responsable de todas las operaciones que se realicen mediante su tel&eacute;fono inteligente.&nbsp;&nbsp;</span></li>
                </ol>
                <p style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">&nbsp;</span></p>
                <ol start="4">
                    <li style="text-align: justify; margin: 5.0pt .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">El CLIENTE declara conocer y acepta que podr&aacute; optar por grabar: (i) el n&uacute;mero de su Tarjeta de D&eacute;bito u otra informaci&oacute;n en la pantalla de acceso del App, y (ii) informaci&oacute;n sobre cuentas propias o de terceros y otros datos, en cuyo caso no ser&aacute; necesario que digite su doble mecanismo de autenticaci&oacute;n, el Touch ID&nbsp;como mecanismo de autenticaci&oacute;n para acceder al App&nbsp;u otros tipos de firmas electr&oacute;nicas, asumiendo el CLIENTE plena responsabilidad por las transacciones realizadas de esta manera, salvo que hubiese mediado dolo o culpa inexcusable de LA CAJA.&nbsp;&nbsp;Asimismo, el CLIENTE declara conocer y acepta que el App tendr&aacute; acceso a su agenda de contactos grabada en el tel&eacute;fono inteligente, para facilitarle el uso de la informaci&oacute;n ah&iacute; contenida durante la realizaci&oacute;n de operaciones a trav&eacute;s del Servicio y acceder a la</span> <span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">ubicaci&oacute;n del dispositivo m&oacute;vil</span><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">para que el App pueda acceder a un sistema de localizaci&oacute;n de agencias, u otros que se puedan implementar.</span></li>

                </ol>
                <p style="margin-right: .75pt; text-align: justify;"><span style="font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">&nbsp;</span></p>

                <ol start="5">

                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black; letter-spacing: -.3pt;">Siempre que no haya mediado dolo o culpa inexcusable LA CAJA, este no asume ninguna responsabilidad por cualquier problema o inconveniente imputable a terceros, de &iacute;ndole t&eacute;cnico, f&iacute;sico, caso fortuito o fuerza mayor que imposibilite, retrase, demore la ejecuci&oacute;n, o no permita la realizaci&oacute;n exitosa del Servicio.</span></li>

                </ol>
                <ol start="6">
                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black;"><span style="letter-spacing: -.3pt;">El CLIENTE declara que el uso del Servicio podr&iacute;a estar sujeto al pago de comisiones, las cuales ser&aacute;n comunicadas a trav&eacute;s de la p&aacute;gina web de LA CAJA.</span></span></li>
                </ol>
                <p style="text-align: justify; margin: 0cm 0cm .0001pt 36.0pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;</span></p>
                <ol start="7">
                    <li style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black;">El CLIENTE podr&aacute; desvincularse de los Servicios y Contenido desinstalando el App sin costo alguno. LA CAJA no se responsabiliza de las consecuencias de dicha desinstalaci&oacute;n.</span></li>
                </ol>
                <p style="text-align: justify;margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;</span></p>
                <p style="margin-bottom: 10.0pt; text-align: justify; line-height: 12.65pt;font-weight:600"><strong><span style="font-family: 'Arial Narrow',sans-serif; color: black;font-weight:600">PROTECCI&Oacute;N DE DATOS PERSONALES</span></strong></p>
                <p style="text-align: justify; margin: 0cm .75pt .0001pt 21.3pt;"><span style="font-size: 11.0pt; font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;PROTECCI&Oacute;N DE DATOS PERSONALES: LA CAJA informa al CLIENTE que, de acuerdo a la Ley N&deg; 29733 - Ley de Protecci&oacute;n de Datos Personales, su Reglamento aprobado mediante Decreto Supremo N&deg; 003-2013-JUS y las dem&aacute;s disposiciones complementarias, LA CAJA est&aacute; legalmente autorizado para tratar la informaci&oacute;n que el CLIENTE le entrega sobre su situaci&oacute;n personal, financiera y crediticia (la &ldquo;<u>Informaci&oacute;n</u>&rdquo;) con la finalidad de ejecutar la relaci&oacute;n contractual que origina este contrato. Asimismo, LA CAJA informa al CLIENTE que para dar cumplimiento a las obligaciones y/o requerimientos que se generen en virtud de las normas vigentes en el ordenamiento jur&iacute;dico peruano y/o en normas internacionales que le sean aplicables, incluyendo pero sin limitarse a las vinculadas al sistema de prevenci&oacute;n de lavado de activos y financiamiento del terrorismo y normas prudenciales, LA CAJA podr&aacute; dar tratamiento y eventualmente transferir su Informaci&oacute;n a autoridades y terceros autorizados por ley.</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">El CLIENTE reconoce que estar&aacute;n incluidos dentro de su Informaci&oacute;n todos aquellos datos, operaciones y referencias a los que LA CAJA pudiera acceder en el curso normal de sus operaciones, ya sea por haber sido proporcionados por el CLIENTE o por terceros o por haber sido desarrollados por LA CAJA, tanto en forma f&iacute;sica, oral o electr&oacute;nica y que pudieran calificar como &ldquo;Datos Personales&rdquo; conforme a la legislaci&oacute;n de la materia.</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">En virtud de lo se&ntilde;alado, el CLIENTE autoriza expresamente a LA CAJA a incorporar su Informaci&oacute;n al banco de datos personales de usuarios de responsabilidad de LA CAJA, almacenar, dar tratamiento, procesar y transferir su Informaci&oacute;n a sus subsidiarias y afiliadas conforme a los procedimientos que LA CAJA determine en el marco de sus operaciones habituales.</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">Asimismo, el CLIENTE autoriza a LA CAJA a utilizar su Informaci&oacute;n a efectos de: (i) ofrecerle, a trav&eacute;s de cualquier medio escrito, verbal, electr&oacute;nico y/o inform&aacute;tico, cualquiera de los productos o servicios del activo que LA CAJA brinda, incluyendo pero sin estar limitado a cr&eacute;ditos directos e indirectos, tarjetas de cr&eacute;dito y otras l&iacute;neas de cr&eacute;dito, (ii) ofrecerle, a trav&eacute;s de cualquier medio escrito, verbal, electr&oacute;nico y/o inform&aacute;tico, cualquiera de los productos o servicios del pasivo que LA CAJA brinda, incluyendo pero sin estar limitado a cuentas corrientes, cuentas de ahorro, cuentas CTS y dep&oacute;sitos a plazo; (iii) ofrecerle, a trav&eacute;s de cualquier medio escrito, verbal, electr&oacute;nico y/o inform&aacute;tico, cualquier otro producto o servicio de LA CAJA.</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">&nbsp;</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">El CLIENTE puede ejercer sus derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n, siempre que cumpla con los requisitos exigidos por las normas aplicables, dirigi&eacute;ndose a LACAJA de forma presencial en cualquiera de sus oficinas a nivel nacional en el horario establecido para la atenci&oacute;n al p&uacute;blico.</span></p>
                <p style="text-align: justify; margin: 0cm .75pt 8.0pt 21.3pt;"><span style="font-family: 'Arial Narrow',sans-serif; color: black;">El CLIENTE se obliga a mantener permanentemente actualizada su Informaci&oacute;n durante la vigencia de este contrato, especialmente en cuanto se refiere a su nacionalidad, lugar de residencia, situaci&oacute;n fiscal o composici&oacute;n accionaria de ser el caso.</span></p>
                <p style="text-align: justify;padding-top:10px"><strong><span style="font-family: 'Arial Narrow',sans-serif;font-weight:600">JURISDICCI&Oacute;N Y LEY APLICABLE</span></strong></p>
                <p style="text-align: justify;"><span style="font-family: 'Arial Narrow',sans-serif;">Los t&eacute;rminos y condiciones se regir&aacute;n por la legislaci&oacute;n peruana. En caso de cualquier controversia entre LA CAJA y EL CLIENTE, ambos se someter&aacute;n a los Juzgados y Tribunales del Distrito Judicial del Per&uacute;, renunciado expresamente al fuero o jurisdicci&oacute;n de sus domicilios.</span></p>

                </p>


            </div>

            <div class="modal-footer">

                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-caja btn-block" data-dismiss="modal" >Cerrar</button>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>

    </div>
</div>