<div class="row cuerpo">
    <%session.setAttribute("origen", "login");%>

    <div class="col-xs-12 col-md-12" style="margin-top: 20px">
        <p class="text-center titulo-negrita" >
            Servicio al cliente
        </p>
    </div>

    <div class="col-xs-12 col-md-12"  >


        <a  href="${pageContext.request.contextPath}/UbicanosServlet?accion=listar_datos_mapa">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span class="pull-left" style="padding-left:5px"><img class="gris" style="height: 15px" src="${pageContext.request.contextPath}/img/i-mapa.png"></span>
                <span class="negrita">Ubícanos</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>

        </a>

        <a href="${pageContext.request.contextPath}/web/menu_publico/contactanos.jsp">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span class="pull-left" ><img style="height: 15px" class="gris" src="${pageContext.request.contextPath}/img/i-auricular.png"></span>
                <span class="negrita">Contáctanos</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>


        </a>


        <a onclick="bloquear_tarjeta();">

            <div class="form-group text-center " style="border:1px solid #808080;border-radius: 10px;padding:10px">

                <span class="pull-left"><img style="height: 15px" class="gris" src="${pageContext.request.contextPath}/img/i-tarjeta-bloquear.png"></span>
                <span class="negrita">Bloquea tu tarjeta</span>
                <span class="pull-right"><img style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png"></span>

            </div>

        </a>



    </div>

</div>