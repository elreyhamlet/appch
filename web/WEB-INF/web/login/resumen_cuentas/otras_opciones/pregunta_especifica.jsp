<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="pregunta_especifica/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="pregunta_especifica/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="pregunta_especifica/script.jsp" />


    </body
</html>