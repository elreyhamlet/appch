<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');
        var correo = "${correo_electronico}";
        if (correo.length === 0)
        {
            $("#correo").attr("disabled", false);
        } else {
            $("#correo").attr("disabled", true);
        }

    });

    alto_pantalla();

    function alto_pantalla() {


        var alto = $(window).height() * 0.5;

        $("#contenedor_texto").height(alto);

    }




    function editar_correo() {

        var correo = "${correo_electronico}";
        if (correo.length !== 0)
        {
            $("#correo").attr("disabled", false);
        }
    }




    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var correo = $("#correo").val();
        var coordenada_clave_dinamica = $("#coordenada_clave_dinamica").val();

        var validar_correo = validar_email(correo);

        var flag = true;
        if (correo.length === 0) {

            result += "<li>Ingrese correo</li>";
            flag = false;
        }

        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {

            if (coordenada_clave_dinamica.length === 0) {

                result += "<li>${mensajeclave}</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada_clave_dinamica.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }

        if (!validar_correo) {

            flag = false;
            result += "<li> Ingrese un correo valido </li>";

        }

        result += "</ul>";
        array = [flag, result];

        return array;
    }
    function registrar() {


        var array = validar_campos();
        if (array[0] === true) {
            funcion_ajax_registrar();

        } else {

            message_req("Mensaje", array[1]);
        }
    }


    function funcion_ajax_registrar() {


        var coordenada = $("#coordenada_clave_dinamica").val();
        var correo = $("#correo").val();
        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar',
                coordenada: coordenada,
                correo: correo,
                ip: ip_movil()

            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }


    function validar_email(email) {

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);

    }

</script>