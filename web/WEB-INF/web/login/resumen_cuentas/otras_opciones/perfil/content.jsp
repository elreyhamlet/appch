<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">

    <%session.setAttribute("origen", "mas");%>

    <div class="col-xs-12 col-md-12">

        <div class="form-group text-center">

            <span class="titulo-negrita">Configura tu perfil</span>

        </div>

    </div>

    <div class="col-xs-2 nopad">

        <span class="negrita" style="line-height:35px">E-mail: </span>

    </div>

    <div class="col-xs-7 nopad" >

        <div class="form-group">

            <input id="correo" type="text" class="form-control" value="${correo_electronico}">

        </div>

    </div>

    <div class="col-xs-3 nopad text-center">

        <button onclick="editar_correo();" class="btn btn-caja">Editar</button>

    </div>




    <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center" style="border-radius:10px;border:solid 1px #C0C7C8;padding:5px;margin-bottom:15px">

        <span> Se envian las constancias de operaci�n a este e-mail registrado </span>

    </div>


<!--    <div class="col-xs-12 col-md-12 text-center">

        <div class="form-group">

            <span>He le�do el tratamiento de datos personales </span>
            <span style="display:inline-block;height: 16px;">
                <div class="onoffswitch" style="top:8px">
                    <input  type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </span>

        </div>

    </div>-->

    <div class="col-xs-12 col-md-12 text-center">

        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">
            <div class="form-group">
                <span>${mensajeclave}</span><br>
                <input id="coordenada_clave_dinamica" type="text" class="form-control">

            </div>
        </c:if>  
    </div>



    <div class="clearfix"></div>

    <div class="col-xs-12 col-md-12">

        <button data-toggle="modal" data-target="#mocal_arco" class="btn btn-caja btn-block"> Guardar </button>

    </div>



</div>

<div class="modal fade" id="mocal_arco" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">
            
            <div class="modal-header">
                
                <p style="margin-bottom: .0001pt; text-align: center"><strong><u><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;font-weight:600">CONSENTIMIENTO PARA EL TRATAMIENTO DE DATOS PERSONALES</span></u></strong></p>
                
            </div>

            <div class="modal-body">

                <div id="contenedor_texto" style="height: 80%;overflow-y: scroll">

                    <p style="margin-bottom: .0001pt; text-align: justify;"><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;">En pleno ejercicio de mis facultades <strong>DECLARO</strong> haber sido informado sobre la Ley de Protecci&oacute;n de Datos Personales, Ley N&deg; 29733, su Reglamento aprobado mediante D.S. 003-2013-JUS y sus alcances, por lo que:</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify;"><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify;"><strong><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;">OTORGO MI CONSENTIMIENTO</span></strong><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;"> de forma voluntaria, previa, libre, expresa, informada e inequ&iacute;voca para que mis datos personales y/o sensible sobre nombre, apellido, documento de identidad, mis datos biom&eacute;tricos (huella dactilar), nacionalidad, estado civil, domicilio, ubicaci&oacute;n geogr&aacute;fica, correo electr&oacute;nico, tel&eacute;fono, ocupaci&oacute;n, actividades que realizo y otros referidos a mis ingresos econ&oacute;micos, estudios, gastos y otros relacionados a mis egresos econ&oacute;micos, estado de salud, bienes y otros sobre mi patrimonio, para que sean tratados por la <strong>CMAC HUANCAYO S.A.</strong>, dentro de los alcances de la Ley N&deg; 29733 y su Reglamento.</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify;"><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify; line-height: normal; text-autospace: none;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">Con &eacute;ste consentimiento permito que <strong>CMAC HUANCAYO S.A.</strong> trate mis datos personales para: (i) conservarlos, almacenarlos y procesarlos en su banco de datos, conforme a las formas previstas por Ley, (ii) enviarme ofertas comerciales, publicidad e informaci&oacute;n en general de los productos y/o servicios de <strong>CMAC HUANCAYO S.A.</strong> y/o de terceros vinculados o no<strong>*</strong>, (por ejemplo cuentas de ahorros, pr&eacute;stamos, campa&ntilde;as, convenios de financiamiento para adquirir inmuebles, veh&iacute;culos, bienes ecol&oacute;gicos y otros), lo que pueden hacer tambi&eacute;n a trav&eacute;s de terceras personas<strong>*</strong>, (iii) evaluar mi comportamiento en el sistema financiero y capacidad de pago (iv) que puedan decidir si se me otorga el(los) producto(s) y/o servicio(s) que solicite, (v) realizar el an&aacute;lisis de perfiles y fines estad&iacute;sticos e hist&oacute;ricos, (vi) transfiera esta informaci&oacute;n a terceros<strong>*</strong>, vinculados o no a <strong>CMAC HUANCAYO S.A.</strong> dentro del territorio nacional (por ejemplo: empresas de mensajer&iacute;a, auditor&iacute;a, empresas recuperadoras de cr&eacute;ditos), para la ejecuci&oacute;n de los servicios prestados u ofrecidos por la <strong>CMAC HUANCAYO S.A.</strong> y para los fines autorizados en el presente documento, (vii) para que me llamen y realicen preguntas referidas a la calidad de servicio y/o de riesgo reputacional. &nbsp;&nbsp;</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify; line-height: normal; text-autospace: none;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
                    <p style="margin-bottom: .0001pt; text-align: justify; line-height: normal; text-autospace: none;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">Esta autorizaci&oacute;n estar&aacute; vigente inclusive despu&eacute;s del vencimiento de las operaciones y/o de las relaciones contractuales y/o comerciales que mantenga o pudiera mantener con <strong>CMAC HUANCAYO S.A.</strong>, para los fines autorizados en el presente documento. En ese sentido, declaro haber sido informado de que en caso no otorgue este consentimiento, mis datos personales solo ser&aacute;n utilizadas (tratadas) para la ejecuci&oacute;n (desarrollo) y cumplimiento de las relaciones contractuales y/o uso de los servicios financieros que mantenga con <strong>CMAC HUANCAYO S.A.</strong></span></p>
                    <p style="margin-bottom: .0001pt; line-height: normal; text-autospace: none;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
                    <p style="text-align: justify; line-height: normal; margin: 0cm -.05pt .0001pt 0cm;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">Por &uacute;ltimo, <strong>DECLARO, </strong>haber sido informado que en cualquier momento puedo ejercer mi derecho de acceso, informaci&oacute;n, actualizaci&oacute;n, rectificaci&oacute;n, oposici&oacute;n o cancelaci&oacute;n de datos personales, descargando el formato de solicitud de derechos ARCO desde la p&aacute;gina web institucional de <strong>CMAC HUANCAYO S.A.</strong> y envi&aacute;ndolo a la direcci&oacute;n de correo electr&oacute;nico </span><a href="mailto:arco@cajahuancayo.com.pe"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">arco@cajahuancayo.com.pe</span></a><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;"> con el asunto &ldquo;Derechos ARCO&rdquo; o de forma presencial a trav&eacute;s de sus agencias a nivel nacional. </span></p>
                    <p style="text-align: justify; line-height: normal; margin: 0cm -.05pt .0001pt 0cm;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">Finalmente me informaron que puedo revocar &eacute;ste consentimiento, presentando una solicitud escrita en cualquiera de las oficinas de CMAC Huancayo S.A.</span></p>
                    <p style="text-align: justify; line-height: normal; margin: 0cm -.05pt .0001pt 0cm;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
                    <p style="text-align: justify; line-height: normal; margin: 0cm -.05pt .0001pt 0cm;"><span style="font-size: 10.0pt; font-family: 'Arial',sans-serif;">* Los cu&aacute;les podr&aacute; visualizarse en la p&aacute;gina web de la Caja Huancayo, www.cajahuancayo.com.pe </span></p>

                </div>

            </div>

            <div class="modal-footer">

                <div class="col-xs-6">

                    <button type="button" class="btn btn-caja btn-block" data-dismiss="modal"> No Acepto </button>

                </div>

                <div class="col-xs-6">

                    <button id="btn-confirmar" onclick="registrar();" class="btn btn-caja btn-block"> Acepto </button>

                </div>

            </div>
        </div>

    </div>
</div>