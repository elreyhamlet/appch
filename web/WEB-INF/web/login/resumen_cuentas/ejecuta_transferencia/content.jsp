<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
                Transferencia a Cuentas Propias

            </p>
            <p class="text-center negrita">Ingrese Monto  </p>

        </div>
    </div>

    <span></span>



    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10px">




        <span class="negrita">Moneda</span><br>

        <div class="btn-group" role="group" >

    
            <button onclick="" id="soles" type="button" class="btn btn-secondary elegir-moneda" > Soles</button>

       
            <button onclick="" id="dolares" type="button" class="btn btn-secondary elegir-moneda" > Dolares </button>


        </div>
        <p class="text-center" style="font-size: 12px">Tipo de Cambio: Compra <span>${tipo_cambio[0]}</span> Venta <span>${tipo_cambio[1]}</span></p>


    </div>


    <div class="col-xs-12 text-center">

        <span class="negrita">Monto</span> <br>



        <div class="input-group">
            <input type="hidden" id="codigo_moneda" value="">
            <input type="hidden" id="tipo_mon_ori" value='${tipo_mon_ori}'>
            <input type='${type}' style="border-right:none;" class="form-control solo-numeros" placeholder="S/" id="monto_propia_trans">
            <div class="input-group-btn">
                <button class="btn btn-borrar" type="submit">
                    <i style="color:#CC3333" class="fas fa-times-circle"></i>
                </button>
            </div>
        </div>

    </div>    



    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="menu_transferencia();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a onclick="js_confir_tran_propias();" class="btn btn-caja btn-block cambiar_pagina" >Siguiente</a>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />


