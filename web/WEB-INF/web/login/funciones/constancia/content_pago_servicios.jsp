<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia Pago de servicios

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">

            <span style="font-weight: 900">C�digo de operaci�n: </span>${constancia_pago_servicio[0]}   <span style="font-weight: 900">Fecha: </span>${constancia_pago_servicio[1]}   <span style="font-weight: 900">Hora: </span>${constancia_pago_servicio[2]}
        </div>
        <div class="col-xs-12 box box-caja" style="padding: 20px">



            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta cargo: </span>${constancia_pago_servicio[3]}</p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Empresa: </span>${constancia_pago_servicio[4]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Datos: </span>${constancia_pago_servicio[5]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Moneda y Monto: </span>  <span id="monedamonto"> ${constancia_pago_servicio[6]} ${constancia_pago_servicio[7]} </span> </p>

            </div>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>
    
    $(function(){
       
        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
        
        $("#monedamonto").text(${pago_servicio_compartir}.monedamonto);
        
    });

    function compartir() {


        var ary = ${pago_servicio_compartir};
        var texto = "Constancia de Pago de servicios" + "\n";
        texto += "-------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta cargo :" + " " + ary.cuenta + "\n";
        texto += "Empresa :" + " " + ary.empresa + "\n";
        texto += "Datos :" + " " + ary.datos + "\n";
        texto += "Moneda y monto :" + " " + ary.monedamonto + "\n";

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }
    }

</script>