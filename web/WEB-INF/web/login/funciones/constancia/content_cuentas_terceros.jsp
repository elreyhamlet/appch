<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Constancia

        </p>
        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            TRANSFERENCIA EXITOSA

        </p>

        <p><span class="text-center" style="font-weight: 500">Codigo de Operaci�n: ${lista_contancia[0]}&nbsp;  Fecha: ${lista_contancia[1]}&nbsp; Hora: ${lista_contancia[2]}</span></p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Origen: </span> ${lista_contancia[3]}</p>
                <a href="../constancia.jsp"></a>
                <!--<p><span style="font-weight: 900">Tipo de operaci�n: </span> Cambio de clave web</p>-->

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Destino: </span>${lista_contancia[4]}</p>
                <p>${lista_contancia[5]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Moneda y Monto: </span> <span id="monedamonto">${lista_contancia[6]} ${lista_contancia[7]} </span> </p>

            </div>



            <p style="font-weight: 900">CAJA HUANCAYO-... �Tu mejor opci�n financiera</p>
            <p style="font-weight: 900">${constancia_mensajeCmac}</p>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function(){
       
        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');
        
        $("#monedamonto").text(${compartir_cuenta_tceros}.monedamonto);
    });


    function compartir() {


        var ary = ${compartir_cuenta_tceros};

        var texto = "Constancia Cuentas terceros" + "\n";
        texto += "------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta origen :" + " " + ary.cuenta + "\n";
        texto += "Cuenta destino :" + " " + ary.destino + "\n";
        texto += "Nombre  :" + " " + ary.datos + "\n";
        texto += "Moneda y monto :" + " " + ary.monedamonto + "\n";


        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }

</script>