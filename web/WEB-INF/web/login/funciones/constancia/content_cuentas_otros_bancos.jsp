<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia

        </p>
        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Solicitud de Transferencia Interbancaria
        </p>

        <p><span class="text-center" style="font-weight: 500">Codigo de Operaci�n: ${lista_contancia[0]}&nbsp;  Fecha: ${lista_contancia[1]}&nbsp; Hora: ${lista_contancia[2]}</span></p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Origen: </span> ${lista_contancia[3]}</p>
                


            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Destino: </span>${lista_contancia[4]}</p>
                <p>${lista_contancia[5]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Moneda y Monto: </span> <span id="monedamonto">${lista_contancia[6]} ${lista_contancia[7]}</span></p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Comisi�n Interbancaria: </span> ${lista_contancia[6]} ${lista_contancia[8]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Comisi�n Cliente: </span> ${lista_contancia[6]} ${lista_contancia[9]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Comisi�n de Confirmaci�n de Abono: </span> ${lista_contancia[6]} ${lista_contancia[11]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">ITF: </span> ${lista_contancia[6]} ${lista_contancia[13]}</p>

            </div>

            



            <div class="form-group">

                <p><span style="font-weight: 900">Total: </span> ${lista_contancia[6]} ${lista_contancia[10]}</p>

            </div>
                
            <div class="form-group">

                <p><span style="font-weight: 900">Codigo de Solicitud: </span> ${lista_contancia[12]}</p>

            </div>

            <p style="font-weight: 900">CAJA HUANCAYO-... �Tu mejor opci�n financiera</p>
            <p style="font-weight: 900"></p>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button  onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');

        $("#monedamonto").text(${compartir_cuenta_otros}.monedamonto);
    });

    function compartir() {

        var ary = ${compartir_cuenta_otros};

        var texto = "Constancia Solicitud Transferencia a Otros bancos" + "\n";
        texto += "------------------- " + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta Origen :" + " " + ary.cuenta + "\n";
        texto += "Cuenta Destino :" + " " + ary.destino + "\n";
        texto += "Entidad :" + " " + ary.banco + "\n";
        texto += "Moneda y Monto :" + " " + ary.monedamonto + "\n";
        texto += "Comisi�n Interbancaria :" + " " + ary.comisioninterbancaria + "\n";
        texto += "Comisi�n Cliente :" + " " + ary.comisioncliente + "\n";
        texto += "Comisi�n de Confirmaci�n de Abono :" + " " + ary.comisionabono + "\n";
        texto += "ITF :" + " " + ary.itf + "\n";
        texto += "Total :" + " " + ary.totaloperacion + "\n";
        texto += "C�digo Solicitud :" + " " + ary.codigosolicitud + "\n";
        

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }

</script>