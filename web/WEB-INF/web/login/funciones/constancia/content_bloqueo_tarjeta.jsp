<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Constancia de Bloqueo de Tarjeta

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">C�digo de operaci�n: </span> ${constancia_bloqueo_codigo_operacion}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Descripci�n del Bloqueo: </span> ${constancia_bloqueo_codigo_descripcion}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Estado de operaci�n: </span>Exitosa</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Fecha y hora: </span> ${constancia_bloqueo_fecha_operacion} ${constancia_bloqueo_hora_operacion}</p>

            </div>

            <p style="font-weight: 900">CAJA HUANCAYO-... �Tu mejor opci�n financiera</p>
            <p style="font-weight: 900">${constancia_bloqueo_mensaje_cmac}</p>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<%
    String id = (String) request.getSession().getAttribute("r_codigoCliente");
    String content = "";
    String cabecera = "";
    if (id != null) {

        content = "/web/glb/footer_login.jsp";

    } else {

        content = "/web/glb/footer_guest.jsp";

    }
%>


<jsp:include page="<%=content%>" flush="true"/>


<jsp:include page="/web/glb/script_general.jsp" />

<script>

    function compartir() {


        var ary = ${compartir_bloqueo};

        var texto = "Constancia de Bloqueo de Tarjeta" + "\n";
        texto += "------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "N�mero de tarjeta :" + " " + ary.numero_tarjeta + "\n";
        texto += "C�digo de descripci�n :" + " " + ary.codigo_descripcion + "\n";
        texto += "Mensaje  :" + " " + ary.mensaje + "\n";

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }

</script>