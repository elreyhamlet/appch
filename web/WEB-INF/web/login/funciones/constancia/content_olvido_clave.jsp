<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Constancia de Modificación de clave

        </p>
    </div>
    <c:set var="constancia" scope="application" value="${constancia}"/>
    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">Tipo de operación: </span>Cambio de Clave Web</p>
                <a href="../constancia.jsp"></a>
                <!--<p><span style="font-weight: 900">Tipo de operación: </span> Cambio de clave web</p>-->

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Estado de operación: </span>Exitosa</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Fecha y hora: </span> ${constancia[2]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Correo Electrónico: </span> ${constancia[3]}</p>

            </div>

            <p style="font-weight: 900">${constancia[4]}</p>

        </div>

    </div>

</div>

<%
    String id = (String) request.getSession().getAttribute("r_codigoCliente");
    String pie = "";
    if (id != null) {
        pie = "/web/glb/footer_login.jsp";
    } else {
        pie = "/web/glb/footer_guest.jsp";
    }
%>

<jsp:include page="<%=pie%>" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("#footer_transferencias").removeClass('footer-gris-img');

         });

</script>