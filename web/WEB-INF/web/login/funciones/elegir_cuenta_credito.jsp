<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="elegir_cuenta/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />  
        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("CuentaPropiaServlet-listar_cuenta_propia") == 0) {// poner de donde viene
                content = "elegir_cuenta/content_origen_cuentas_propias.jsp";
            } else if (referrer.compareTo("CuentaPropiaServlet-listar_cuenta_propia_destino") == 0) {// poner de donde vien
                content = "elegir_cuenta/content_destino_cuentas_propias.jsp";
            } else if (referrer.compareTo("PagoServicioServlet") == 0) {// poner de donde viene
                content = "elegir_cuenta/content_cargo_pago_servicios.jsp";
            } else if (referrer.compareTo("PagoCreditoPropioServlet") == 0) {// poner de donde viene
                content = "elegir_cuenta_credito/content_cargo_pago_creditos_propios.jsp";
            } 
            else if (referrer.compareTo("PagoCreditoTerceroServlet") == 0) {// poner de donde viene
                content = "elegir_cuenta/content_cargo_pago_creditos_terceros.jsp";
            }
            else if (referrer.compareTo("EnvioGirosServlet") == 0) {// poner de donde viene
                content = "elegir_cuenta/content_cargo_envios_giros.jsp";
            }
            else if (referrer.compareTo("RegistroPagoCreditoServlet") == 0) {// poner de donde viene
                content = "elegir_cuenta/content_cargo_pago_tarjeta_credito.jsp";
            }
        %>

        <jsp:include page="<%=content%>" />



        

        <jsp:include page="elegir_cuenta/script.jsp" />


    </body>
</html>