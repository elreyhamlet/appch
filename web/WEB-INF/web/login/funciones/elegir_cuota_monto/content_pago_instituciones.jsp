
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row cuerpo">


    <div class="col-xs-12 text-center">

        <span class="negrita" style="font-size:18px">Selecciona monto a pagar</span>

    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">



        <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
            <div class="text-center"> 
                <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${elegir_cuota_monto_instituciones[2]}
                </label>
            </div>
            <div class="text-center"> ${elegir_cuota_monto_instituciones[4]}</div>

        </div>

        <c:forEach var="lista" items="${elegir_cuota_monto_instituciones_detalle}">  
            <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

                <div class="text-center"> 

                    <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${lista[20]}<span class="formato_monto"> ${lista[8]}</span>

                        <input    name="radioButton" id="ids__${lista[25]}" type="checkbox">

                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="text-center">Vence: ${lista[21]}</div>
            </div>

        </c:forEach>

    </div>

    <div class="col-xs-6 text-center">
        <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
    </div>
    <div class="col-xs-6 text-center">
        <a   onclick="funcion_validar_seleccionar();" class="btn btn-caja btn-block">Confirmar</a>
    </div>
    <br><br>

</div>
<script type="text/javascript">

    function funcion_validar_seleccionar() {

        var codigo = [];
        var valor_anterior = 0;

        var alerta1 = 0;
        var alerta2 = 0;
        var alerta3 = 0;


        var mensaje_error = 0;

        $.each($("input:checkbox[name=radioButton]:checked"), function (key, value) {

            var arr = value.id;
            var id_actual = arr.split("__")[1];

            if( parseInt( valor_anterior ) + 1 !== parseInt( id_actual ) ){

                mensaje_error = 1;
                alerta2 = 1;

            }

            if( key === 0  && parseInt( id_actual ) !== 1){

                mensaje_error = 1;
                alerta1 = 1;

                alert(2);

            }

            codigo.push( id_actual );

            valor_anterior = id_actual;

        });

        if( codigo.length === 0 ){

            mensaje_error = 1;
            alerta3 = 1;

        }

        if( mensaje_error === 1 ){

            var texto = '<ul>';

            if( parseInt( alerta3 ) === 1 ){

                texto += "<li>Elija una cuota</li>";

            }

            if( parseInt( alerta1 ) === 1 ){

                texto += "<li>Elija la primera cuota</li>";

            }

            if( parseInt( alerta2 ) === 1 ){

                texto += "<li>Elija cuotas consecutivas</li>";

            }

            texto += '</ul>';

            message_req( "Error" , texto );
            return;

        }


        if (codigo.length > 0) {

            $.ajax({
                url: '${pageContext.request.contextPath}/PagoInstitucionesServlet',
                type: 'post',
                dataType: "text",
                beforeSend: function () {

                },
                data: {
                    accion: 'elegir_cuenta_cargo',
                    array: codigo
                },
                success: function (salida) {

                    if (salida.length !== 0) {

                        var array = salida.split("%%%");
                        if (array[0] === "0") {
                            crearForm(array[1]);
                         } else {
                            message_req("Mensaje", array[1]);
                        }

                    } else {
                        message_req("Mensaje", "Error Inesperado");
                    }
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                }
            });


        } else {
            message_req("Mensaje", "Ingrese cuota a pagar");

        }

    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>
