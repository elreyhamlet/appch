
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row cuerpo">


    <div class="col-xs-12 text-center">

        <span class="negrita" style="font-size:18px">Selecciona monto a pagar</span>

    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">



        <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
            <div class="text-center"> 
                <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${elegir_cuota_monto_servicio[2]}
                </label>
            </div>
            <div class="text-center"> ${elegir_cuota_monto_servicio[8]}</div>

        </div>
        <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
            <div class="text-center"> 
                <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${elegir_cuota_monto_servicio[10]} <span class="formato_monto"> ${elegir_cuota_monto_servicio[11]}</span>
                </label>
            </div>
            <div class="text-center">Vence:  ${elegir_cuota_monto_servicio[16]}</div>

        </div>


    </div>

    <div class="col-xs-6 text-center">
        <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
    </div>
    <div class="col-xs-6 text-center">
        <a   onclick="funcion_validar_seleccionar();" class="btn btn-caja btn-block">Confirmar</a>
    </div>
    <br><br>

</div>
<script type="text/javascript">


    function funcion_validar_seleccionar() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoServicioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'elegir_cuenta_cargo'
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>
