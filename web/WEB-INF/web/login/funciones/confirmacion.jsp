<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="confirmacion/head.jsp" />

        
    </head>
    <body>
        <jsp:include page="/web/glb/header_login.jsp" />  
        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("PagoServicioServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_pago_servicios.jsp";
            } else if (referrer.compareTo("EnvioGirosServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_envio_giros.jsp";
            } else if (referrer.compareTo("RegistroPagoCreditoServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_pago_tarjeta_credito.jsp";
            } else if (referrer.compareTo("PagoCreditoPropioServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_pago_creditos_propios.jsp";
            } else if (referrer.compareTo("PagoCreditoTerceroServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_pago_creditos_terceros.jsp";
            } else if (referrer.compareTo("CuentaPropiaServlet-confirmar_transferencia_cuenta_propia") == 0) {// poner de donde viene
                content = "confirmacion/content_cuentas_propia.jsp";
            }      
             else if (referrer.compareTo("CuentaTerceroServlet-confirmar_transferencia_cuenta_tercero") == 0) {// poner de donde viene
                content = "confirmacion/content_cuentas_terceros.jsp";
            }      
             else if (referrer.compareTo("CuentaOtroBancoServlet-confirmar_transferencia_cuenta_otro_banco") == 0) {// poner de donde viene
                content = "confirmacion/content_cuentas_otros_bancos.jsp";
            }      
             else if (referrer.compareTo("PagosRecargasServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_recarga_celular.jsp";
            }      
             else if (referrer.compareTo("PagoInstitucionesServlet") == 0) {// poner de donde viene
                content = "confirmacion/content_pago_instituciones.jsp";
            }      
        %>
        <jsp:include page="<%=content%>" />
        

        <jsp:include page="confirmacion/script.jsp" />


    </body>
</html>