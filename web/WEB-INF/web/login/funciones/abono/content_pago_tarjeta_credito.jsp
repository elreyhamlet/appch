<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center negrita titulo" >Confirmaci�n de solicitud de abono</p>

        </div>
    </div>

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10px">

        <div class="col-xs-12 text-center" style="padding-bottom: 10px">�Desea recibir la confirmaci�n de abono?</div>

        <div class="col-xs-6">

            <button style="border: 1px solid #C0C7C8" id="boton_si" type="button" class="btn btn-secondary elegir-abono" > Si </button><br>
            <span style="font-size: 12px">Aplica Comisi�n Adicional</span>
        </div>
        <div class="col-xs-6">

            <button style="border: 1px solid #C0C7C8" id="boton_no" type="button" class="btn btn-secondary elegir-abono btn-elegido" > No </button>

        </div>



    </div>


    <div class="col-xs-12 text-center" style="padding-top: 20px">




        <div class="btn-group">

            <button style="width:100px;border: 1px solid #C0C7C8" id="boton_celular" type="button" class="btn btn-secondary" > Celular </button>
            <button style="width:100px;border: 1px solid #C0C7C8" id="boton_correo" type="button" class="btn btn-secondary" > Correo </button>


        </div>


        <div class="col-xs-12" style="padding-top: 10px;padding-bottom: 20px">

            <input id="ingresar_contacto" type="text" class="form-control" placeholder="Elija Primero un Medio" disabled>

            <input style="display:none" id="ingresar_contacto_telefono" type="tel" class="form-control solo-numeros" placeholder="Ingrese su Celular">
            <input style="display:none" id="ingresar_contacto_correo" type="text" class="form-control" placeholder="Ingrese su Correo">


        </div>

    </div>




    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a onclick="registrar_abono_pago_credito();" class="btn btn-caja btn-block">Siguiente</a>
        </div><br><br>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    var celular_correo = "";

    var opcion_elegida = "false";

    $("body").on("click", ".elegir-abono", function () {

        switch (this.id) {

            case "boton_si":

                $("#boton_no").removeClass("btn-elegido");
                $(this).addClass("btn-elegido");

                $("#boton_celular").addClass("elegir-medio");
                $("#boton_correo").addClass("elegir-medio");

                $("#ingresar_contacto").show();
                $("#ingresar_contacto_telefono").hide();
                $("#ingresar_contacto_correo").hide();

                opcion_elegida = "true";
                celular_correo = "";

                break;

            case "boton_no":

                $("#boton_si").removeClass("btn-elegido");
                $(this).addClass("btn-elegido");

                $("#boton_celular").removeClass("elegir-medio").removeClass("btn-elegido");
                $("#boton_correo").removeClass("elegir-medio").removeClass("btn-elegido");

                $("#ingresar_contacto").attr("placeholder", "Elija un Medio Primero").attr("disabled", true).val("");

                $("#ingresar_contacto").show();
                $("#ingresar_contacto_telefono").hide();
                $("#ingresar_contacto_correo").hide();

                opcion_elegida = "false";
                celular_correo = "";

                break;

        }

    });


    $("body").on("click", ".elegir-medio", function () {

        switch (this.id) {

            case "boton_celular":
                $("#boton_correo").removeClass("btn-elegido");
                $(this).addClass("btn-elegido");
                $("#ingresar_contacto").hide();
                $("#ingresar_contacto_telefono").show();
                $("#ingresar_contacto_correo").hide();
                celular_correo = "T";

                break;

            case "boton_correo":
                $("#boton_celular").removeClass("btn-elegido");
                $(this).addClass("btn-elegido");
                $("#ingresar_contacto").hide();
                $("#ingresar_contacto_telefono").hide();
                $("#ingresar_contacto_correo").show();
                celular_correo = "C";

                break;

        }

    });

    function registrar_abono_pago_credito() {


        var array = validarCampos();
        if (array[0] === true) {
            js_confirmar_otro_banco();

        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function validarCampos() {

        var array = [];
        var result = "<ul>";
        var flag = true;

        var medio_actual = "";

        if (celular_correo === "C") {

            medio_actual = $("#ingresar_contacto_correo").val();

        } else if (celular_correo === "T") {

            medio_actual = $("#ingresar_contacto_telefono").val();

            if (!medio_actual.match(/^\d+$/)) {

                result += "<li> Ingrese solo valores n�mericos </li>";
                flag = false;

            }

            if (medio_actual.length !== 9) {

                result += "<li> Ingrese un n�mero de 9 d�gitos </li>";
                flag = false;

            }

            if (medio_actual.charAt(0) !== "9") {

                result += "<li> Ingrese un n�mero de Celular Valido </li>";
                flag = false;

            }

        }

        if (opcion_elegida === "true") {

            if (celular_correo === "") {

                result += "<li> Elija un medio de Confirmaci�n </li>";
                flag = false;

            }

            if (celular_correo === "C" && !validar_email(medio_actual)) {

                result += "<li> Ingrese un Correo Valido </li>";
                flag = false;

            }





            if (medio_actual.length === 0) {

                result += "<li> Ingrese el medio indicado </li>";
                flag = false;

            }

        }

        array = [flag, result];

        return array;
    }

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

    });



    function js_confirmar_otro_banco() {

        var medio_ingresado = "";

        if (celular_correo === "C") {

            medio_ingresado = $("#ingresar_contacto_correo").val();

        } else if (celular_correo === "T") {

            medio_ingresado = $("#ingresar_contacto_telefono").val();

        }

        $.ajax({
            url: '${pageContext.request.contextPath}/RegistroPagoCreditoServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'confirmar_registro_pago_credito',
                opcion_elegida: opcion_elegida,
                celular_correo: celular_correo,
                medio_ingresado: medio_ingresado,
                ippublicamovil: ip_movil()


            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });



    }



    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }
</script>