<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="elegir_cuota_monto/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            
            if (referrer.compareTo("CuentaOtroBancoServlet") == 0) {
                content = "abono/content_cuentas_otros_bancos.jsp";
            } else if (referrer.compareTo("RegistroPagoCreditoServlet") == 0) {
                content = "abono/content_pago_tarjeta_credito.jsp";
            }
            
        %>
        <jsp:include page="<%=content%>" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="elegir_cuota_monto/script.jsp" />


    </body
</html>