<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center negrita titulo" >Confirmación de solicitud de Transferencia Interbancaria</p>

        </div>
    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta Origen: <span>${conf_det_otro_banco[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Cuenta Destino: <span>${conf_det_otro_banco[1]} <br>${conf_det_otro_banco[2]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Moneda y Monto: <span>${conf_det_otro_banco[3]}</span><span id="monto_interbancario"> ${conf_det_otro_banco[4]}</span></span>  

        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Comisión Interbancaria: <span>${conf_det_otro_banco[3]}</span> <span>${conf_det_otro_banco[5]}</span></span>  

            <br>

            <span class="negrita" style="font-size: 15px">Comisión Cliente: <span>${conf_det_otro_banco[3]}</span> <span>${conf_det_otro_banco[6]}</span></span>  

            <br>

            <span class="negrita" style="font-size: 15px">Comisión de Confirmación de Abono: <span>${conf_det_otro_banco[3]}</span> <span>${conf_det_otro_banco[7]}</span></span> 

            <br>

            <span class="negrita" style="font-size: 15px">ITF: <span>${conf_det_otro_banco[3]}</span> <span>${conf_det_otro_banco[10]}</span></span> 

        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Total: <span>${conf_det_otro_banco[3]} <span id="total_interbancario">${conf_det_otro_banco[8]} </span>  </span></span>  

        </div>



    </div>


    <div class="col-xs-12 text-center">

        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">
            <p>

                <span>${conf_det_otro_banco[9]}</span> <br>

                <input id="coordenada_clave_dinamica" type="tel" maxlength="6" class="form-control 
                       <% String clave = (String) request.getSession().getAttribute("flag_clave");
                           if (clave.equals("A")) { %>
                       validar-numero-3
                       <% } else if (clave.equals("D")) { %>
                       validar-numero-6
                       <% }%>
                       ">
            </p>
        </c:if>

        <div style="margin-bottom:15px">
            <span>Guardar como operación frecuente  </span> 
            <span style="display:inline-block;height: 16px;padding-left:5px">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" >
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                    <input type="hidden" id="estado_ope" value="false">
                </div>
            </span>

        </div>


        <div class="col-xs-9 col-xs-offset-3" style="margin-top: 10px">   

            <input readonly="true" style="text-align: right" type="text"  class="form-control " placeholder="Ejemplos: luz casa, Cuenta papá" id="ope_frecuente">


        </div>

    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_transferencias();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <button onclick="registrar_constancia_otro_banco();" class="btn btn-caja btn-block">Confirmar</button>
        </div><br><br>

    </div>

</div>

<div class="modal fade" id="modal_terminos_condiciones_otros_bancos" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">

            <div class="modal-header">

                <p style="margin-bottom: .0001pt; text-align: center"><strong><u><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;font-weight:600">TÉRMINOS Y CONDICIONES</span></u></strong></p>

            </div>

            <div class="modal-body">

                <div id="contenedor_texto" style="height: 80%;overflow-y: scroll">

                    <ol style="text-align:justify;padding-left:15px;padding-right:10px">
                        <li style="margin-bottom: 5px; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> La Caja no asume responsabilidad por eventuales errores, demoras en la tramitaci&oacute;n e interpretaci&oacute;n de la transferencia a informaci&oacute;n consignada por el cliente/solicitante.</span></li>
                        <li style="margin-bottom: 5px; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Queda establecido que, si el pago no llegara a realizarse por causas ajenas a la Caja, la comisi&oacute;n y gastos no ser&aacute;n reembolsados.</span></li>
                        <li style="margin-bottom: 5px; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Los Bancos efectuar&aacute;n la devoluci&oacute;n o rechazo de las transacciones por los motivos y en los plazos definidos por el reglamento: Cuenta errada o suspendida, Cuenta Inexistente, Nro. de Cuenta inv&aacute;lido, Sucursal u Of. Errada, Cuenta Bloqueada, Moneda distinta a la de la Cuenta a acreditar, Sucursal u Of. no habilitada, Transacci&oacute;n duplicada.</span></li>
                        <li style="margin-bottom: 5px; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Los Bancos en el caso de las transferencias con destino a Cuentas de abono o pagos a Cuenta de tarjetas de Cr&eacute;dito se basan en el CCI o C&oacute;digo de Tarjeta especificado por el ordenante de la transferencia. El Banco no asume responsabilidad alguna si por error del ordenante al proporcionar el CCI o el C&oacute;digo de Tarjeta de Cr&eacute;dito, los fondos se acreditan a favor de otro beneficiario distinto.</span></li>
                        <li style=" line-height: normal;"><span style="font-size: 11.5pt; color: black;"> De haber una devoluci&oacute;n o rechazo por parte del banco de destino, su importe se acreditar&aacute; en la cuenta origen.</span></li>
                    </ol>

                </div>

            </div>

            <div class="modal-footer">

                <div class="col-xs-6">

                    <button type="button" class="btn btn-caja btn-block" data-dismiss="modal"> No Acepto </button>

                </div>

                <div class="col-xs-6">

                    <button id="btn-confirmar" onclick="js_constancia_otro_banco();" class="btn btn-caja btn-block"> Acepto </button>

                </div>

            </div>
        </div>

    </div>
</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');

        $("#monto_interbancario,#total_interbancario").number(true, 2);

        alto_pantalla();

        function alto_pantalla() {


            var alto = $(window).height() * 0.6;

            $("#contenedor_texto").height(alto);

        }


    });


    $(function () {

        $(document).ready(function () {
            $('.onoffswitch-checkbox').on('change', function () {
                var isChecked = $(this).is(':checked');

                console.log('isChecked: ' + isChecked);
                if (isChecked === true) {
                    document.getElementById("estado_ope").value = "true";
                    $('#ope_frecuente').removeAttr("readonly");
                } else {
                    document.getElementById("estado_ope").value = "false";
                    $('#ope_frecuente').attr("readonly", "true");
                }

            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.myonoffswitch'), true);
        });

    });


    function validarCampos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var ope_frecuente = $("#ope_frecuente").val();
        var flag = true;

        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>Ingrese su Clave</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres dígitos</li>";
                    }
                }

            }
        }
        if (operacion_recurente === "1") {
            if (ope_frecuente.length === 0) {
                flag = false;
                result += "<li>Ingrese alias</li>";
            }
        }


        array = [flag, result];

        return array;
    }
    function registrar_constancia_otro_banco() {


        var array = validarCampos();
        if (array[0] === true) {
            terminos_condiciones_otros_bancos();

        } else {

            message_req("Mensaje", array[1]);



        }
    }

    function terminos_condiciones_otros_bancos() {

        $("#modal_terminos_condiciones_otros_bancos").modal();

    }




    function js_constancia_otro_banco() {

        ope_frecuente = $("#ope_frecuente").val();
        estado_ope = $("#estado_ope").val();

        coordenada_clave_dinamica = $("#coordenada_clave_dinamica").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaOtroBancoServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'constancia_transferencia_cuenta_otro_banco',
                ope_frecuente: ope_frecuente,
                estado_ope: estado_ope,
                ippublicamovil: ip_movil(),
                clave: coordenada_clave_dinamica

            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        $("#modal_terminos_condiciones_otros_bancos").modal("toggle");
                        message_req("Mensaje", array[1]);

                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
        });



    }



    function ingresar_transferencias() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
        });

    }
</script>