<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita titulo" >Confirmaci�n de Env�o de giro</p>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta  cargo <span>${confirmacion_giros[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Beneficiario de giro <span>${confirmacion_giros[1]}</span></span>
            <br/>
            <span class="negrita" style="font-size: 15px">DNI <span>${confirmacion_giros[2]}</span></span> 
            <br/>
            <span class="negrita" style="font-size: 15px">Destino <span>${confirmacion_giros[3]}</span></span>  

        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Moneda y monto: <span>${confirmacion_giros[4]}</span></span>  


        </div>




    </div>


    <div class="col-xs-12 text-center">
        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">

            <p>

                <span>${confirmacion_giros[5]}</span> <br>

                <input id="coordenada_clave_dinamica" type="tel" maxlength="6" class="form-control 
                       <% String clave = (String) request.getSession().getAttribute("flag_clave");
                           if (clave.equals("A")) { %>
                       validar-numero-3
                       <% } else if (clave.equals("D")) { %>
                       validar-numero-6
                       <% }%>
                       ">

            </p>
        </c:if>




    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <button id="btn-confirmar" onclick="registrar_giro();" class="btn btn-caja btn-block">Confirmar</button>
        </div><br><br>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

    });

    
    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
   
        var flag = true;

        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>${confirmacion_giros[5]}</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }

        array = [flag, result];

        return array;
    }
    function registrar_giro() {


        var array = validar_campos();
        if (array[0] === true) {
            funcion_ajax_registrar_giro();

        } else {

            message_req("Mensaje", array[1]);
        }
    }


    function funcion_ajax_registrar_giro() {

        var coordenada = $("#coordenada_clave_dinamica").val();
     
        $.ajax({
            url: '${pageContext.request.contextPath}/EnvioGirosServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled",true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar_giro',
                coordenada: coordenada,
                ip: ip_movil()
            },
            success: function (salida) {
                
                $("#btn-confirmar").attr("disabled",false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
            
        });

    }

</script>