<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita titulo" >Confirmaci�n de Pago de cr�dito terceros</p>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta cargo: <span> ${confirmacion_creditos_terceros[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Cuenta cr�dito: <span> ${confirmacion_creditos_terceros[1]} </span></span><br> 

            <span class="negrita" style="font-size: 15px">T�tular: <span> ${confirmacion_creditos_terceros[2]}</span></span>

        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Cuota ${confirmacion_creditos_terceros[3]}: <span>${confirmacion_creditos_terceros[4]}</span><span id="monto_1"> ${confirmacion_creditos_terceros[5]}</span></span>  <span class="negrita" style="font-size: 15px">Vence:  <span>  ${confirmacion_creditos_terceros[6]} </span></span>


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Moneda y Monto: <span>${confirmacion_creditos_terceros[4]}</span><span id="monto_2">  ${confirmacion_creditos_terceros[7]}</span></span>  


        </div>



    </div>


    <div class="col-xs-12 text-center">

        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">

            <p>
                <span>${confirmacion_creditos_terceros[8]}</span> <br>

                <input id="coordenada_clave_dinamica" type="tel" maxlength="6" class="form-control 
                       <% String clave = (String) request.getSession().getAttribute("flag_clave");
                           if (clave.equals("A")) { %>
                       validar-numero-3
                       <% } else if (clave.equals("D")) { %>
                       validar-numero-6
                       <% }%>
                       ">

            </p>
        </c:if> 

        <div style="margin-bottom:15px">
            <span>Guardar como operaci�n frecuente  </span> 
            <span style="display:inline-block;height: 16px;padding-left:5px">
                <div class="onoffswitch" style="top:8px">
                    <input onchange="validar_alias();"  type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </span>

        </div>



        <p>

            <input id="alias_operacion" style="border-radius: 10px;border:1px solid #C0C7C8;padding: 5px" class="pull-right"placeholder="Ejemplos: luz casa, Cuenta pap�"/>

        </p>

    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <button id="btn-confirmar1" onclick="confirmar_registro();" class="btn btn-caja btn-block">Confirmar</button>
        </div><br><br>

    </div>

</div>


<div class="modal fade" id="modal_confirmar" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">

            <div class="modal-body">

                <h3 align="center">El monto a pagar no cubre la cuota, lo cual puede generar intereses y un mal historial crediticio, �desea continuar?</h3>

            </div>

            <div class="modal-footer">

                <div class="col-md-12 text-center">

                    <button class="btn btn-caja" data-dismiss="modal">Cancelar</button>
                    <button id="btn-confirmar2" class="btn btn-caja" onclick="registrar_pago_credito_tercero();" >Confirmar</button>

                </div>

                <div class="clearfix"></div>


            </div>

        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
        $("#monto_1").number(true, 2);
        $("#monto_2").number(true, 2);
        validar_alias();

    });

    function validar_alias() {

        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';

        if (operacion_recurente === "0") {
            $("#alias_operacion").val("");
            $("#alias_operacion").attr("readonly", true);
        } else {
            $("#alias_operacion").attr("readonly", false);
        }
    }

    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var alias_operacion = $("#alias_operacion").val();
        var flag = true;

        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>${confirmacion_creditos_terceros[8]}</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }
        if (operacion_recurente === "1") {
            if (alias_operacion.length === 0) {
                flag = false;
                result += "<li>Ingrese alias</li>";
            }
        }


        array = [flag, result];

        return array;
    }
    function registrar_pago_credito_tercero() {


        var array = validar_campos();
        if (array[0] === true) {
            funcion_ajax_registrar_credito_tercero();

        } else {

            message_req("Mensaje", array[1]);
        }
    }


    function funcion_ajax_registrar_credito_tercero() {

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var alias_operacion = $("#alias_operacion").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar1").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
                $("#btn-confirmar2").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar_pago_credito_tercero',
                coordenada: coordenada,
                operacion_recurente: operacion_recurente,
                alias_operacion: alias_operacion,
                ip: ip_movil()
            },
            success: function (salida) {

                $("#btn-confirmar1").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");
                $("#btn-confirmar2").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }


    function confirmar_registro() {
        // !!!
        $("#btn-confirmar1").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");

        if (${confirmacion_creditos_terceros[7]} < ${monto_maximo}) {

            $("#modal_confirmar").modal();

            $("#btn-confirmar1").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

        } else {

            registrar_pago_credito_tercero();

        }

    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>