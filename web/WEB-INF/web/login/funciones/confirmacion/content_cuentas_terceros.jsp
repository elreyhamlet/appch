<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center negrita titulo" >Confirmaci�n Transferencia a Cuenta de Terceros</p>

        </div>
    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta origen: <span>${confirmacion_cuentas_terceros[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Cuenta destino: <span>${confirmacion_cuentas_terceros[1]} <br> ${confirmacion_cuentas_terceros[2]} </span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Moneda y monto: <span>${confirmacion_cuentas_terceros[3]}</span><span id="monto_tercero">${confirmacion_cuentas_terceros[4]}</span></span>  


        </div>


    </div>


    <div class="col-xs-12 text-center">

        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">
            <p>
                <input type="hidden" id="tipo_afiliacion" value=${confirmacion_cuentas_terceros[5]}>
                <span>${confirmacion_cuentas_terceros[5]}</span> <br>

                <input id="coordenada_clave_dinamica" type="tel" maxlength="6" class="form-control 
                       <% String clave = (String) request.getSession().getAttribute("flag_clave");
                           if (clave.equals("A")) { %>
                       validar-numero-3
                       <% } else if (clave.equals("D")) { %>
                       validar-numero-6
                       <% }%>
                       ">

            </p>
        </c:if>

        <div style="margin-bottom:15px">
            <span>Guardar como operaci�n frecuente  </span> 
            <span style="display:inline-block;height: 16px;padding-left:5px">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" >
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                    <input type="hidden" id="estado_ope" value="false">
                </div>
            </span>

        </div>



        <div class="col-xs-9 col-xs-offset-3" style="margin-top: 10px">   

            <input readonly="false" style="text-align: right" type="text"  class="form-control " placeholder="Ejemplos: luz casa, Cuenta pap�" id="ope_frecuente">


        </div>

    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_transferencias();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <!--<a onclick="js_constancia_tercero();" class="btn btn-caja btn-block">Confirmar</a>-->
            <button id="btn-confirmar" onclick="registrar_constancia_tercero();" class="btn btn-caja btn-block">Confirmar</button>
        </div><br><br>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>



    (function () {
        $(document).ready(function () {
            
            $("img#footer_transferencias").attr('src','/AppCH/img/i-celular.png');
            $("#monto_tercero").number(true, 2);
            
            $('.onoffswitch-checkbox').on('change', function () {
                var is_checked = $(this).is(':checked');

                
                if (is_checked === true) {
                    document.getElementById("estado_ope").value = "true";
                    $('#ope_frecuente').removeAttr("readonly");
                } else {
                    document.getElementById("estado_ope").value = "false";
                    $('#ope_frecuente').attr("readonly", "true");
                }

            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.myonoffswitch'), true);
        });

    })();

    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var alias_operacion = $("#ope_frecuente").val();
        var flag = true;
        


        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>${confirmacion_cuentas_terceros[5]}</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }
        if (operacion_recurente === 1) {
            if (alias_operacion.length === 0) {
                flag = false;
                result += "<li>Ingrese alias</li>";
            }
        }


        array = [flag, result];

        return array;
    }
    
    function registrar_constancia_tercero() {


        var array = validar_campos();
        if (array[0] === true) {
            js_constancia_tercero();

        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function ingresar_transferencias() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled",true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {
                
                $("#btn-confirmar").attr("disabled","").addClass("btn-caja").removeClass("btn-caja-activo");
                
                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function js_constancia_tercero() {


        var flag_servlet = "${FLAG_OP_SERVLET}";
        var flag_frecuente = "${r_estado_frecuente}";
        if (flag_servlet === "false" || flag_frecuente === "false") {

            clave = document.getElementById("coordenada_clave_dinamica").value;

        } else {

            clave = "";



        }

        estado_ope = document.getElementById("estado_ope").value;
        ope_frecuente = document.getElementById("ope_frecuente").value;

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled",true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'constancia_transferencia_cuenta_tercero',

                ope_frecuente: ope_frecuente,
                estado_ope: estado_ope,
                ippublicamovil: ip_movil(),
                clave: clave

            },
            success: function (salida) {
                
                $("#btn-confirmar").attr("disabled",false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>