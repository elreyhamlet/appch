<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita titulo" >Confirmaci�n de solicitud de pago de tarjeta</p>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta Origen: <span>${datosConfirmacion_rpc[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Banco Destino: <span>${datosConfirmacion_rpc[1]}</span> </span><br>
            <span class="negrita" style="font-size: 15px">N�mero de Tarjeta: <span>${datosConfirmacion_rpc[2]} </span>


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Moneda y monto: <span>${datosConfirmacion_rpc[3]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Comisi�n Interbancar�a: <span>${datosConfirmacion_rpc[4]}</span></span>  

            <br>

            <span class="negrita" style="font-size: 15px">Comisi�n Cliente: <span>${datosConfirmacion_rpc[5]}</span></span>  

            <br>

            <span class="negrita" style="font-size: 15px">Comisi�n de Confirmaci�n de Abono: <span>${datosConfirmacion_rpc[6]}</span></span> 
            
            <br>

            <span class="negrita" style="font-size: 15px">ITF: <span>${datosConfirmacion_rpc[7]}</span></span>  

        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Total: <span>${datosConfirmacion_rpc[8]}</span></span>  

        </div>

    </div>


    <div class="col-xs-12 text-center">


        <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">
            <p>
                <span>${datosConfirmacion_rpc[9]}</span> <br>

                <input id="coordenada_clave_dinamica" type="tel" maxlength="6" class="form-control 
                       <% String clave = (String) request.getSession().getAttribute("flag_clave");
                           if (clave.equals("A")) { %>
                       validar-numero-3
                       <% } else if (clave.equals("D")) { %>
                       validar-numero-6
                       <% }%>
                       ">
            </p>

        </c:if>


        <div style="margin-bottom:15px">
            <span>Guardar como operaci�n frecuente  </span> 
            <span style="display:inline-block;height: 16px;padding-left:5px">
                <div class="onoffswitch" style="top:8px">
                    <input  onchange="validar_alias();" type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" >
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </span>

        </div>



        <p>

            <input readonly="true" id="alias_operacion" style="border-radius: 10px;border:1px solid #C0C7C8;padding: 5px" class="pull-right" placeholder="Ejemplos: luz casa, Cuenta pap�"  >

        </p>



    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a onclick="registrar_pagotarjeta();" class="btn btn-caja btn-block">Confirmar</a>
        </div><br><br>

    </div>

</div>
            

<div class="modal fade" id="modal_terminos_condiciones_tarjeta_credito" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">
            
            <div class="modal-header">
                
                <p style="margin-bottom: .0001pt; text-align: center"><strong><u><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif;font-weight:600">T�RMINOS Y CONDICIONES</span></u></strong></p>
                
            </div>

            <div class="modal-body">

                <div id="contenedor_texto" style="height: 80%;overflow-y: scroll">

<ol style="text-align:justify;padding-left:15px;padding-right:10px">
<li style="margin-bottom: 1.15pt; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> La Caja no asume responsabilidad por eventuales errores, demoras en la tramitaci&oacute;n e interpretaci&oacute;n de la transferencia a informaci&oacute;n consignada por el cliente/solicitante.</span></li>
<li style="margin-bottom: 1.15pt; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Queda establecido que, si el pago no llegara a realizarse por causas ajenas a la Caja, la comisi&oacute;n y gastos no ser&aacute;n reembolsados.</span></li>
<li style="margin-bottom: 1.15pt; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Los Bancos efectuar&aacute;n la devoluci&oacute;n o rechazo de las transacciones por los motivos y en los plazos definidos por el reglamento: Cuenta errada o suspendida, Cuenta Inexistente, Nro. de Cuenta inv&aacute;lido, Sucursal u Of. Errada, Cuenta Bloqueada, Moneda distinta a la de la Cuenta a acreditar, Sucursal u Of. no habilitada, Transacci&oacute;n duplicada.</span></li>
<li style="margin-bottom: 1.15pt; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> Los Bancos en el caso de las transferencias con destino a Cuentas de abono o pagos a Cuenta de tarjetas de Cr&eacute;dito se basan en el CCI o C&oacute;digo de Tarjeta especificado por el ordenante de la transferencia. El Banco no asume responsabilidad alguna si por error del ordenante al proporcionar el CCI o el C&oacute;digo de Tarjeta de Cr&eacute;dito, los fondos se acreditan a favor de otro beneficiario distinto.</span></li>
<li style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 11.5pt; color: black;"> De haber una devoluci&oacute;n o rechazo por parte del banco de destino, su importe se acreditar&aacute; en la cuenta origen.</span></li>
</ol>

                </div>

            </div>

            <div class="modal-footer">

                <div class="col-xs-6">

                    <button type="button" class="btn btn-caja btn-block" data-dismiss="modal"> No Acepto </button>

                </div>

                <div class="col-xs-6">

                    <button id="btn-confirmar" onclick="funcion_ajax_reg_pagotarjeta();" class="btn btn-caja btn-block"> Acepto </button>

                </div>

            </div>
        </div>

    </div>
</div>
            
            
<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {
        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
        
        alto_pantalla();

        function alto_pantalla(){


            var alto = $(window).height() * 0.6;

            $("#contenedor_texto").height(alto);

        }
        
    });

    function validar_alias() {

        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';

        if (operacion_recurente === "0") {
            $("#alias_operacion").val("");
            $("#alias_operacion").attr("readonly", true);
        } else {
            $("#alias_operacion").attr("readonly", false);
        }
    }
    function validarCampos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var alias_operacion = $("#alias_operacion").val();
        var flag = true;


        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>Ingrese su Clave</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }
        if (operacion_recurente === "1") {
            if (alias_operacion.length === 0) {

                flag = false;
                result += "<li>Ingrese alias</li>";
            }
        }


        array = [flag, result];

        return array;
    }
    function registrar_pagotarjeta() {


        var array = validarCampos();
        if (array[0] === true) {
            terminos_condiciones_tarjeta_credito();

        } else {

            message_req("Mensaje", array[1]);
            
            
        }
    }
    
    function terminos_condiciones_tarjeta_credito(){
    
        $("#modal_terminos_condiciones_tarjeta_credito").modal();
    
    }


    function funcion_ajax_reg_pagotarjeta() {

        var coordenada = $("#coordenada_clave_dinamica").val();
        var operacion_recurente = $("#myonoffswitch").is(":checked") ? '1' : '0';
        var alias_operacion = $("#alias_operacion").val();
        $.ajax({
            url: '${pageContext.request.contextPath}/RegistroPagoCreditoServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled",true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar',
                coordenada: coordenada,
                operacion_recurente: operacion_recurente,
                ip: ip_movil(),
                alias_operacion: alias_operacion
            },
            success: function (salida) {
                
                $("#btn-confirmar").attr("disabled",false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        $("#modal_terminos_condiciones_tarjeta_credito").modal("toggle");
                        message_req("Mensaje", array[1]);
                        
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>