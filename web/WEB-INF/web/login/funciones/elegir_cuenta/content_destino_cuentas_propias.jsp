<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
                Transferencia a Cuentas Propias

            </p>
            <span class="negrita">Selecciona una cuenta destino</span>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${elegir_cuenta_destino_cuentas_propias}">

            <a onclick="js_ing_monto('${lista[4]}');" style="color:black" class="cambiar_pagina">

                <div class="col-xs-12 cuenta-elegida text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">


                    <span class="negrita">${lista[1]} <span></span></span>
                   <!-- <span class="pull-right">${lista[3]} <span> ${lista[2]}</span>--> <span class="glyphicon glyphicon-play text-color-1"></span>
                </div>

            </a>

        </c:forEach>

    </div>




</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>
    $(function () {

        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');


        var mensaje = $("#hdd_mensaje_login").val();
        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }

    });

    function js_ing_monto(indicador_cuenta_destino) {


        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'ingresa_monto_propia',
                indicador_cuenta: indicador_cuenta_destino,
                tipo_mon_ori_cuenta_pro: "${tipo_mon_ori_cuenta_pro}"

            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>