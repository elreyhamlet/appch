
<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 text-center" style="margin-top:10px">

        <img src="${pageContext.request.contextPath}/img/i-auricular.png" alt="" style="max-width: 50px;width: 100%">

    </div>

    <div class="col-xs-12 col-md-12" style="margin-top: 20px">
        <p class="text-center" style="font-weight: 600;font-size: 20px">
            Cont�ctanos
        </p>
    </div>

    <div class="col-xs-12 col-md-12"  >

        <div class="form-group text-center" style="border:1px solid #808080;border-radius: 10px">
            <a  onclick="get_call_tel();">
                <div class="col-xs-4" style="padding: 8px">
                    <span><img style="width: 20px" src="${pageContext.request.contextPath}/img/i-telefono.png" alt=""></span>
                </div>
                <div class="col-xs-8" style="padding: 8px">
                    <span style="color: black">Call center</span>
                </div>
                <div class="clearfix"></div>
            </a>
        </div>
         <div class="form-group text-center" style="border:1px solid #808080;border-radius: 10px">

            <a onclick="enviar_email();">

                <div class="col-xs-4" style="padding: 8px" >
                    <span><img style="width: 20px"  src="${pageContext.request.contextPath}/img/i-correo.png" alt=""></span>
                </div>
                <div class="col-xs-8" style="padding: 8px">
                    <span style="color: black">Buz�n de Sugerencias</span>
                </div>
                <div class="clearfix"></div>
            </a>
        </div>

        <div class="form-group text-center" style="border:1px solid #808080;border-radius: 10px">
            <a href="#" onclick="set_compartir();">
                <div class="col-xs-4" style="padding: 8px">
                    <span><img style="width: 20px" src="${pageContext.request.contextPath}/img/i-like.png" alt=""></span>
                </div>
                <div class="col-xs-8" style="padding: 8px">
                    <span style="color: black">Recomendar a un amigo</span>
                </div>
                <div class="clearfix"></div>
            </a>
        </div>

    </div>

</div>

<script type="text/javascript">
 
</script>
