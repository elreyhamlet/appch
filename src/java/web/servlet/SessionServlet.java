package web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SessionServlet", urlPatterns = {"/SessionServlet"})
public class SessionServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String controlador = (String) request.getSession().getAttribute("rutaServlet");
        switch (controlador) {
            // poner de donde viene
            case "BloqueoTarjetaServlet":
                break;
            // poner de donde vien
            case "OlvidoClaveServlet":
                break;
            // poner de donde vien
            case "PagoServicioServlet":
                break;
            // poner de donde vien
            case "EnvioGirosServlet":
                break;
            // poner de donde vien
            case "RegistroPagoCreditoServlet":
                break;
            // poner de donde vien
            case "PagoCreditoPropioServlet":
                break;
            // poner de donde vien
            case "PagoCreditoTerceroServlet":
                break;
            // poner de donde vien
            case "CuentaPropiaServlet-constancia_transferencia_cuenta_propia":
                break;
            // poner de donde vien
            case "CuentaTerceroServlet-constancia_transferencia_cuenta_tercero":
                break;
            // poner de donde vien
            case "PagosRecargasServlet":

                break;
            // poner de donde vien
            case "CuentaOtroBancoServlet-constancia_transferencia_cuenta_otro_banco":
                break;
            // poner de donde vien
            case "MiPerfilServlet":
                break;
            // poner de donde vien
            case "PagoInstitucionesServlet":
                break;
            // poner de donde vien
            case "ConfiguraTarjetaServlet":
                break;
            // poner de donde vien
            case "ConfiguraOperFrecuentesServlet":
                break;
            default:
                break;

        }
        response.sendRedirect("./web/login/resumen_cuentas.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
