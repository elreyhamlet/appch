package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.LoginService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(req, resp);

        String accion = req.getParameter("accion");

        if (accion.equals("autenticar")) {

            String codigo_tarjeta = req.getParameter("codigoTarjeta");
            String numero_tarjeta = req.getParameter("numeroTarjeta");
            String clave_web = req.getParameter("claveWeb");
            String ippublicamovil = MetodosGlobales.getClientIpAddr(req);
            String codigo_instalacion = req.getParameter("codigoInstalacion");
            String estado_recordar = req.getParameter("estadoRecordar");
            String estado_huella_digital = req.getParameter("estadoHuellaDigital");
            String huella_cifrada = req.getParameter("huellaCifrada");
            String res = "";
            LoginService loginService = new LoginService();
            Result result = loginService.Login_autenticar(codigo_tarjeta, numero_tarjeta, clave_web, ippublicamovil, codigo_instalacion, estado_recordar, estado_huella_digital, huella_cifrada);

            if (result.getTipoMensaje() == 0) {

                List<Object> lista = metodosGlobales.read(result.getResultado());

                int c = 0;
                List<Object[]> array = new LinkedList<>();

                for (int i = 0; i < lista.size(); i++) {
                    List<Object> lis0 = (List<Object>) lista.get(i);
                    Object[] obj = new Object[lis0.size()];
                    for (Object obj1 : lis0) {
                        List<Object> lis1 = (List<Object>) obj1;

                        for (Object object1 : lis1) {
                            obj[c++] = object1;
                        }

                    }
                    array.add(obj);
                }
                String r_numeroTarjeta = array.get(0)[0].toString();
                String r_codigoCliente = array.get(0)[1].toString();
                String r_nombreCliente = array.get(0)[2].toString();
                String r_numeroDni = array.get(0)[3].toString();
                List<Object> array_tokenAcceso = (List<Object>) array.get(0)[4];
                List<Object> arr_tokenAcceso = (List<Object>) array_tokenAcceso.get(0);

                String r_tokenAcceso = "Bearer " + arr_tokenAcceso.get(0).toString();
                String r_tipoAfilicacionprevia = array.get(0)[5].toString();
                String r_tipoAfilicacion = (r_tipoAfilicacionprevia  == null) ? "" : r_tipoAfilicacionprevia;
                String r_descripcionAfiliacion = array.get(0)[6].toString();
                String r_estado_frecuente = array.get(0)[7].toString();

                Object correo_sugerencias = req.getSession().getAttribute("correo_sugerencias");
                Object tipo_cambio_compra = req.getSession().getAttribute("tipo_cambio_compra");
                Object tipo_cambio_venta = req.getSession().getAttribute("tipo_cambio_venta");
                Object numero_fijo = req.getSession().getAttribute("numero_fijo");
                String fecha_sistema = (String) req.getSession().getAttribute("fecha_sistema");
                String[] datos_tarjeta_recordar = (String[]) req.getSession().getAttribute("datos_tarjeta_recordar");
                String IP_iOS = (String) req.getSession().getAttribute("IP_iOS");
                String codigoInstalacion_iOS = (String) req.getSession().getAttribute("codigoInstalacion_iOS");
                String validador = (String) req.getSession().getAttribute("validador");
                Object[] lista_datos_tarjeta = (Object[]) req.getSession().getAttribute("lista_datos_tarjeta");
                Long fecha_numero = metodosGlobales.fecha_numero();

                req.getSession(false).invalidate();

                req.getSession(true);

                req.getSession().setAttribute("correo_sugerencias", correo_sugerencias);
                req.getSession().setAttribute("tipo_cambio_compra", tipo_cambio_compra);
                req.getSession().setAttribute("tipo_cambio_venta", tipo_cambio_venta);
                req.getSession().setAttribute("numero_fijo", numero_fijo);
                req.getSession().setAttribute("fecha_sistema", fecha_sistema);
                req.getSession().setAttribute("r_codigoCliente", null);
                req.getSession().setAttribute("datos_tarjeta_recordar", datos_tarjeta_recordar);
                req.getSession().setAttribute("IP_iOS", IP_iOS);
                req.getSession().setAttribute("codigoInstalacion_iOS", codigoInstalacion_iOS);
                req.getSession().setAttribute("validador", validador);
                req.getSession().setAttribute("lista_datos_tarjeta", lista_datos_tarjeta);

                req.getSession().setAttribute("fecha_numero", fecha_numero);

                req.getSession().setAttribute("r_numeroTarjeta", r_numeroTarjeta);
                req.getSession().setAttribute("r_codigoCliente", r_codigoCliente);
                req.getSession().setAttribute("r_nombreCliente", r_nombreCliente);
                req.getSession().setAttribute("r_numeroDni", r_numeroDni);
                req.getSession().setAttribute("r_tokenAcceso", r_tokenAcceso);
                req.getSession().setAttribute("r_tipoAfilicacion", r_tipoAfilicacion);
                req.getSession().setAttribute("r_descripcionAfiliacion", r_descripcionAfiliacion);
                req.getSession().setAttribute("r_estado_frecuente", r_estado_frecuente);
                res = result.getTipoMensaje() + "%%%" + "listar_cuentas";

                final String ID = req.getSession().getId();
                req.getSession().setAttribute("ID", ID);

            } else {

                res = result.getTipoMensaje() + "%%%" + result.getDescripcion();

            }
            try (PrintWriter out = resp.getWriter()) {
                out.print(res);
            }
        } else if (accion.equals("LOGOUT")) {

            req.getSession().invalidate();
            req.getSession().removeAttribute("ID");
            resp.sendRedirect("./Index?accion=index");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
