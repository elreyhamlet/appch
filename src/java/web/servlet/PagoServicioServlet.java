package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.PagoCreditoTerceroService;
import service.PagoServicioService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PagoServicioServlet", urlPatterns = {"/PagoServicioServlet"})
public class PagoServicioServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);

        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                request.getSession().setAttribute("rutaServlet", "PagoServicioServlet");
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

                switch (accion) {
                    case "pagoservicio":
                        pago_serv(request, response);
                        break;
                    case "buscarEmpresa":
                        buscarEmpresa(request, response);
                        break;
                    case "validarClienteServicio":
                        validarClienteServicio(request, response);
                        break;
                    case "confirmacionClienteServicio":
                        confirmacionClienteServicio(request, response);
                        break;
                    case "confirmarRegistro":
                        confirmarRegistro(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void validarClienteServicio(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String codigo_empresa = "";
        String dni_cliente_servicio = "";
        if (flag_servlet == false) {
            codigo_empresa = request.getParameter("codigo_empresa");
            dni_cliente_servicio = request.getParameter("codigo_cliente");
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP07");
            codigo_empresa = sesion.get(6).toString();
            dni_cliente_servicio = sesion.get(7).toString();
        }

        PagoServicioService tarjetaService = new PagoServicioService();
        Result resultadoWS = tarjetaService.validar_cliente_servicio(codigo_empresa, dni_cliente_servicio, token);
        String result;

        if (resultadoWS.getTipoMensaje() == 0) {
            List lista = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            request.getSession().setAttribute("elegir_cuota_monto_servicio", lista);
            
            // web/login/funciones/elegir_cuota_monto.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuotaMonto";
        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void confirmacionClienteServicio(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result;
        List data_session = (LinkedList) request.getSession().getAttribute("elegir_cuota_monto_servicio");
        Object[] obj_confirmacion = (Object[]) request.getSession().getAttribute("confirmacion_pago_servicios");
        String codigo_cuenta_origen = obj_confirmacion[0].toString();
        String codigo_empresa = data_session.get(1).toString();
        String razon_social_empresa = data_session.get(2).toString();
        String cuenta_servicio = data_session.get(14).toString();
        String codigo_cliente_servicio = data_session.get(7).toString();
        String nombre_cliente_servicio = data_session.get(8).toString();

        String estado_operacion_frecuente = request.getParameter("operacion_recurente");
        if (estado_operacion_frecuente.compareTo("1") == 0) {
            estado_operacion_frecuente = "true";
        } else {
            estado_operacion_frecuente = "false";
        }
        String alias_operacion_frecuente = request.getParameter("alias_operacion");

//        datos de validacion
        String tipo_afilacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String numero_columna = "";
        String numero_fila = "";
        //datos auditoria
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP07";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String claveDinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String claveCordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");

        String periodoPago = "";

        if (data_session.get(15) == null) {

            periodoPago = "";

        } else {

            periodoPago = data_session.get(15).toString();

        }

        String codigoSucursal = data_session.get(3).toString();
        String codigoConcepto = data_session.get(5).toString();
        String montoCuota = data_session.get(11).toString();
        String montoMora = data_session.get(12).toString();
        String montoComision = data_session.get(13).toString();

        String codigoSolicitud = "";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());
        if (flag_servlet == false || estado_frecuente_confirmar == false) {
            if (tipo_afilacion.compareTo("D") == 0) {
                claveCordenada = "";
                codigoSolicitud = (String) request.getSession().getAttribute("servicio_codigoSolicitud");

            } else if (tipo_afilacion.compareTo("A") == 0) {
                claveDinamica = "";
                numero_columna = (String) request.getSession().getAttribute("servicio_columna");
                numero_fila = (String) request.getSession().getAttribute("servicio_fila");
            }
        }
        PagoServicioService pss = new PagoServicioService();
        Result resultadoWS = pss.registrar_pago_servicio(
                codigo_cuenta_origen,
                codigo_empresa,
                razon_social_empresa,
                codigoSucursal,
                codigoConcepto,
                montoCuota,
                montoMora,
                montoComision,
                periodoPago,
                cuenta_servicio,
                codigo_cliente_servicio,
                nombre_cliente_servicio,
                estado_operacion_frecuente,
                alias_operacion_frecuente,
                tipo_afilacion,
                numero_tarjeta,
                codigo_cliente,
                claveDinamica,
                codigoSolicitud,
                claveCordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                estado_frecuente_confirmar.toString()
        );

        if (resultadoWS.getTipoMensaje() == 0) {
            List lista = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            String codigo_operacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();
            String cuentacargo = lista.get(3).toString();
            String empresa = lista.get(4).toString();
            String datos = lista.get(5).toString();
            String moneda = lista.get(6).toString();
            String mon = metodosGlobales.formato_monto(lista.get(7).toString());

            Object[] constancia = {codigo_operacion, fecha, hora, cuentacargo, empresa, datos, moneda, mon};
            request.getSession().setAttribute("constancia_pago_servicio", constancia);
 // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            //eliminar datos de session
            request.getSession().setAttribute("elegir_cuota_monto_servicio", null);
            request.getSession().setAttribute("confirmacion_pago_servicios", null);
            request.getSession().setAttribute("elegir_cuenta_servicios", null);

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();
            arrayCabecera.addObject("cod", codigo_operacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuentacargo)
                    .addObject("empresa", empresa)
                    .addObject("datos", datos)
                    .addObject("monedamonto", moneda + " " + mon);

            request.getSession().setAttribute("pago_servicio_compartir", arrayCabecera.getString());

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmarRegistro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List data_session = (LinkedList) request.getSession().getAttribute("elegir_cuota_monto_servicio");
        String monto = data_session.get(11).toString();

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_servicios");

        String indicador_cuenta = request.getParameter("indicador_cuenta");
        Integer ind = Integer.parseInt(indicador_cuenta);
        Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
        String cuenta_origen = (String) cuenta_total[0];

        String empresa = data_session.get(2).toString();
        String datos = data_session.get(8).toString();
        String moneda_monto = data_session.get(10).toString() + " " + metodosGlobales.formato_monto(monto);

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String codigo_sub_menu = "AP07";
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);
        String result = "";
        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();

        boolean flag = true;
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
        if (flag_servlet == false || estado_frecuente == false) {
            estado_frecuente_confirmar = false;
            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, monto, token);
                if (r.getTipoMensaje() == 0) {
                    List codigo_solicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("servicio_codigoSolicitud", codigo_solicitud.get(0));
                    System.out.println(codigo_solicitud.get(0));
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("servicio_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("servicio_fila", coordenada_clave.get(1));

            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        }
        request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
        if (flag) {

            Object[] obj = {cuenta_origen, empresa, datos, moneda_monto, coordenada_clave.get(2)};
            request.getSession().setAttribute("confirmacion_pago_servicios", obj);
            // web/login/funciones/confirmacion.jsp
            result = "0" + "%%%" + "confirmacion";
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar

        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("elegir_cuenta_servicios", lista);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta.jsp";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

//        result = "0" + "%%%" + "web/login/funciones/confirmacion.jsp";
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void pago_serv(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 // web/login/resumen_cuentas/pagos/pago_servicios.jsp
        String result = "0" + "%%%" + "pagoServicios";

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP07", null);

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void buscarEmpresa(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String razon_social_empresa = request.getParameter("empresa");
//        String razonSocialEmpresa = "c";
        String codigo_sub_menu = "AP07";
        PagoServicioService tarjetaService = new PagoServicioService();
        Result resultadoWS = tarjetaService.buscarEmpresa(razon_social_empresa, codigo_sub_menu);
        String content = "text/xml;charset=UTF-8";
        request.setCharacterEncoding("UTF-8");
        response.setContentType(content);
        String result = "";
        if (resultadoWS.getTipoMensaje() == 0) {
            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            List<Object[]> array = new LinkedList<>();
            Object[] obj = null;
            for (Object object : lista) {
                List data = (LinkedList) object;
                obj = new Object[data.size()];

                obj[0] = data.get(0);
                obj[1] = data.get(1);
                obj[2] = data.get(2);
                array.add(obj);
            }

            StringBuilder sb = new StringBuilder();
            for (Object[] f : array) {
                sb.append("<option value='").append(f[0]).append("%%%").append(f[2]).append("'>");
                sb.append(f[1]);
                sb.append("</option>");
            }
            result = sb.toString();

            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
