package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.PagoCreditoTerceroService;
import service.PagosRecargasService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PagosRecargasServlet", urlPatterns = {"/PagosRecargasServlet"})
public class PagosRecargasServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");
            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
                request.getSession().setAttribute("rutaServlet", "PagosRecargasServlet");
                switch (accion) {
                    case "listar_operadores":
                        listar_operadores(request, response);
                        break;
                    case "ver_confirmacion":
                        ver_confirmacion(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    case "registrar_recarga":
                        registrar_recarga(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar
        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP10", null);

        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("elegir_cuenta_recarga_celular", lista);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_operadores(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      
        String type=metodosGlobales.validar_dispositivo_teclado(req, resp);
        
        PagosRecargasService service = new PagosRecargasService();
        Result resultadoWS = service.listar_operadores();
        String result;
        Object obj_Flag = req.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String numeroctacargo = "";
        String tipo_mon_ori = "";
        if (flag_servlet == false) {
            List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
            array_lista_cuenta_ori = (List<Object[]>) req.getSession().getAttribute("elegir_cuenta_recarga_celular");

            String indicador_cuenta = req.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            numeroctacargo = (String) cuenta_total[0];
            tipo_mon_ori = (String) cuenta_total[3];

        } else {
            List sesion = (List) req.getSession().getAttribute("inputs_AP10");
            numeroctacargo = sesion.get(3).toString();
            tipo_mon_ori = sesion.get(4).toString();
        }

        String codigoMoneda = tipo_mon_ori;
        String moneda = "";
        if (codigoMoneda.compareTo("1") == 0) {
            moneda = "S/";
        } else {
            moneda = "USD";
        }

        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            if (flag_servlet == false) {
                ((List) lista[0]).set(1, "Seleccione el operador");
            } else {
                List sesion = (List) req.getSession().getAttribute("inputs_AP10");
                for (Object obj : lista) {

                    List list_operador = (List) obj;

                    if ((sesion.get(7).toString().trim()).equals(list_operador.get(0).toString())) {
                        ((List) lista[0]).set(0, list_operador.get(0).toString());
                        ((List) lista[0]).set(1, list_operador.get(1).toString());
                    }
                }
            }

            req.getSession().setAttribute("moneda_celular", moneda);
            req.getSession().setAttribute("numeroctacargo_celular", numeroctacargo);
            req.getSession().setAttribute("codigoMoneda_celular", codigoMoneda);
            req.getSession().setAttribute("lista_operadores", lista);
            req.getSession().setAttribute("tipo_mon_ori", tipo_mon_ori);
            req.getSession().setAttribute("type", type);
                          
            // web/login/resumen_cuentas/pagos/recarga_celular.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "recargaCelular.jsp";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }

    }

    protected void ver_confirmacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String numerotarjetacargo = (String) request.getSession().getAttribute("numeroctacargo_celular");
        String moneda_celular = (String) request.getSession().getAttribute("moneda_celular");
        String operador = request.getParameter("operador");
        String celular = request.getParameter("celular");
        String monto = request.getParameter("monto");
        String nombre_operador = request.getParameter("nombre_operador");
        String result = "";
        String codigoSubMenu = "AP10";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);
        Object[] datos = {numerotarjetacargo, nombre_operador, celular, moneda_celular, Double.parseDouble(monto), operador, coordenada_clave.get(2)};
        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();

        boolean flag = true;
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
        if (flag_servlet == false || estado_frecuente == false) {
            estado_frecuente_confirmar = false;
            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigoSubMenu, monto, token);
                if (r.getTipoMensaje() == 0) {
                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("reacarga_codigoSolicitud", codigoSolicitud.get(0));
                    System.out.println(codigoSolicitud.get(0));
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("giro_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("giro_fila", coordenada_clave.get(1));

            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        }
        request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
        if (flag) {
            request.getSession().setAttribute("recargas_columna", coordenada_clave.get(0));
            request.getSession().setAttribute("recargas_fila", coordenada_clave.get(1));
            request.getSession().setAttribute("confirmacion_recarga_celular", datos);

            // web/login/funciones/confirmacion.jsp
            result = "0" + "%%%" + "confirmacion";
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar_recarga(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Object[] session = (Object[]) request.getSession().getAttribute("confirmacion_recarga_celular");
        String codigo_cuenta_origen = session[0].toString();
        String codigo_operador = session[5].toString();
        String descripcion_operador = session[1].toString();
        String numero_celular = session[2].toString();
        String codigo_moneda = (String) request.getSession().getAttribute("codigoMoneda_celular");
        String monto_recarga = session[4].toString();
        String estado_operacion_frecuente = request.getParameter("operacion_recurente");

        if (estado_operacion_frecuente.compareTo("1") == 0) {
            estado_operacion_frecuente = "true";
        } else {
            estado_operacion_frecuente = "false";
        }
        String aliasOperacion = request.getParameter("alias_operacion");

        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");

        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");

        String numero_columna = "";
        String numero_fila = "";
        String ip_publica_movil  = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP10";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_solicitud = "";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());

        if (flag_servlet == false || estado_frecuente_confirmar == false) {
            if (tipo_afiliacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigo_solicitud = (String) request.getSession().getAttribute("reacarga_codigoSolicitud");

            } else if (tipo_afiliacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("recargas_columna");
                numero_fila = (String) request.getSession().getAttribute("recargas_fila");
            }
        }

        PagosRecargasService service = new PagosRecargasService();

        Result resultadoWS = service.registrar_recarga(
                codigo_cuenta_origen,
                codigo_operador,
                descripcion_operador,
                numero_celular,
                codigo_moneda,
                monto_recarga,
                estado_operacion_frecuente,
                aliasOperacion,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                codigo_solicitud,
                estado_frecuente_confirmar.toString());
        String result = "";
        if (resultadoWS.getTipoMensaje() == 0) {
            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());

            String codigo_operacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();
            String cuentacargo = lista.get(3).toString();
            String operador = lista.get(4).toString();
            String telefono = lista.get(5).toString();
            String moneda = lista.get(6).toString();
            String monto = metodosGlobales.formato_monto(lista.get(7).toString());

            Object[] datos = {codigo_operacion, fecha, hora, cuentacargo, operador, telefono, moneda, monto};
            request.getSession().setAttribute("constancia_recarga", datos);
            
            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            request.getSession().setAttribute("inputs_AP10", null);
            request.getSession().setAttribute("Session_AP10", false);

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();
            arrayCabecera.addObject("cod", codigo_operacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuentacargo)
                    .addObject("operador", operador)
                    .addObject("telefono", telefono)
                    .addObject("moneda", moneda)
                    .addObject("monto", monto);

            request.getSession().setAttribute("compartir_recarga", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
