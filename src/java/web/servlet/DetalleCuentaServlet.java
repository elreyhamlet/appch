package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.DetalleCuentaAhorroService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "DetalleCuentaServlet", urlPatterns = {"/DetalleCuentaServlet"})
public class DetalleCuentaServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {
            if (accion.equals("listar_detalle_cuenta")) {

                listar_detalle_cuenta(request, response);

            } else if (accion.equals("listar_detalle_cuenta_credito")) {

                listar_detalle_cuenta_credito(request, response);

            }
        }
    }

    protected void listar_detalle_cuenta(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String descripcion = req.getParameter("descripcion");
        String indicador = req.getParameter("indicador");

        Integer ind= Integer.parseInt(indicador);
        List<Object[]> array_lista_ahorro_cta = new LinkedList<>();

        array_lista_ahorro_cta  =  (List<Object[]>) req.getSession().getAttribute("lista_ahorro_cta");

        Object [] cuenta_total = array_lista_ahorro_cta.get(ind);
        String codigo_cuenta = (String) cuenta_total[0];
        String token = (String) req.getSession().getAttribute("r_tokenAcceso");
        String result = "";

        DetalleCuentaAhorroService service = new DetalleCuentaAhorroService();
        Result resultadoWSAhorro = service.listar_detalle_cuenta_ahorro(codigo_cuenta, token);
        if (resultadoWSAhorro.getTipoMensaje() == 0) {
            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWSAhorro.getResultado());

            for (Object object : (Object[]) ((List) lista[0]).get(4)) {
                List list = (List) object;
                String monto = metodosGlobales.formato_monto((String) list.get(2));
                list.set(2, monto);

            }

            req.getSession().setAttribute("array_lista_detalle_ahorro_cta_session", lista[0]);

            List<Object[]> array = new LinkedList<>();
            array = (List<Object[]>) lista[0];
            int dimension = array.get(4).length;

            req.getSession().setAttribute("dimension", dimension);
            req.getSession().setAttribute("descripcion", descripcion);
 
             // web/login/resumen_cuentas/detalle_ahorro.jsp
            result = resultadoWSAhorro.getTipoMensaje() + "%%%" + "detalle_ahorro";
        } else {
            result = resultadoWSAhorro.getTipoMensaje() + "%%%" + resultadoWSAhorro.getDescripcion();
        }
        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_detalle_cuenta_credito(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getSession().setAttribute("FLAG_OP_SERVLET", false);
        req.getSession().setAttribute("inputs_AP02", null);
        String descripcion_credito = req.getParameter("descripcion_credito");
        String indicador_credito = req.getParameter("indicador_credito");

        String token = (String) req.getSession().getAttribute("r_tokenAcceso");
        String numero_tarjeta = (String) req.getSession().getAttribute("r_numeroTarjeta");

        Integer ind= Integer.parseInt(indicador_credito);
        List<Object[]> array_lista_credito_cta = new LinkedList<>();

        array_lista_credito_cta  =  (List<Object[]>) req.getSession().getAttribute("lista_credito_cta");

        Object [] cuenta_total = array_lista_credito_cta.get(ind);
        String cuenta_credito_destino = (String) cuenta_total[0];

        String result = "";
        DetalleCuentaAhorroService service = new DetalleCuentaAhorroService();
        Result resultadoWSAhorro = service.listar_detalle_cuenta_credito(numero_tarjeta, cuenta_credito_destino, token);
        if (resultadoWSAhorro.getTipoMensaje() == 0) {
            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWSAhorro.getResultado());
            req.getSession().setAttribute("lista_detalle_credito_cta", lista[0]);

            req.getSession().setAttribute("descripcion_credito", descripcion_credito);

            //  web/login/resumen_cuentas/detalle_credito.jsp
            result = resultadoWSAhorro.getTipoMensaje() + "%%%" + "detalle_credito";

        } else {
            result = resultadoWSAhorro.getTipoMensaje() + "%%%" + resultadoWSAhorro.getDescripcion();

        }
        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
