package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaOtroBancoService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "CuentaOtroBancoServlet", urlPatterns = {"/CuentaOtroBancoServlet"})
public class CuentaOtroBancoServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {

            String accion = request.getParameter("accion");
            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {

                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

//                try (PrintWriter out = response.getWriter()) {
//                    out.print("1%%%Esta opción se encuentra inhabilitada");
//                }
                switch (accion) {
                    case "listar_cuenta_propia_otro_banco":
                        listar_cuenta_propia_otro_banco(request, response);
                        break;
                    case "ingresa_monto_propia_otro_banco":
                        ingresa_monto_propia_otro_banco(request, response);
                        break;
                    case "registrar_abono":
                        registrar_abono(request, response);
                        break;
                    case "confirmar_transferencia_cuenta_otro_banco":
                        confirmar_transferencia_cuenta_otro_banco(request, response);
                        break;
                    case "constancia_transferencia_cuenta_otro_banco":
                        constancia_transferencia_cuenta_otro_banco(request, response);
                        break;

                    default:
                        break;
                }

            }
        }
    }

    protected void listar_cuenta_propia_otro_banco(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP03", null);

        request.getSession().setAttribute("rutaServlet", "CuentaOtroBancoServlet-listar_cuenta_propia_otro_banco");

        String tipo_operacion_string = "P";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String result = "";
        CuentaOtroBancoService service = new CuentaOtroBancoService();
        Result resultadoWS = service.listar_cuenta_propia_otro_banco_detalle(codigo_cliente, tipo_operacion_string, token);

        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista_cuenta_otro_banco = new LinkedList<>();
            lista_cuenta_otro_banco = metodosGlobales.read3(resultadoWS.getResultado());

            request.setAttribute("elegir_cuenta_cuentas_otros_bancos", lista_cuenta_otro_banco);

            request.getSession().setAttribute("elegir_cuenta_cuentas_otros_bancos", lista_cuenta_otro_banco);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void ingresa_monto_propia_otro_banco(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String type = metodosGlobales.validar_dispositivo_teclado(request, response);

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        String numero_cuenta_origen = "";
        Boolean flag_servlet = (Boolean) obj_Flag;

        String tipo_mon_ori = "";
        String[] objsol = null;
        String[] objsol2 = new String[3];
        String[] objdol = new String[3];

        if (flag_servlet == false) {
            array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_cuentas_otros_bancos");
            String indicador_cuenta = request.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            numero_cuenta_origen = (String) cuenta_total[0];
            tipo_mon_ori = (String) cuenta_total[3];
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP03");
            request.getSession().setAttribute("numero_cuenta_pro", sesion.get(3).toString());
            numero_cuenta_origen = sesion.get(3).toString();
            tipo_mon_ori = sesion.get(4).toString();
        }

        String tipo_cambio_compra = (String) request.getSession().getAttribute("tipo_cambio_compra");
        String tipo_cambio_venta = (String) request.getSession().getAttribute("tipo_cambio_venta");

        String[] tipo_cambio = new String[2];
        tipo_cambio[0] = tipo_cambio_compra;
        tipo_cambio[1] = tipo_cambio_venta;

        String result = "";

        CuentaOtroBancoService service = new CuentaOtroBancoService();
        Result resultadoWS = service.listar_moneda_otro_banco();

        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] dat_mon = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
            for (Object dato : dat_mon) {

                LinkedList<String> linkedlist = (LinkedList<String>) dato;

                objsol = new String[linkedlist.size()];
                objdol = new String[linkedlist.size()];

                objsol[0] = linkedlist.get(0);
                objsol[1] = linkedlist.get(1);
                objsol[2] = linkedlist.get(2);

                String valor1 = objsol[0];
                String valor2 = objsol[1];
                String valor3 = objsol[2];

                if (valor1.equals("1")) {

                    objsol2[0] = valor1;
                    objsol2[1] = valor2;
                    objsol2[2] = valor3;
                } else if (valor1.equals("2")) {

                    objdol[0] = valor1;
                    objdol[1] = valor2;
                    objdol[2] = valor3;
                }

            }
            String valor_moneda = "";
            if (tipo_mon_ori.equals(objsol2[0])) {
                valor_moneda = objsol2[2];
            } else {
                valor_moneda = objdol[2];
            }

            request.getSession().setAttribute("dat_mon_sol", objsol2);
            request.getSession().setAttribute("dat_mon_dol", objdol);
            request.getSession().setAttribute("tipo_cambio", tipo_cambio);
            request.getSession().setAttribute("valor_moneda", valor_moneda);
            request.getSession().setAttribute("tipo_mon_ori", tipo_mon_ori);
            request.getSession().setAttribute("numero_cuenta_origen", numero_cuenta_origen);
            request.getSession().setAttribute("type", type);
            // web/login/resumen_cuentas/ingresar_monto_cuenta_interbancaria.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "ingresar_monto_cuenta_interbancaria";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }
// queda aquiui

    protected void registrar_abono(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaOtroBancoServlet");

        String monto = request.getParameter("monto");
        String estado_titular = request.getParameter("estado_titular");
        String cuenta_destino_interbancaria = request.getParameter("cuenta_destino_interbancaria");

        String tipo_mon_ori = (String) request.getSession().getAttribute("tipo_mon_ori");
        String numero_cuenta_origen = (String) request.getSession().getAttribute("numero_cuenta_origen");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String result = "";

        CuentaOtroBancoService service = new CuentaOtroBancoService();
        Result resultadoWS = service.val_cuenta_destino_interbancaria(
                cuenta_destino_interbancaria,
                monto, tipo_mon_ori, numero_cuenta_origen, estado_titular,
                token);

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista_val_destino = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            String cuenta_otrobanco_nombre = (String) lista_val_destino.get(0);
            monto = (String) lista_val_destino.get(1);
            String comision_interbancaria = (String) lista_val_destino.get(2);
            String comision_cliente = (String) lista_val_destino.get(3);
            String numero_cci = (String) lista_val_destino.get(4);
            String comision_abono = (String) lista_val_destino.get(5);
            String simbolo_moneda = (String) lista_val_destino.get(6);
            String itf = (String) lista_val_destino.get(7);
            String validador_ruta = (String) lista_val_destino.get(8);

            request.getSession().setAttribute("cuenta_otrobanco_nombre", cuenta_otrobanco_nombre);
            request.getSession().setAttribute("monto", monto);
            request.getSession().setAttribute("comision_interbancaria", comision_interbancaria);
            request.getSession().setAttribute("comision_cliente", comision_cliente);
            request.getSession().setAttribute("numero_cci", numero_cci);
            request.getSession().setAttribute("comision_abono", comision_abono);
            request.getSession().setAttribute("simbolo_moneda", simbolo_moneda);
            request.getSession().setAttribute("itf", itf);
            request.getSession().setAttribute("estado_titular", estado_titular);
            request.getSession().setAttribute("validador_ruta", validador_ruta);

            if (validador_ruta.equals("true")) {
                // web/login/funciones/abono.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "abono";
            } else {
                request.getSession().setAttribute("opcion_elegida", "");
                request.getSession().setAttribute("celular_correo", "");
                request.getSession().setAttribute("medio_ingresado", "");

                confirmar_transferencia_cuenta_otro_banco(request, response);
            }

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmar_transferencia_cuenta_otro_banco(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaOtroBancoServlet-confirmar_transferencia_cuenta_otro_banco");

        String validador_ruta = (String) request.getSession().getAttribute("validador_ruta");
        String opcion_elegida = "";
        String celular_correo = "";
        String medio_ingresado = "";

        if (validador_ruta.equals("true")) {
            opcion_elegida = request.getParameter("opcion_elegida");
            celular_correo = request.getParameter("celular_correo");
            medio_ingresado = request.getParameter("medio_ingresado");
        } else {
            opcion_elegida = (String) request.getSession().getAttribute("opcion_elegida");
            celular_correo = (String) request.getSession().getAttribute("celular_correo");
            medio_ingresado = (String) request.getSession().getAttribute("medio_ingresado");
        }

        String result = "";

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String numero_cuenta_origen = (String) request.getSession().getAttribute("numero_cuenta_origen");
        String valor_moneda = (String) request.getSession().getAttribute("valor_moneda");
        String tipo_mon_ori = (String) request.getSession().getAttribute("tipo_mon_ori");
        String tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");

        String cuenta_otrobanco_nombre = (String) request.getSession().getAttribute("cuenta_otrobanco_nombre");
        String monto = (String) request.getSession().getAttribute("monto");
        String comision_interbancaria = (String) request.getSession().getAttribute("comision_interbancaria");
        String comision_cliente = (String) request.getSession().getAttribute("comision_cliente");
        String numero_cci = (String) request.getSession().getAttribute("numero_cci");
        String comision_abono = (String) request.getSession().getAttribute("comision_abono");
        String itf = (String) request.getSession().getAttribute("itf");

        if (opcion_elegida.equals("false")) {
            comision_abono = "0.00";
        }

        double monto_int = Double.parseDouble(monto);
        double comision_interbancaria_int = Double.parseDouble(comision_interbancaria);
        double comision_cliente_int = Double.parseDouble(comision_cliente);
        double comision_abono_int = Double.parseDouble(comision_abono);
        double itf_int = Double.parseDouble(itf);

        String codigo_sub_menu = "AP03";
        String total_val = "";

        CuentaOtroBancoService service = new CuentaOtroBancoService();

        tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");

        ArrayList<String> afiliacion_tipo = new ArrayList<>();
        afiliacion_tipo = metodosGlobales.tipo_afiliacion(tipoAfiliacion);

        double total = monto_int + comision_interbancaria_int + comision_cliente_int + comision_abono_int + itf_int;
        total_val = String.valueOf(total);

        String monto_formato = metodosGlobales.formato_monto(monto);
        String comision_interbancaria_formato = metodosGlobales.formato_monto(comision_interbancaria);
        String comision_cliente_formato = metodosGlobales.formato_monto(comision_cliente);
        String comision_abono_formato = metodosGlobales.formato_monto(comision_abono);
        String itf_formato = metodosGlobales.formato_monto(itf);

        String[] confirmar = new String[11];

        confirmar[0] = numero_cuenta_origen;
        confirmar[1] = numero_cci;
        confirmar[2] = cuenta_otrobanco_nombre;
        confirmar[3] = valor_moneda;
        confirmar[4] = monto_formato;
        confirmar[5] = comision_interbancaria_formato;
        confirmar[6] = comision_cliente_formato;
        confirmar[7] = comision_abono_formato;
        confirmar[8] = total_val;
        confirmar[9] = afiliacion_tipo.get(2);
        confirmar[10] = itf_formato;
        request.getSession().setAttribute("confirmacion_otro_banco", confirmar);

        String[] coordenadas = new String[2];
        String codigo_solicitud = "";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;
        String columna = "";
        String fila = "";

        if (flag_servlet == false || estado_frecuente == false) {

            if (tipoAfiliacion.equals("A")) {

                coordenadas[0] = afiliacion_tipo.get(0);
                coordenadas[1] = afiliacion_tipo.get(1);

                columna = afiliacion_tipo.get(0);
                fila = afiliacion_tipo.get(1);

                // web/login/funciones/confirmacion.jsp
                result = 0 + "%%%" + "confirmacion";
            } else {

                Result resultadoWS2 = service.clave_dinamica(numero_tarjeta, codigo_sub_menu, monto, token);
                if (resultadoWS2.getTipoMensaje() == 0) {
                    List dinamica = (LinkedList) metodosGlobales.read2(resultadoWS2.getResultado());

                    codigo_solicitud = (String) dinamica.get(0);

                    coordenadas[0] = "";
                    coordenadas[1] = "";

                    columna = "";
                    fila = "";
                    // web/login/funciones/confirmacion.jsp
                    result = resultadoWS2.getTipoMensaje() + "%%%" + "confirmacion";

                } else {
                    result = resultadoWS2.getTipoMensaje() + "%%%" + resultadoWS2.getDescripcion();
                }
            }
            request.getSession().setAttribute("flag_clave", tipoAfiliacion);

        } else {
            // web/login/funciones/confirmacion.jsp
            result = 0 + "%%%" + "confirmacion";
        }
        request.getSession().setAttribute("coordenadas", coordenadas);
        request.getSession().setAttribute("columna", columna);
        request.getSession().setAttribute("fila", fila);
        request.getSession().setAttribute("codigo_solicitud", codigo_solicitud);

        request.getSession().setAttribute("opcion_elegida", opcion_elegida);
        request.getSession().setAttribute("celular_correo", celular_correo);
        request.getSession().setAttribute("medio_ingresado", medio_ingresado);
        request.getSession().setAttribute("codigo_solicitud", codigo_solicitud);
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void constancia_transferencia_cuenta_otro_banco(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaOtroBancoServlet-constancia_transferencia_cuenta_otro_banco");

        String numero_cuenta_origen = (String) request.getSession().getAttribute("numero_cuenta_origen");
        String estado_titular = (String) request.getSession().getAttribute("estado_titular");
        String monto = (String) request.getSession().getAttribute("monto");
        String cuenta_destino_interbancaria_val = (String) request.getSession().getAttribute("numero_cci");
        String tipo_mon_ori = (String) request.getSession().getAttribute("tipo_mon_ori");
        String comision_interbancaria = (String) request.getSession().getAttribute("comision_interbancaria");
        String comision_cliente = (String) request.getSession().getAttribute("comision_cliente");
        String tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String afecta_comision = (String) request.getSession().getAttribute("opcion_elegida");
        String comision_abono = (String) request.getSession().getAttribute("comision_abono");
        String tipo_medio = (String) request.getSession().getAttribute("celular_correo");
        String valor_medio = (String) request.getSession().getAttribute("medio_ingresado");
        String itf = (String) request.getSession().getAttribute("itf");

        String ope_frecuente = request.getParameter("ope_frecuente");//luz de casa
        String estado_ope = request.getParameter("estado_ope");//true or false
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_moneda = request.getParameter("codigo_moneda");
        String clave = request.getParameter("clave");
        String columna = (String) request.getSession().getAttribute("columna");
        String fila = (String) request.getSession().getAttribute("fila");
        String codigo_solicitud = (String) request.getSession().getAttribute("codigo_solicitud");

        String tipo_afiliacion = tipoAfiliacion;

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_sub_menu = "AP03";

        String clave_dinamica = "";

        String clave_coordenada = "";

        String numero_columna = columna;

        String numero_fila = fila;

        String result = "";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        if (flag_servlet == false || estado_frecuente == false) {

            if (tipo_afiliacion.equals("D")) {

                String clave_dinamica2 = request.getParameter("clave");
                clave_dinamica = clave_dinamica2;

            } else if (tipo_afiliacion.equals("A")) {

                String clave_coordenada2 = request.getParameter("clave");
                clave_coordenada = clave_coordenada2;

            }
        }
        CuentaOtroBancoService service = new CuentaOtroBancoService();
        Result resultadoWS = service.constancia_cuenta_otro_banco_destino_detalle(
                numero_cuenta_origen,
                cuenta_destino_interbancaria_val,
                tipo_mon_ori,
                monto,
                comision_interbancaria,
                comision_cliente,
                estado_titular,
                estado_ope,
                ope_frecuente,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                codigo_solicitud,
                clave_coordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                estado_frecuente.toString(),
                afecta_comision,
                comision_abono,
                itf,
                tipo_medio,
                valor_medio
        );

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista_contancia_otro_banco = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            request.getSession().setAttribute("lista_contancia", lista_contancia_otro_banco);

            
             // web/login/funciones/constancia.jsp
            
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            request.getSession().setAttribute("inputs_AP03", null);

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", lista_contancia_otro_banco.get(0).toString())
                    .addObject("fecha", lista_contancia_otro_banco.get(1).toString())
                    .addObject("hora", lista_contancia_otro_banco.get(2).toString())
                    .addObject("cuenta", lista_contancia_otro_banco.get(3).toString())
                    .addObject("destino", lista_contancia_otro_banco.get(4).toString())
                    .addObject("banco", lista_contancia_otro_banco.get(5).toString())
                    .addObject("monedamonto", lista_contancia_otro_banco.get(6).toString() + " " + metodosGlobales.formato_monto(lista_contancia_otro_banco.get(7).toString()))
                    .addObject("comisioninterbancaria", lista_contancia_otro_banco.get(8).toString())
                    .addObject("comisioncliente", lista_contancia_otro_banco.get(9).toString())
                    .addObject("totaloperacion", lista_contancia_otro_banco.get(10).toString())
                    .addObject("comisionabono", lista_contancia_otro_banco.get(11).toString())
                    .addObject("codigosolicitud", lista_contancia_otro_banco.get(12).toString())
                    .addObject("itf", lista_contancia_otro_banco.get(13).toString());

            request.getSession().setAttribute("compartir_cuenta_otros", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
