package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.OperFrecuentesService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "OperFrecuentesServlet", urlPatterns = {"/OperFrecuentesServlet"})
public class OperFrecuentesServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {

            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "./";
                    out.print(result);
                }

            } else {
                request.getSession().setAttribute("rutaServlet", "OperFrecuentesServlet");

                switch (accion) {
                    case "listar_oper_frec":
                        listar_oper_frec(request, response);
                        break;
                    case "eliminar_oper_frec":
                        eliminar_oper_frec(request, response);
                        break;
                    case "ultra_instinto":
                        ultra_instinto(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void listar_oper_frec(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String result = "";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar
        OperFrecuentesService cps = new OperFrecuentesService();
        Result resultadoWS = cps.listar_oper_frecuentes(numero_tarjeta, token);
        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
            request.getSession().setAttribute("opera_frecuentes", lista);
            request.getSession().setAttribute("cantidad_oper", lista.length);
            // web/login/funciones/operaciones_frecuentes.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "operacionesFrecuentes";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void eliminar_oper_frec(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String result = "";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String array[] = request.getParameterValues("array[]");
        OperFrecuentesService cps = new OperFrecuentesService();

        Result resultadoWS = cps.eliminar_operacion_frecuente(numero_tarjeta, array, token);
        if (resultadoWS.getTipoMensaje() == 0) {

            listar_oper_frec(request, response);
        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void ultra_instinto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//Transferencias propias     AP01
//Transferencias Terceros    AP02
//Transferencias a Bancos    AP03
//Pago de tarjeta crédito    AP04
//Pago de créditos propios   AP05
//Pago de créditos terceros  AP06
//Pago de servicios          AP07
//Pago de Instituciones      AP08
//Envió giro                 AP09
//Recargas                   AP10
//Registro estado frecuente  AP12
//Registro datos perfil      AP13

        String result = "";
        Object[] array_list = (Object[]) request.getSession().getAttribute("opera_frecuentes");
        String codigo_operacion = request.getParameter("codigo_operacion");
        String constrolador = "";
        String accion = "";
        JsonObjectWaka data = new JsonObjectWaka();
        boolean flag = false;
        for (Object objects : array_list) {
            List sesion = (List) objects;

            if (codigo_operacion.compareTo(sesion.get(0).toString()) == 0) {

                switch (sesion.get(1).toString()) {
                    case "AP10":

                        accion = "listar_operadores";
                        constrolador = "PagosRecargasServlet";

                        request.getSession().setAttribute("inputs_AP10", sesion);
                        flag = true;
                        break;
                    case "AP04":

                        accion = "listar_banco_origen";
                        constrolador = "RegistroPagoCreditoServlet";

                        request.getSession().setAttribute("inputs_AP04", sesion);
                        flag = true;
                        break;
                    case "AP09":

                        accion = "listar_agencias";
                        constrolador = "EnvioGirosServlet";

                        request.getSession().setAttribute("inputs_AP09", sesion);
                        flag = true;
                        break;
                    case "AP06":

                        accion = "validar_cuenta_credito";
                        constrolador = "PagoCreditoTerceroServlet";

                        request.getSession().setAttribute("inputs_AP06", sesion);
                        flag = true;
                        break;
                    case "AP05":

                        accion = "listar_cuotas";
                        constrolador = "PagoCreditoPropioServlet";

                        request.getSession().setAttribute("inputs_AP05", sesion);
                        flag = true;
                        break;
                    case "AP03":

                        accion = "ingresa_monto_propia_otro_banco";
                        constrolador = "CuentaOtroBancoServlet";

                        request.getSession().setAttribute("inputs_AP03", sesion);
                        flag = true;
                        break;
                    case "AP02":

                        accion = "ingresa_monto_propia_tercero";
                        constrolador = "CuentaTerceroServlet";

                        request.getSession().setAttribute("inputs_AP02", sesion);
                        flag = true;
                        break;
                    case "AP01":

                        accion = "ingresa_monto_propia";
                        constrolador = "CuentaPropiaServlet";

                        request.getSession().setAttribute("inputs_AP01", sesion);
                        flag = true;
                        break;
                    case "AP08":

                        accion = "validarClienteServicio";
                        constrolador = "PagoInstitucionesServlet";

                        request.getSession().setAttribute("inputs_AP08", sesion);
                        flag = true;
                        break;
                    case "AP07":

                        accion = "validarClienteServicio";
                        constrolador = "PagoServicioServlet";

                        request.getSession().setAttribute("inputs_AP07", sesion);
                        flag = true;
                        break;

                }

                break;
            }

        }
        if (flag) {
            result = "0" + "%%%" + accion + "%%%" + constrolador;
            request.getSession().setAttribute("FLAG_OP_SERVLET", true);
        } else {
            result = "1" + "%%%" + "No se encontró operación frecuente";
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
