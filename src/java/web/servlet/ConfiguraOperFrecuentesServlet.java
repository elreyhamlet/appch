package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.ConfigurarOperFrecuentesService;
import service.PagoCreditoTerceroService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "ConfiguraOperFrecuentesServlet", urlPatterns = {"/ConfiguraOperFrecuentesServlet"})
public class ConfiguraOperFrecuentesServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {

            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

                request.getSession().setAttribute("rutaServlet", "ConfiguraOperFrecuentesServlet");
                switch (accion) {

                    case "registrar":
                        registrar(request, response);
                        break;
                    case "operaciones_frecuentes":
                        configura_per_frec(request, response);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    

    protected void configura_per_frec(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        String result = "";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();
        String codigo_sub_menu = "AP12";
        String tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipoAfiliacion);
        boolean flag = true;

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;

        if (flag_servlet == false || estado_frecuente == false) {
            if (tipoAfiliacion.compareTo("D") == 0) {

                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, "0", token);

                if (r.getTipoMensaje() == 0) {

                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("frec_codigoSolicitud", codigoSolicitud.get(0));
                    System.out.println(codigoSolicitud.get(0));
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipoAfiliacion.compareTo("A") == 0) {
                request.getSession().setAttribute("frec_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("frec_fila", coordenada_clave.get(1));
            }
            request.getSession().setAttribute("flag_clave", tipoAfiliacion);
        }
        if (flag) {
            ConfigurarOperFrecuentesService cps = new ConfigurarOperFrecuentesService();
            Result resultadoWS = cps.retornar_estado_frecuente(numero_tarjeta, token);
            if (resultadoWS.getTipoMensaje() == 0) {
                List datos = (List) metodosGlobales.read2(resultadoWS.getResultado());
                request.getSession().setAttribute("configura_oper", datos);
                request.getSession().setAttribute("configura_oper_men", coordenada_clave.get(2));

                // 
                result = resultadoWS.getTipoMensaje() + "%%%" + "operaciones_recuentes";

            } else {
                result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
            }
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String result;

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String estadoFrecuente = request.getParameter("check");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String numero_columna = "";
        String numero_fila = "";
        String ip_publica_movil =  MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP12";
        String header = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_solicitud = "";
        Object obj_flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_flag;
        if (flag_servlet == false || estado_frecuente == false) {

            if (tipo_afiliacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigo_solicitud = (String) request.getSession().getAttribute("frec_codigoSolicitud");
            } else if (tipo_afiliacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("frec_columna");
                numero_fila = (String) request.getSession().getAttribute("frec_fila");;
            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);

        }
        ConfigurarOperFrecuentesService service = new ConfigurarOperFrecuentesService();

        Result resultadoWS = service.registrar_estado(
                numero_tarjeta,
                estadoFrecuente,
                tipo_afiliacion,
                codigo_cliente,
                clave_dinamica,
                codigo_solicitud,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                header,
                estado_frecuente.toString());

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());
            String tipoOperacion = lista.get(0).toString();
            String fechaOperacion = lista.get(1).toString();
            String horaOperacion = lista.get(2).toString();
            String estadoFrecuentes = lista.get(3).toString();

            Object[] datos = {tipoOperacion, fechaOperacion, horaOperacion, estadoFrecuentes};
            request.getSession().setAttribute("constancia_oper_frecuente", datos);
            request.getSession().setAttribute("r_estado_frecuente", estadoFrecuente);
            
           // constanciaweb/login/funciones/constancia.jsp

            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
