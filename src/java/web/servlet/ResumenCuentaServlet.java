package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.ResumenCuentaService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "ResumenCuentaServlet", urlPatterns = {"/ResumenCuentaServlet"})
public class ResumenCuentaServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        metodosGlobales.validar_dispositivo(request, response);

        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {
            if (accion.equals("listar_cuentas")) {
                listar_cuentas(request, response);
            }
        }

    }

    protected void listar_cuentas(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String result = "";
        String numero_tarjeta = (String) req.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) req.getSession().getAttribute("r_tokenAcceso");
        boolean flag = true;
        ResumenCuentaService service = new ResumenCuentaService();
        Result resultadoWSAhorro = service.listar_cuenta_ahorro(numero_tarjeta, token);
        if (resultadoWSAhorro.getTipoMensaje() == 0) {
            List<Object> lista = metodosGlobales.read(resultadoWSAhorro.getResultado());

            List<Object[]> array = new LinkedList<>();

            int cont = 0;
            for (int i = 0; i < lista.size(); i++) {
                Object[] arrayObjetos = (Object[]) lista.get(i);

                for (Object obj1 : arrayObjetos) {
                    List<Object> lis1 = (List<Object>) obj1;
                    int c = 0;

                    Object[] obj = null;
                    for (Object object1 : lis1) {

                        List<Object> lis2 = (List<Object>) object1;

                        Object contador = String.valueOf(cont);
                        cont++;
                        List<Object> lis4 = new LinkedList<>();
                        lis4.add(contador);
                        lis2.add(lis4);

                        obj = new Object[lis2.size()];

                        for (Object object2 : lis2) {
                            List<Object> lis3 = (List<Object>) object2;
                            for (Object object3 : lis3) {
                                obj[c++] = object3;
                            }
                        }
                    }

                    array.add(obj);

                }

            }

            req.getSession().setAttribute("lista_ahorro_cta", array);

        } else {

        }
        if (flag) {
            Result resultadoWsCredito = service.listar_cuenta_credito(numero_tarjeta, token);

            if (resultadoWsCredito.getTipoMensaje() == 0) {

                List<Object[]> lista = new LinkedList<>();

                lista = metodosGlobales.read3(resultadoWsCredito.getResultado());

                req.getSession().setAttribute("lista_credito_cta", lista);

                req.getSession().setAttribute("pago_propio_credito", lista);
            }
// web/login/resumen_cuentas.jsp
            result = "0" + "%%%" + "resumenCuentas";

        }

        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
