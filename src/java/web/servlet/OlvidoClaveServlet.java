package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.OlvidoClaveService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "OlvidoClaveServlet", urlPatterns = {"/OlvidoClaveServlet"})
public class OlvidoClaveServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        
            request.getSession().setAttribute("rutaServlet", "OlvidoClaveServlet");

            if (accion.equals("validarClavePin")) {
                validarClavePin(request, response);
            }
        
    }

    protected void validarClavePin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String numero_tarjeta = request.getParameter("numerotarjeta");
        String clave_pin = request.getParameter("clave");
        String ippublicamovil = MetodosGlobales.getClientIpAddr(request);
        String clave_web = request.getParameter("nuevaClave");

        OlvidoClaveService claveService = new OlvidoClaveService();
        Result resultadoWS = claveService.validar_clave_web(numero_tarjeta, clave_pin, ippublicamovil);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {
            List res = (List) metodosGlobales.read2(resultadoWS.getResultado());
            String token = res.get(0).toString();
            Result resultado = claveService.cambiarClave(numero_tarjeta, clave_web, ippublicamovil, "Bearer "+token);

            if (resultado.getTipoMensaje() == 0) {

                Object obj = metodosGlobales.read2(resultado.getResultado());
                request.getSession().setAttribute("constancia", obj);
                
                // web/login/funciones/constancia.jsp
                result = resultado.getTipoMensaje() + "%%%" + "constancia";

                
                
                
            } else {
                result = resultado.getTipoMensaje() + "%%%" + resultado.getDescripcion();
            }
        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
