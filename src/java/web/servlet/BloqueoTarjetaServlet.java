package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.BloqueoTarjetaService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "BloqueoTarjetaServlet", urlPatterns = {"/BloqueoTarjetaServlet"})
public class BloqueoTarjetaServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        metodosGlobales.validar_dispositivo(request, response);

        String accion = request.getParameter("accion");

        request.getSession().setAttribute("rutaServlet", "BloqueoTarjetaServlet");
        if (accion.equals("bloquear_tarjeta")) {
            bloquear_tarjeta(request, response);
        }
        if (accion.equals("listar_combos")) {
            listar_combos(request, response);
        }

    }

    protected void bloquear_tarjeta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String clave_pin = request.getParameter("clavePin");
        String ippublicamovil = MetodosGlobales.getClientIpAddr(request);
        String documento_cliente = request.getParameter("documentoCliente");
        String codigo_descripcion_bloqueo = request.getParameter("codigoDescripcionBloqueo");
        String descripcion_bloqueo = request.getParameter("descripcionBloqueo");
        BloqueoTarjetaService tarjetaService = new BloqueoTarjetaService();
        String result;
        Result resultadoWS = tarjetaService.bloquearTarjeta(
                clave_pin,
                ippublicamovil,
                documento_cliente,
                codigo_descripcion_bloqueo,
                descripcion_bloqueo
        );

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());
            Object[] obj = new Object[6];

            int c = 0;
            for (Object object : lista) {
                obj[c++] = object;
            }
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = dt.parse(obj[1].toString());
            } catch (ParseException ex) {
                Logger.getLogger(BloqueoTarjetaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");

            request.getSession().setAttribute("constancia_bloqueo_codigo_operacion", obj[0]);
            request.getSession().setAttribute("constancia_bloqueo_fecha_operacion", dt1.format(date));
            request.getSession().setAttribute("constancia_bloqueo_hora_operacion", obj[2]);
            request.getSession().setAttribute("constancia_bloqueo_numero_tarjeta", obj[3]);
            request.getSession().setAttribute("constancia_bloqueo_codigo_descripcion", obj[4]);
            request.getSession().setAttribute("constancia_bloqueo_mensaje_cmac", obj[5]);

            // "web/login/funciones/constancia.jsp"
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", (String) obj[0])
                    .addObject("fecha", (String) obj[1])
                    .addObject("hora", (String) obj[2].toString())
                    .addObject("numero_tarjeta", (String) obj[3].toString())
                    .addObject("codigo_descripcion", (String) obj[4])
                    .addObject("mensaje", (String) obj[5]);

            request.getSession().setAttribute("compartir_bloqueo", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_combos(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BloqueoTarjetaService tarjetaService = new BloqueoTarjetaService();
        Result resultadoWSAhorro = tarjetaService.listar_tipo_documento();
        String result = "";
        if (resultadoWSAhorro.getTipoMensaje() == 0) {

            List<Object> lista = metodosGlobales.read(resultadoWSAhorro.getResultado());
            List<Object[]> array = new LinkedList<>();
            for (int i = 0; i < lista.size(); i++) {
                Object[] arrayObjetos = (Object[]) lista.get(i);

                for (Object obj1 : arrayObjetos) {
                    List<Object> lis1 = (List<Object>) obj1;
                    int c = 0;
                    Object[] obj = null;
                    for (Object object1 : lis1) {

                        List<Object> lis2 = (List<Object>) object1;
                        obj = new Object[lis2.size()];

                        for (Object object2 : lis2) {
                            List<Object> lis3 = (List<Object>) object2;
                            for (Object object3 : lis3) {
                                obj[c++] = object3;
                            }
                        }
                    }
                    array.add(obj);

                }

            }

            req.getSession().setAttribute("lista_tipo_documento", array);

        }
        Result resultadoDescripcionBloqueo = tarjetaService.listar_descripcion_bloqueo();

        if (resultadoDescripcionBloqueo.getTipoMensaje() == 0) {

            List<Object> lista = metodosGlobales.read(resultadoDescripcionBloqueo.getResultado());
            List<Object[]> array = new LinkedList<>();
            for (int i = 0; i < lista.size(); i++) {
                Object[] arrayObjetos = (Object[]) lista.get(i);

                for (Object obj1 : arrayObjetos) {
                    List<Object> lis1 = (List<Object>) obj1;
                    int c = 0;
                    Object[] obj = null;
                    for (Object object1 : lis1) {

                        List<Object> lis2 = (List<Object>) object1;
                        obj = new Object[lis2.size()];

                        for (Object object2 : lis2) {
                            List<Object> lis3 = (List<Object>) object2;
                            for (Object object3 : lis3) {
                                obj[c++] = object3;
                            }
                        }
                    }
                    array.add(obj);

                }

            }

            req.getSession().setAttribute("lista_descripcion_bloqueo", array);

        }
        // "web/menu_publico/bloquear_tarjeta.jsp"
        result = "0" + "%%%" + "bloqueoTarjeta";
        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
