package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.ConfigurarTarjetaService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "ConfiguraTarjetaServlet", urlPatterns = {"/ConfiguraTarjetaServlet"})
public class ConfiguraTarjetaServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        metodosGlobales.validar_dispositivo(request, response);

        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");

        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                // "./"
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {

            request.getSession().setAttribute("rutaServlet", "ConfiguraTarjetaServlet");

            switch (accion) {

                case "registrar":
                    registrar(request, response);
                    break;
                case "operaciones_frecuentes":
                    operaciones_frecuentes(request, response);
                    break;
                case "configura_tarjeta":
                    configura_tarjeta(request, response);
                    break;
                case "otras_opciones":
                    otras_opciones(request, response);
                    break;

                default:
                    break;
            }

        }
    }

    protected void operaciones_frecuentes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //!!!
        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {

            response.sendRedirect("./");

        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("web/login/resumen_cuentas/otras_opciones/operaciones_frecuentes.jsp");
            dispatcher.forward(request, response);

        }
    }

    protected void configura_tarjeta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String result = "";
        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            result = "0" + "%%%" + "index";

        } else {
            ConfigurarTarjetaService cps = new ConfigurarTarjetaService();
            String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
            String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
            String token = (String) request.getSession().getAttribute("r_tokenAcceso");

            Result resultadoWS = cps.retornar_estado_fuera_pais(numero_tarjeta, codigo_cliente, token);

            if (resultadoWS.getTipoMensaje() == 0) {
                List datos = (List) metodosGlobales.read2(resultadoWS.getResultado());
                request.getSession().setAttribute("configuratarjeta", datos);
                // web/login/resumen_cuentas/otras_opciones/configura_tarjeta.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "configuraTarjeta";

            } else {
                result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
            }
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String result;
        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            result = "0" + "%%%" + "index";

        } else {
            String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
            String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
            String estado_activo_fuera = request.getParameter("estado");
            String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
            String codigo_sub_menu = "AP12";
            String token = (String) request.getSession().getAttribute("r_tokenAcceso");

            ConfigurarTarjetaService service = new ConfigurarTarjetaService();

            Result resultadoWS = service.registrar_estado(
                    numero_tarjeta,
                    codigo_cliente,
                    estado_activo_fuera,
                    ip_publica_movil,
                    codigo_sub_menu,
                    token);

            if (resultadoWS.getTipoMensaje() == 0) {

                List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());
                String fecha = lista.get(0).toString();
                String hora = lista.get(1).toString();
                String numeroTarjetas = lista.get(2).toString();
                String estadoActivoFueras = lista.get(3).toString();
                String emailCliente = lista.get(4).toString();

                Object[] datos = {fecha, hora, numeroTarjetas, estadoActivoFueras, emailCliente};
                request.getSession().setAttribute("constancia_estado_frecuente", datos);
                // web/login/funciones/constancia.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            } else {

                result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

            }
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void otras_opciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";
// web/login/resumen_cuentas/otras_opciones.jsp
        result = "0" + "%%%" + "otrasOpciones";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
