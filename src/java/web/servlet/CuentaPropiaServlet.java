package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "CuentaPropiaServlet", urlPatterns = {"/CuentaPropiaServlet"})
public class CuentaPropiaServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);

        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {
            switch (accion) {
                case "listar_cuenta_propia":
                    listar_cuenta_propia(request, response);
                    break;
                case "listar_cuenta_propia_destino":
                    listar_cuenta_propia_destino(request, response);
                    break;
                case "confirmar_transferencia_cuenta_propia":
                    confirmar_transferencia_cuenta_propia(request, response);
                    break;
                case "ingresa_monto_propia":
                    ingresa_monto_propia(request, response);
                    break;
                case "constancia_transferencia_cuenta_propia":
                    constancia_transferencia_cuenta_propia(request, response);
                    break;
                case "transferencia":
                    transferencia(request, response);
                    break;
                default:
                    break;
            }

        }
    }

    protected void listar_cuenta_propia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaPropiaServlet-listar_cuenta_propia");

        String tipo_operacion_string = "P";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String result = "";
        CuentaPropiaService service = new CuentaPropiaService();
        Result resultadoWS = service.listar_cuenta_propia_detalle(codigo_cliente, tipo_operacion_string, token);

        if (resultadoWS.getTipoMensaje() == 0) {
            List<Object[]> lista_cuenta = new LinkedList<>();
            lista_cuenta = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("elegir_cuenta_cuentas_propias", lista_cuenta);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_cuenta_propia_destino(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        List<Object[]> elegir_cuenta_destino_cuentas_propias = new LinkedList<>();
        elegir_cuenta_destino_cuentas_propias.clear();
        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP01", null);

        request.getSession().setAttribute("rutaServlet", "CuentaPropiaServlet-listar_cuenta_propia_destino");

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        List<Object[]> lista_cuenta_destino = new LinkedList<>();
        array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_cuentas_propias");

        String indicador_cuenta = request.getParameter("indicador_cuenta");
        Integer ind = Integer.parseInt(indicador_cuenta);
        Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
        String numero_cuenta = (String) cuenta_total[0];
        String tipo_mon_ori = (String) cuenta_total[3];

        String result = "";
        String tipo_operacion_string = "P";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");

        CuentaPropiaService service = new CuentaPropiaService();
        Result resultadoWS = service.listar_cuenta_propia_destino_detalle(codigo_cliente, tipo_operacion_string, token);
        LinkedList<Object[]> linkedlis = new LinkedList<>();

        if (resultadoWS.getTipoMensaje() == 0) {

            lista_cuenta_destino = metodosGlobales.read3(resultadoWS.getResultado());

            int cont = 0;
            Object[] arrayObjetos2 = null;
            for (int i = 0; i < lista_cuenta_destino.size(); i++) {
                Object[] arrayObjetos = (Object[]) lista_cuenta_destino.get(i);

                String var2 = String.valueOf(arrayObjetos[0]);

                    
                if (!var2.equals(numero_cuenta)) {

                    elegir_cuenta_destino_cuentas_propias.add(arrayObjetos);

                } else {

                }

            }

            request.getSession().setAttribute("elegir_cuenta_destino_cuentas_propias", elegir_cuenta_destino_cuentas_propias);
            request.getSession().setAttribute("lista_cuenta_destino", lista_cuenta_destino);
            request.getSession().setAttribute("numero_cuenta_pro", numero_cuenta);
            request.getSession().setAttribute("tipo_mon_ori_cuenta_pro", tipo_mon_ori);
            
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void ingresa_monto_propia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaPropiaServlet-ingresa_monto_propia");
        
        String type=metodosGlobales.validar_dispositivo_teclado(request, response);
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        List<Object[]> array_lista_cuenta_des = new LinkedList<>();
        String numero_cuenta_destino = "";

        if (flag_servlet == false) {
            array_lista_cuenta_des = (List<Object[]>) request.getSession().getAttribute("lista_cuenta_destino");
            String indicador_cuenta = request.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_des.get(ind);
            numero_cuenta_destino = (String) cuenta_total[0];
        } else {
           List sesion=  (List) request.getSession().getAttribute("inputs_AP01");
           request.getSession().setAttribute("numero_cuenta_pro",sesion.get(3).toString());
            numero_cuenta_destino = sesion.get(6).toString();
           request.getSession().setAttribute("numero_cuenta_destino",sesion.get(6).toString());
            request.getSession().setAttribute("tipo_mon_ori_cuenta_pro", sesion.get(4).toString());
        }
        String result = "";
        String tipo_mon_ori = (String) request.getSession().getAttribute("tipo_mon_ori_cuenta_pro");
        String tipo_cambio_compra = (String) request.getSession().getAttribute("tipo_cambio_compra");
        String tipo_cambio_venta = (String) request.getSession().getAttribute("tipo_cambio_venta");

        String[] tipo_cambio = new String[2];;
        tipo_cambio[0] = tipo_cambio_compra;
        tipo_cambio[1] = tipo_cambio_venta;
        CuentaPropiaService service = new CuentaPropiaService();
        Result resultadoWS = service.listar_moneda();
        Object[] dat_mon;
        String[] objsol = null;
        String[] objsol2 = new String[3];
        String[] objdol = new String[3];

        if (resultadoWS.getTipoMensaje() == 0) {

            dat_mon = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
            for (Object dato : dat_mon) {

                LinkedList<String> linkedlist = (LinkedList<String>) dato;

                objsol = new String[linkedlist.size()];
                objdol = new String[linkedlist.size()];

                objsol[0] = linkedlist.get(0);
                objsol[1] = linkedlist.get(1);
                objsol[2] = linkedlist.get(2);

                String valor1 = objsol[0];
                String valor2 = objsol[1];
                String valor3 = objsol[2];

                if (valor1.equals("1")) {

                    objsol2[0] = valor1;
                    objsol2[1] = valor2;
                    objsol2[2] = valor3;
                } else if (valor1.equals("2")) {

                    objdol[0] = valor1;
                    objdol[1] = valor2;
                    objdol[2] = valor3;
                }

            }

            request.getSession().setAttribute("dat_mon_sol", objsol2);
            request.getSession().setAttribute("dat_mon_dol", objdol);
            request.getSession().setAttribute("numero_cuenta_destino", numero_cuenta_destino);
            request.getSession().setAttribute("tipo_cambio", tipo_cambio);
            request.getSession().setAttribute("tipo_mon_ori", tipo_mon_ori);
            request.getSession().setAttribute("type", type);
            // web/login/resumen_cuentas/ejecuta_transferencia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "ejecutaTransferencia";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmar_transferencia_cuenta_propia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaPropiaServlet-confirmar_transferencia_cuenta_propia");
        String numero_cuenta = (String) request.getSession().getAttribute("numero_cuenta_pro");
        String numero_cuenta_destino  = (String) request.getSession().getAttribute("numero_cuenta_destino");
        String monto = request.getParameter("monto");

        String codigo_moneda = request.getParameter("codigo_moneda");
        String result = "";

        String[] confirmar = new String[4];

        confirmar[0] = numero_cuenta;
        confirmar[1] = numero_cuenta_destino;
        confirmar[2] = codigo_moneda;
        confirmar[3] = monto;
        request.getSession().setAttribute("confirmacion_cuentas_propias", confirmar);
        request.getSession().setAttribute("monto", monto);

        
        //  web/login/funciones/confirmacion.jsp
        result = "0" + "%%%" + "confirmacion";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void constancia_transferencia_cuenta_propia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaPropiaServlet-constancia_transferencia_cuenta_propia");

        String numero_cuenta = (String) request.getSession().getAttribute("numero_cuenta_pro");
        String numero_cuenta_destino  = (String) request.getSession().getAttribute("numero_cuenta_destino");
        String monto  = (String) request.getSession().getAttribute("monto");

        String ope_frecuente = request.getParameter("ope_frecuente");//luz de casa
        String estado_ope = request.getParameter("estado_ope");//true or false
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_sub_menu = "AP01";

        String result = "";

        List lista_contancia;

        CuentaPropiaService service = new CuentaPropiaService();
        Result resultadoWS = service.constancia_cuenta_propia_destino_detalle(
                numero_cuenta,
                numero_cuenta_destino,
                monto, estado_ope,
                ope_frecuente,
                ip_publica_movil,
                numero_tarjeta,
                codigo_cliente,
                codigo_sub_menu,
                token);

        if (resultadoWS.getTipoMensaje() == 0) {

            lista_contancia = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            request.getSession().setAttribute("lista_contancia", lista_contancia);

            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", lista_contancia.get(0).toString())
                    .addObject("fecha", lista_contancia.get(1).toString())
                    .addObject("hora", lista_contancia.get(2).toString())
                    .addObject("cuenta", lista_contancia.get(3).toString())
                    .addObject("destino", lista_contancia.get(4).toString())
                    .addObject("monedamonto", lista_contancia.get(5).toString() + " " + metodosGlobales.formato_monto(lista_contancia.get(6).toString()));

            request.getSession().setAttribute("compartir_cuenta_propios", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void transferencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";
  // web/login/resumen_cuentas/transferencias.jsp
        result = "0" + "%%%" + "transferencia";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
