package util;

import com.google.gson.JsonElement;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetodosGlobales {

    public static String ruta_service = new Config().getRuta();
    public static boolean flag_service = new Config().getFlag();
    public static String ruta_service_get = new Config().getRutaGet();
    public static String ruta_service_post = new Config().getRutaPost();

    public List<Object> read(JsonElement dataJson) {
        List<Object> r = new LinkedList<>();

        if (!dataJson.isJsonNull()) {
            if (dataJson.isJsonPrimitive()) {
                r.add(dataJson.getAsJsonPrimitive().getAsString());
            } else if (dataJson.isJsonObject()) {

                Set<Map.Entry<String, JsonElement>> entries = dataJson.getAsJsonObject().entrySet();
                List<Object> nuevaLista = new LinkedList<>();

                for (Map.Entry<String, JsonElement> entry : entries) {
                    nuevaLista.add(read(dataJson.getAsJsonObject().get(entry.getKey())));

                }
                r.add(nuevaLista);
            } else if (dataJson.isJsonArray()) {
                Object[] nuevoArray = new Object[dataJson.getAsJsonArray().size()];
                int i = 0;
                for (JsonElement je : dataJson.getAsJsonArray()) {
                    nuevoArray[i++] = read(je);
                }
                r.add(nuevoArray);
            }

        }

        return r;

    }

    public Object read2(JsonElement dataJson) {
        Object r = null;

        if (!dataJson.isJsonNull()) {
            if (dataJson.isJsonPrimitive()) {

                r = dataJson.getAsJsonPrimitive().getAsString();
            } else if (dataJson.isJsonObject()) {

                Set<Map.Entry<String, JsonElement>> entries = dataJson.getAsJsonObject().entrySet();
                List<Object> nuevaLista = new LinkedList<>();

                for (Map.Entry<String, JsonElement> entry : entries) {
                    nuevaLista.add(read2(dataJson.getAsJsonObject().get(entry.getKey())));

                }
                r = nuevaLista;

            } else if (dataJson.isJsonArray()) {
                Object[] nuevoArray = new Object[dataJson.getAsJsonArray().size()];
                int i = 0;
                for (JsonElement je : dataJson.getAsJsonArray()) {
                    nuevoArray[i++] = read2(je);
                }
                r = nuevoArray;

            }

        }

        return r;

    }

    public List<Object[]> read3(JsonElement dataJson) {

        List<Object> r = new LinkedList<>();

        if (!dataJson.isJsonNull()) {
            if (dataJson.isJsonPrimitive()) {
                r.add(dataJson.getAsJsonPrimitive().getAsString());
            } else if (dataJson.isJsonObject()) {

                Set<Map.Entry<String, JsonElement>> entries = dataJson.getAsJsonObject().entrySet();
                List<Object> nuevaLista = new LinkedList<>();
                for (Map.Entry<String, JsonElement> entry : entries) {
                    nuevaLista.add(read(dataJson.getAsJsonObject().get(entry.getKey())));
                }
                r.add(nuevaLista);
            } else if (dataJson.isJsonArray()) {
                Object[] nuevoArray = new Object[dataJson.getAsJsonArray().size()];
                int i = 0;
                for (JsonElement je : dataJson.getAsJsonArray()) {
                    nuevoArray[i++] = read(je);
                }
                r.add(nuevoArray);
            }

        }

        List<Object[]> array = new LinkedList<>();

        int cont = 0;
        for (int i = 0; i < r.size(); i++) {
            Object[] arrayObjetos = (Object[]) r.get(i);

            for (Object obj1 : arrayObjetos) {
                List<Object> lis1 = (List<Object>) obj1;
                int c = 0;

                Object[] obj = null;
                for (Object object1 : lis1) {

                    List<Object> lis2 = (List<Object>) object1;

                    Object contador = String.valueOf(cont);
                    cont++;
                    List<Object> lis4 = new LinkedList<>();
                    lis4.add(contador);
                    lis2.add(lis4);

                    obj = new Object[lis2.size()];

                    for (Object object2 : lis2) {
                        List<Object> lis3 = (List<Object>) object2;
                        for (Object object3 : lis3) {
                            obj[c++] = object3;
                        }
                    }
                }

                array.add(obj);

            }

        }

        return array;

    }

    public ArrayList<String> tipo_afiliacion(String tipoAfiliacion) {

        String numero_columna2 = "";
        String numero_fila2 = "";

        ArrayList<String> afiliacion = new ArrayList<>();
        if (!tipoAfiliacion.equals("D") && !tipoAfiliacion.equals("A")) {
            afiliacion.add("false");
            afiliacion.add("Para realizar esta operación Ud. necesita afiliarse a la clave dinámica o tarjeta coordenadas en cualquiera de nuestras agencias a nivel nacional");
            afiliacion.add("1");
        } else if (tipoAfiliacion.equals("D")) {

            afiliacion.add("Clave Dinámica");
            afiliacion.add("Sin Coordenadas");
            afiliacion.add("Ingrese su Clave Dinámica");

        } else if (tipoAfiliacion.equals("A")) {

            String[] arr_columna = {"A", "B", "C", "D", "E", "F", "G", "H"};
            int amount_col;

            Random r = new Random();
            amount_col = (int) (Math.random() * 8);
            numero_columna2 = arr_columna[r.nextInt(8)];
            afiliacion.add(numero_columna2);

            String[] arr_fila = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
            int amount_fil;

            Random r2 = new Random();
            amount_fil = (int) (Math.random() * 10);
            numero_fila2 = arr_fila[r2.nextInt(10)];
            afiliacion.add(numero_fila2);

            String texto_tipo = " Ingrese su clave Coordenada: " + numero_columna2 + numero_fila2;
            afiliacion.add(texto_tipo);
        }

        return afiliacion;

    }

    public String formato_monto(String x) {

        Double dx;
        dx = Double.parseDouble(x);
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.UK);
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.applyPattern("###,###,###,###,##0.00");

        String result = null;
        try {
            result = decimalFormat.format(dx);
        } catch (IllegalArgumentException e) {
        }

        return result;
    }

    public Long fecha_numero() {

        long unixTime = System.currentTimeMillis() / 1000L;

        return unixTime;

    }

    public boolean validar_dispositivo(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getHeader("User-Agent").contains("Mobile") || req.getHeader("User-Agent").contains("iPad") || req.getHeader("User-Agent").contains("iPhone")) {

            return true;
        } else {
            req.getSession().invalidate();

            return false;
    }

    }

    private static final String IPV4_REGEX
            = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
            + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
            + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
            + "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

    private static final Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);

    public static boolean isValidInet4Address(String ip) {
        if (ip == null) {
            return false;
        }

        Matcher matcher = IPV4_PATTERN.matcher(ip);

        return matcher.matches();
    }

    public static String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip) || "".equals(ip)) {

            String ip1 = request.getHeader("Proxy-Client-IP");
            String ip2 = request.getHeader("WL-Proxy-Client-IP");
            String ip3 = request.getHeader("HTTP_CLIENT_IP");
            String ip4 = request.getHeader("HTTP_X_FORWARDED_FOR");
            String ip5 = request.getRemoteAddr();
            String ipgenerica = "0.0.0.0";
            if (ip1 != null && !"".equals(ip1)) {
                ipgenerica = ip1;
            } else if (ip2 != null && !"".equals(ip2)) {
                ipgenerica = ip2;
            } else if (ip3 != null && !"".equals(ip3)) {
                ipgenerica = ip3;
            } else if (ip4 != null && !"".equals(ip4)) {
                ipgenerica = ip4;
            } else {
                ipgenerica = ip5;
            }

            ip = ipgenerica;

        } else {
            ip = "0.0.0.0";
        }

        if (isValidInet4Address(ip)) {
            return ip;
        } else {
            return ip = "0.0.0.0";
        }

    }
    
    
     public String validar_dispositivo_teclado(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getHeader("User-Agent").contains("Mobile") ) {

            return "tel";
        }else if(req.getHeader("User-Agent").contains("iPad") || req.getHeader("User-Agent").contains("iPhone")) {

            return "number";
        } else {

            return "tel";
        }

    }

}
