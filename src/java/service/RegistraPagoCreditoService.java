package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class RegistraPagoCreditoService {

    public Result registrarPagoCredito(
            String codigo_cuenta_origen,
            String codigo_banco,
            String codigo_agencia,
            String numero_tarjeta_banco,
            String codigo_moneda,
            String monto_pago,
            String estado_operacion_frecuente,
            String alias_operacion,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header,
            String codigo_solicitud,
            String autenticacion_frecuente,
            String afecta_comision,
            String comision_abono,
            String tipo_medio,
            String valor_medio,
            String itf
    ) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/CamaraComercio/RegistrarPagoTarjeta";
        }
        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka j1 = new JsonObjectWaka();
        JsonObjectWaka confirmacionAbono = new JsonObjectWaka();

        j1.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);
        JsonObjectWaka j2 = new JsonObjectWaka();

        j2.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoBanco", codigo_banco)
                .addObject("codigoAgencia", codigo_agencia)
                .addObject("numeroTarjetaBanco", numero_tarjeta_banco)
                .addObject("codigoMoneda", codigo_moneda)
                .addObject("montoPago", monto_pago)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);
        
        confirmacionAbono.addObject("afectaComision", afecta_comision)
                .addObject("comisionAbono", comision_abono)
                .addObject("tipoMedio", tipo_medio)
                .addObject("ValorMedio", valor_medio);

        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosAuditoria", j2).addObject("datosValidacion", j1).addObject("confirmacionAbono", confirmacionAbono).addObject("itf", itf)).addObject("url", "/CamaraComercio/RegistrarPagoTarjeta");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);

        } else {
            jo.addObject("datosAuditoria", j2).addObject("datosValidacion", j1).addObject("confirmacionAbono", confirmacionAbono).addObject("itf", itf);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }

        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result listar_banco_origen() {

        String url;
        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/TarjetaCredito/ListarBancoOrigen";
        } else {
            url = MetodosGlobales.ruta_service + "/TarjetaCredito/ListarBancoOrigen";
        }

        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result retornar_comision(
            String codigo_banco,
            String codigo_agencia,
            String codigo_moneda,
            String monto_transferencia,
            String numero_tarjeta,
            String codigo_cuenta_origen,
            String token
    ) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/CamaraComercio/RetornarComisionTransferencia";
        }

        JsonObjectWaka jo = new JsonObjectWaka();
        jo.addObject("codigoBanco", codigo_banco)
                .addObject("codigoAgencia", codigo_agencia)
                .addObject("codigoMoneda", codigo_moneda)
                .addObject("montoTransferencia", monto_transferencia)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCuentaOrigen", codigo_cuenta_origen);
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/CamaraComercio/RetornarComisionTransferencia");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), token);
        }

        int tipoMensaje = 1;

//        String result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        JsonElement contenido = null;
        String mensaje = "";
        if (result[0].equals("0")) {
            JsonParser parser = new JsonParser();
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }

        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
