
package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class CuentaTerceroService {
    
    public Result listar_cuenta_propia_tercer_detalle(String codigo_cliente,String tipo_operacion, String header) {
        String url ;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentaOrigen";
        }
        
        int tipoMensaje = 1;
        String mensaje ;
       
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoCliente", codigo_cliente)
                .addObject("tipoOperacion", tipo_operacion);

        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Cuenta/ListarCuentaOrigen");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }   
    public Result listar_moneda_tercero() {
        String url;
        
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_get + "/General/ListarMoneda";

        } else {
            url = MetodosGlobales.ruta_service + "/General/ListarMoneda";
        }
        
        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
    
     public Result val_cuenta_destino(String codigo_cuenta, String token) {
        String url;
       
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/cuenta/ValidarCuentaDestino/"+codigo_cuenta;
        } else {
            url = MetodosGlobales.ruta_service + "/cuenta/ValidarCuentaDestino/"+codigo_cuenta;
        }
        
        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
     
      public Result constancia_cuenta_tercero_destino_detalle(
                String codigo_cuenta_origen, 
                String codigo_cuenta_destino,
                String monto_operacion, 
                String estado_operacion_frecuente,
                String alias_operacion, 
                
                String tipo_afiliacion,
                String numero_tarjeta,
                String codigo_cliente,
                String clave_dinamica,
                String codigo_solicitud,
                String clave_cordenada,
                String numero_columna,
                String numero_fila,
                
                String ip_publica_movil,
                String codigo_sub_menu,
   
                String header,
                String autenticacion_frecuente
      ) {
        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/transferencia/RegistrarTransferenciasTerceros";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();
        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoCuentaDestino", codigo_cuenta_destino)
                .addObject("montoOperacion", monto_operacion)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

        datosValidacion.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

      datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)   
                .addObject("codigoSubMenu", codigo_sub_menu);

        
        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
             jogeneral.addObject("data", jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion)).addObject("url", "/transferencia/RegistrarTransferenciasTerceros");

            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
      
       public Result clave_dinamica(String numero_tarjeta,String codigo_sub_menu,String monto_operacion, String header) {
        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/ClaveDinamica/RetornarCodigoOperacion";
        }
        
        int tipoMensaje = 1;
        String mensaje ;
      
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoSubMenu", codigo_sub_menu)
                .addObject("montoOperacion", monto_operacion);

        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/ClaveDinamica/RetornarCodigoOperacion");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }  
     
    
}
