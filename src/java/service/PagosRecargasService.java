package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class PagosRecargasService {

    public Result registrar_recarga(
            String codigo_cuenta_origen,
            String codigo_operador,
            String descripcion_operador,
            String numero_celular,
            String codigo_moneda,
            String monto_recarga,
            String estado_operacion_frecuente,
            String alias_operacion,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header,
            String codigo_solicitud,
            String autenticacion_frecuente) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Recarga/RegistrarRecargaMovil";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();

        datosValidacion.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoOperador", codigo_operador)
                .addObject("descripcionOperador", descripcion_operador)
                .addObject("numeroCelular", numero_celular)
                .addObject("codigoMoneda", codigo_moneda)
                .addObject("montoRecarga", monto_recarga)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria)).addObject("url", "/Recarga/RegistrarRecargaMovil");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);

        } else {
            jo.addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }

        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result listar_operadores() {

        String url;
        if (MetodosGlobales.flag_service) {
        
            url = MetodosGlobales.ruta_service_get + "/Recarga/ListarOperadores";
        } else {
            url = MetodosGlobales.ruta_service + "/Recarga/ListarOperadores";
        }

        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
}
