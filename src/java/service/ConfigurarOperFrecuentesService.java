package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class ConfigurarOperFrecuentesService {

    public Result retornar_estado_frecuente(String numero_tarjeta, String token) {
        String url;
       
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/Frecuente/RetornarEstadoFrecuente/"  + numero_tarjeta;
        } else {
            url = MetodosGlobales.ruta_service + "/Frecuente/RetornarEstadoFrecuente/"  + numero_tarjeta;
        }
    
        int tipoMensaje = 1;

        String mensaje;
        System.out.println(token);
        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result registrar_estado(
            String numero_tarjeta,
            String estado_frecuente,
            String tipo_afiliacion,
            String codigo_cliente,
            String clave_dinamica,
            String codigo_solicitud,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header,
            String autenticacion_frecuente
    ) {
        
   
        String url;
        
        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/Frecuente/RegistrarEstadoFrecuente";
        }
        
        
        
        int tipoMensaje = 1;
        String mensaje;
        
      
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("estadoFrecuente", estado_frecuente);

        JsonObjectWaka datosAuditoria = new JsonObjectWaka();
        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        JsonObjectWaka datosValidacion = new JsonObjectWaka();

        datosValidacion
                .addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);


        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion)).addObject("url", "/Frecuente/RegistrarEstadoFrecuente");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
       
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
