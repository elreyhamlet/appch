package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.MetodosGlobales;
import util.Result;

public class PreguntasFrecuentesService {

    public Result listar_oper_frecuentes() {

        String url;
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/General/RetornarPreguntasFrecuentes";
        } else {
            url = MetodosGlobales.ruta_service + "/General/RetornarPreguntasFrecuentes";
        }
        int tipoMensaje = 1;
        String mensaje;
        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_redes_sociales() {
        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/General/RetornarRedesSociales";
        } else {
            url = MetodosGlobales.ruta_service + "/General/RetornarRedesSociales";
        }
        int tipoMensaje = 1;
        String mensaje;
        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
}
