package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class HuellaDigitalService {

    public Result registrar_estado_huella_digital(String numero_tarjeta, String estado_huella_digital, String huella_cifrada ,String ip_publica_movil, String codigo_sub_menu, String header) {
        String url ;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Opcion/RegistarEstadoHuellaDigital";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("estadoHuellaDigital", estado_huella_digital)
                .addObject("huellaCifrada", huella_cifrada);

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

      

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
             jogeneral.addObject("data", jo.addObject("datosAuditoria", datosAuditoria)).addObject("url", "/Opcion/RegistarEstadoHuellaDigital");
            
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", datosAuditoria);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result retornar_estado_huella(String numero_tarjeta, String token) {
        String url ;
       
        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get +  "/Opcion/RetornarEstadoHuellaDigital/"+numero_tarjeta;
        } else {
            url = MetodosGlobales.ruta_service + "/Opcion/RetornarEstadoHuellaDigital/"+numero_tarjeta;
        }
        
        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
    
    public Result retornar_terminos() {
    String url;
        
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_get + "/General/RetornarTerminosCondiciones";

        } else {
            url = MetodosGlobales.ruta_service + "/General/RetornarTerminosCondiciones";
        }
        
        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
    
}
