package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class LoginService {

    public Result bloqueoTarjeta(
            String numero_tarjeta,
            String clave_pin,
            String ippublicamovil,
            String codigo_tarjeta,
            String documento_cliente,
            String codigo_descripcion_bloqueo,
            String descripcion_bloqueo
    ) {
        String url;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/RegistrarBloqueoTarjeta";
        }

        JsonObjectWaka jo = new JsonObjectWaka();
        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("clavePin", clave_pin)
                .addObject("ippublicamovil", ippublicamovil)
                .addObject("codigoTarjeta", codigo_tarjeta)
                .addObject("documentoCliente", documento_cliente)
                .addObject("codigoDescripcionBloqueo", codigo_descripcion_bloqueo)
                .addObject("descripcionBloqueo", descripcion_bloqueo);


        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Tarjeta/RegistrarBloqueoTarjeta");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }

        Result resultadoFinal;
        if (result[0].equals("0")) {
            resultadoFinal = new Result(new JsonParser().parse(result[1]).getAsJsonObject(), 0, "ok");
        } else {
            resultadoFinal = new Result(null, 1, result[1]);
        }

        return resultadoFinal;
    }

    public Result Login_autenticar(
            String codigo_tarjeta,
            String numero_tarjeta,
            String clave_web,
            String ippublicamovil,
            String codigo_instalacion,
            String estado_recordar,
            String estado_huella_digital,
            String huella_cifrada
    ) {
        String url;

        if (MetodosGlobales.flag_service) {
            
             url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/Login/ValidarAutenticacionLogin";
        }


        JsonObjectWaka jo = new JsonObjectWaka();
        jo.addObject("codigoTarjeta", codigo_tarjeta)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("claveWeb", clave_web)
                .addObject("ippublicamovil", ippublicamovil)
                .addObject("codigoInstalacion", codigo_instalacion)
                .addObject("estadoHuellaDigital", estado_huella_digital)
                .addObject("estadoRecordar", estado_recordar)
                .addObject("huellaCifrada",  huella_cifrada);

        int tipoMensaje = 1;
        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Login/ValidarAutenticacionLogin");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }


        JsonElement contenido = null;
        String mensaje = "";
        if (result[0].equals("0")) {
            JsonParser parser = new JsonParser();
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }

        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
