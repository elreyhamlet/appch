package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class RetornoDatosGobales {

    public Result listar_datos_globales() {
        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/General/RetornarVariablesGlobales";
        } else {
            url = MetodosGlobales.ruta_service + "/General/RetornarVariablesGlobales";
        }

        int tipoMensaje = 1;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        String mensaje = "";
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonParser parser = new JsonParser();
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }

        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result retornar_numeroTarjeta(
            String codigo_tarjeta,
            String codigo_instalacion
    ) {
        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/RetornarNumeroTarjeta";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        jo.addObject("codigoTarjeta", codigo_tarjeta)
        .addObject("codigoInstalacion", codigo_instalacion);

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Tarjeta/RetornarNumeroTarjeta");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }

        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }
    
        public Result listar_tipo_tarjeta() {

        String url;

        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/Tarjeta/ListarTipoTarjeta";
        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/ListarTipoTarjeta";
        }

        int tipoMensaje = 1;
        String mensaje;
         String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
}
