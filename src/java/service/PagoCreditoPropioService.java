package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class PagoCreditoPropioService {

    public Result listarCuotas(String numero_tarjeta, String cuenta_credito_destino, String header) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Creditos/ListarCuotasPendientes";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("cuentaCreditoDestino", cuenta_credito_destino);
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Creditos/ListarCuotasPendientes");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result listarDistribucionCuotas(String numero_tarjeta, String cuenta_credito_destino, String monto_pago_credito, String header) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Creditos/ListarDistribucionCuotas";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("cuentaCreditoDestino", cuenta_credito_destino)
                .addObject("montoPagoCredito", monto_pago_credito);
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Creditos/ListarDistribucionCuotas");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result registrar_pago_credito_propio(
            String numero_tarjeta,
            String codigo_cliente,
            String cuenta_origen_ahorro,
            String cuenta_credito_destino,
            String monto_pago_credito,
            String estado_operacion_frecuente,
            String alias_operacion,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header
    ) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Creditos/RegistrarPagoCredito";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka auditoria = new JsonObjectWaka();

        auditoria.addObject("ipPublicaMovil", ip_publica_movil).addObject("codigoSubMenu", codigo_sub_menu);

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("cuentaOrigenAhorro", cuenta_origen_ahorro)
                .addObject("cuentaCreditoDestino", cuenta_credito_destino)
                .addObject("montoPagoCredito", monto_pago_credito)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosAuditoria", auditoria)).addObject("url", "/Creditos/RegistrarPagoCredito");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", auditoria);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
