<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Caja Huancayo</title>

        <jsp:include page="web/glb/estilos.jsp" />

        <style>

            .titulo_slider{
                color:#666666;
            }
            .subtitulo_slider{
                color:#808080;
            }
            .carousel-caption{
                color:black;
                position:static;
                padding-bottom: 40px;
                text-shadow:none;
            }

            .progress {
                height: 30px;
                background-color: white;
            }
            .progress > svg {
                height: 100%;
                display: block;
            }

            .item img{

                max-height: 300px;
                width:auto;

            }

        </style>

        <!--AppCH-->
    </head>
    <body>

        <div class="row" style="margin: 50px 8px;">

            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>


                    <div class="carousel-inner" role="listbox" style="text-align:center">
                        <div class="item active">
                            <img src="img/Beneficios-min.jpg">
                            <div class="carousel-caption">
                                <p class="text-center">
                                    <span class="tam1 titulo_slider">Bienvenido a Banca Móvil Caja Huancayo  </span>
                                    <br>
                                    <span class="tam2 subtitulo_slider">ingresa a <button style="background: none;color: inherit;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit;" id="url-externo">www.misbeneficioscajahuancayo.pe</button> y descubre lo que tenemos para ti. </span>
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/Safety-min.jpg">
                            <div class="carousel-caption">
                                <p class="text-center">
                                    <span class="tam1 titulo_slider">Bienvenido a Banca Móvil Caja Huancayo </span>
                                    <br>
                                    <span class="tam2 subtitulo_slider">Realiza tus compras Online de manera segura con safetyPay</span>
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/Pishi-min.jpg">
                            <div class="carousel-caption">
                                <p class="text-center">
                                    <span class="tam1 titulo_slider">Bienvenido a Banca Móvil Caja Huancayo </span>
                                    <br>
                                    <span class="tam2 subtitulo_slider">Siempre sigue este consejo de seguridad.</span>
                                </p>
                            </div>
                        </div>


                    </div>

                </div>
            </div>






            <div class="col-xs-12  col-sm-10 col-sm-offset-1  text-center" >
                <h1 id="xx"></h1>
                <a id="btn1" class="btn btn-block btn-caja tam1 cambiar_pagina" style="border-radius: 20px">Siguiente</a>
            </div>


        </div>

        <div class="modal fade" id="modal-ch">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">
                        <button style="opacity: .7" type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span style="font-weight:700;color:black;">X</span></button>
                        <h4 class="modal-title" id="modal-title"></h4>
                    </div>

                    <div id="modal-body" class="modal-body">

                    </div>

                    <div class="modal-footer">

                    </div>

                </div>

            </div>



            <input type="hidden" id="hdd_mensaje_login" value="${mensaje}" />
        </div>



        <script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/jq/jquery.redirect.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/touchswipe.js" type="text/javascript"></script>
        <script src="js/controlador.js" type="text/javascript"></script>

        <script>

            $(function () {

                $(".carousel").swipe({

                    swipe: function (event, direction, distance, duration, fingercount, fingerdata) {

                        if (direction === 'left')
                            $(this).carousel('next');
                        if (direction === 'right')
                            $(this).carousel('prev');

                    },
                    allowPageScroll: "vertical"

                });
            });

            $("body").on("click", "#url-externo", function () {

                var link = $(this).text();

                if (check_so() === "iOS") {

                    set_LINK("http://" + link);

                } else if (check_so() === "Android") {

                    Android.url_externo("http://" + link);

                }

            });


            function message_req(titulo, cuerpo) {
                $("#modal-title").text(titulo);
                $("#modal-body").html(cuerpo);

                $("#modal-ch").modal();
            }


            $(document).ready(function () {


                $("#btn1").click(function () {


                    if (check_so() === "iOS") {
                        huella_condicion = FLAG_HUELLA_IOS;
                        get_UIDD();
                    } else if (check_so() === "Android") {

                        huella_condicion = undefined;
                        iniciar2(huella_condicion);
                    } else {
                        alert("ss");
                        huella_condicion = undefined;
                        iniciar2(huella_condicion);
                    }

                });


            });

            function iniciar(codigoInstalacion) {

                $.ajax({
                    url: 'Index',
                    type: 'post',
                    dataType: "text",
                    beforeSend: function () {

                    },
                    data: {
                        accion: 'iniciar_variables_globales',
                        codigoInstalacion: codigoInstalacion,
                        ip: IP_DISPOSITIVO_iOS ,
                        validador: huella_condicion
                    },
                    error: function () {
                        window.location.href = "${pageContext.request.contextPath}/";
                    },
                    success: function (salida) {

                        if (salida.length !== 0) {

                            var array = salida.split("%%%");
                            if (array[0] === "0") {

                                crearForm(array[1]);
                            } else {
                                message_req("Mensaje", array[1]);
                            }

                        } else {
                            message_req("Mensaje", "Error Inesperado");
                        }
                    }
                });

            }

            function iniciar2(validador) {
                alert("dd");
                // codigoInstalacion = Android.getInstanceID();
                codigoInstalacion = "100"
                //   ip = Android.get_ip_cell_phone();

                ip = "192.168.1.1";
                $.ajax({
                    url: 'Index',
                    type: 'post',
                    dataType: "text",
                    beforeSend: function () {

                    },
                    data: {
                        accion: 'iniciar_variables_globales',
                        codigoInstalacion: codigoInstalacion,
                        ip: ip,
                        validador: validador
                    },
                    error: function () {
                        window.location.href = "${pageContext.request.contextPath}/";
                    },

                    success: function (salida) {

//                        var form = document.createElement("form");
//                        form.setAttribute("method", "post");
//                        form.setAttribute("action", "cs?accion=prueba");
//
//                        document.body.appendChild(form);
//                        form.submit();
                        if (salida.length !== 0) {
                            var array = salida.split("%%%");
                            if (array[0] === "0") {
                                crearForm(array[1]);
                            } else {
                                message_req("Mensaje", array[1]);
                            }
                        } else {
                            message_req("Mensaje", "Error Inesperado");
                        }

                    }
                });

            }

        </script>

</html>
