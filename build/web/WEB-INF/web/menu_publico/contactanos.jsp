<html>
    <head>
        
        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="contactanos/head.jsp" />

    </head>
    <body>
        
        <%
            String id = (String) request.getSession().getAttribute("r_codigoCliente");
            String content = "";
            String cabecera = "";
            if (id != null) {// poner de donde viene
                content = "/web/glb/footer_login.jsp";
                cabecera = "/web/glb/header_login.jsp";
            } else {
                content = "/web/glb/footer_guest.jsp";
                cabecera = "";
            }
        %>
        
        <% if ( !cabecera.equals("") ) { %>
          <jsp:include page="/web/glb/header_login.jsp" flush="true"/>
        <% }%>
        
        
        
        <jsp:include page="contactanos/content.jsp" />
        
        <jsp:include page="<%=content%>" />
        
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="contactanos/script.jsp" />
        
    </body>
</html>
