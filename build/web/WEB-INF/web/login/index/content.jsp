<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row" style="margin: 8px;margin-top:20px;margin-bottom:100px">

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10px">

        <img src="${pageContext.request.contextPath}/img/logo1.png" alt="" style="max-width: 200px;width: 100%">

    </div>

    <div class="col-xs-12 col-md-12" style="margin-top: 20px">


        <p class="text-center titulo_login" >
            �Bienvenido!
            <img src="${pageContext.request.contextPath}/img/ekeko1.png" alt="ekeko" style="position: absolute;right: 0px;top:-30px;width: 60px">
        </p>
    </div>

    <div class="col-xs-12 col-md-12"  style="margin-top: 20px">

        <div class="col-xs-12">

            <div class="form-group has-feedback">

                <select id="codigo_tarjeta" class="form-control" placeholder="Seleccione tipo de tarjeta" onchange="js_listado()">
                    
                    <c:forEach var="list" items="${lista_datos_tarjeta}">
                        <option  value="${list[0]}%%%${list[2]}">${list[1]}</option>
                    </c:forEach>   
                </select>
                <input type="hidden" id="codigo_tarjeta_default" value="" />
                <input type="hidden" id="condicion_tarjeta_default" value="" />
                <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-select-gray.png">
            </div>

            <div class="form-group has-feedback">

                <input maxlength="16" type="tel" id="numero_tarjeta" class="form-control requerido validar-numero-16 solo-numeros" placeholder="N�mero de Tarjeta">
                <input type="hidden" id="return-numero-tarjeta" >
                
                <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-tarjeta-gray.png">
            </div>

            <div class="form-group">

                <input type="checkbox" id="recordar" placeholder="N�mero de Tarjeta" />
                Recordar

            </div>

            <div class="form-group has-feedback">

                <input type="password" id="numero_password" class="form-control keyboard requerido solo-numeros" placeholder="Clave Web" readonly="readonly"/>
                <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-candado-gray.png">
            </div>

            <div class="form-group ">

                <a href="${pageContext.request.contextPath}/web/login/olvido_clave.jsp" class="text-color-2 cambiar_pagina" style="text-decoration: underline">Olvidaste tu clave / Nuevo Cliente</a>

            </div>

        </div>

    </div>

    <div class="col-xs-12 col-md-12" style="margin-top:20px">
        
        <div id="huella" class="col-xs-3 col-md-2"> 

            <a type="iniciar" id="btn1" ><img id="finger" class="img-responsive" src="${pageContext.request.contextPath}/img/finger_login.png"> </a>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content modal-dialog-center">

                        <div class="modal-body">

                            <p  align="center"><img class="img-responsive" src="${pageContext.request.contextPath}/img/fingerprint-timer.png"></p>
                            <p align="center">Touch ID for "Caja Huancayo".</p>
                            <p align="center">Confirma tu Huella Digital</p>


                        </div>

                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-caja btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>






        </div>
        <div id="complemento_huella" class="col-xs-9 col-md-10"> 

            <button id="ingresar_ch" onclick="ingresar();" class="btn btn-block btn-caja tam1 cambiar_pagina" style="border-radius: 20px">Ingresar</button>
        </div>



    </div>

</div>
