<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo">

    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center">Tipo de Cambio: Compra <span>${tipo_cambio_compra}</span> Venta <span>${tipo_cambio_venta}</span></p>

    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 8px; margin-bottom: 0px">

            <p style="font-size: 20px;text-align: center;margin-bottom: 0px">
                <img class="pull-left" src="${pageContext.request.contextPath}/img/i-alcancia.png" style="width: 40px;vertical-align: middle" >
                Cuenta de Ahorro
            </p>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">


        <c:forEach  var="lista" items="${lista_ahorro_cta}">
            <a class="cambiar_pagina" onclick="js_detalle_cuenta('${lista[5]}', '${lista[1]}');" style="color: black">
                <div class="col-xs-12 lista-caja">

                    <span style="font-size: 0.6em;font-weight:600">${lista[1]} : <span>${lista[0]}</span></span> 
                    <span class="pull-right" style="display: inline-block;line-height: 24px;font-size: 0.8em;font-weight:600">${lista[2]}<span class="monto_formato" style="font-weight:600"> ${lista[3]}</span><span class="glyphicon glyphicon-play text-color-1" aria-hidden="true"></span></span>
                </div>
            </a>
        </c:forEach>

    </div>


    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 8px; margin-bottom: 0px">

            <p style="font-size: 20px;text-align: center;margin-bottom: 0px">
                <img class="pull-left" src="${pageContext.request.contextPath}/img/i-tarjeta-check.png" style="width: 40px;vertical-align: middle"> 
                Cuenta de Cr�dito
            </p>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${lista_credito_cta}">
            <a class="cambiar_pagina" onclick="js_detalle_cuenta_credito('${lista[5]}', '${lista[1]}');" style="color: black">
                <div class="col-xs-12 lista-caja" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
                    <span style="font-size: 0.6em;font-weight:600">${lista[1]} : <span>${lista[0]}</span></span>                                                             
                    <span class="pull-right" style="display: inline-block;line-height: 24px;font-size: 0.8em;font-weight:600">${lista[2]} <span class="monto_formato" style="font-weight:600">${lista[3]}</span><span class="glyphicon glyphicon-play text-color-1" aria-hidden="true"></span></span>
                </div>
            </a>
        </c:forEach>


    </div>



</div>