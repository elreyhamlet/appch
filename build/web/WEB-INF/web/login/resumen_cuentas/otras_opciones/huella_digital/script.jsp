<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        //pintamos footer
        $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');



    });

    (function () {
        $(document).ready(function () {
            $('.onoffswitch-checkbox').on('change', function () {
                var isChecked = $(this).is(':checked');


                if (isChecked === true) {
                    document.getElementById("habilitar").value = "true";
                    //isChecked = false; 
                    $('#btn_confirmar').attr("class", "btn btn-caja btn-block");


                } else {
                    document.getElementById("habilitar").value = "false";
                    $('#btn_confirmar').attr("class", "btn btn-caja btn-block disabled");

                }

            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.myonoffswitch'), true);
        });

    })();


    function configurar_huella_digital() {

        var huella_estado = ${estado_huella[0]};


        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraHuellaDigitalServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'configura_huella',
                huella_estado: huella_estado


            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ir_configuraciones() {

        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "configuraciones"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>