<div class="row cuerpo">

    <%session.setAttribute("origen", "mas");%>

    <div class="col-xs-12 col-md-12">

        <div class="form-group text-center">

            <span class="titulo-negrita">Configura tu huella digital</span>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 text-center">

        <div class="form-group">

            <span class="negrita" id="valor_mensaje">${mensaje_estado} </span> 
            <span style="display:inline-block;height: 16px;">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                    <input type="hidden" id="habilitar" value="true">
                </div>
            </span>

        </div>

    </div>


    <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center" style="border-radius:10px;border:solid 1px #C0C7C8;padding:5px;margin-bottom:15px">

        <span> Esta funcionalidad permitira ingresar a tu APP de Caja Huancayo con tu huella digital sin uso de tu clave web de 06 d�gitos </span>

    </div>


    <div class="clearfix"></div>

    <div class="col-xs-6 col-md-6 ">

        <a onclick="ir_configuraciones();" class="btn btn-caja btn-block">Cancelar</a>

    </div>
    <div class="col-xs-6 col-md-6">

        <a onclick="configurar_huella_digital();" id="btn_confirmar" class="btn btn-caja btn-block disabled">Confirmar</a>

    </div>



</div>