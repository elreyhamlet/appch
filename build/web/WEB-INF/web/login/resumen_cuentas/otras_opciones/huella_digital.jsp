<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="huella_digital/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="huella_digital/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="huella_digital/script.jsp" />


    </body
</html>