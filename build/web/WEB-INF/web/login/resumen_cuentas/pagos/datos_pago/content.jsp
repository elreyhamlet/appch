<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita" style="font-size:18px">Datos de la tarjeta</p>

    </div>

    <div class="col-xs-12 text-center">

        <div class="form-group">

            <select name="" id="bancos" class="form-control">
                <c:forEach var="lista" items="${listabancos}">
                    <option value="${lista[0]}%%%${lista[2]}">${lista[1]}</option>
                </c:forEach>
            </select>

        </div>

    </div>

    <div class="col-xs-12 text-center">


        <div class="form-group">

            <input   id="numero_tarjeta" type="tel" class="form-control solo-numeros" placeholder="Ingrese n�mero de tarjeta">

        </div>

    </div>

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10px">

        <span class="negrita">Moneda</span><br>
        <div class="btn-group" role="group" >
            <button id="soles" type="button" class="btn btn-secondary elegir-moneda" > Soles </button>
            <button id="dolares" type="button" class="btn btn-secondary elegir-moneda" > Dolares </button>
        </div>
        <p class="text-center">Tipo de Cambio: Compra <span>${tipo_cambio_compra}</span> Venta <span>${tipo_cambio_venta}</span></p>

    </div>


    <div class="col-xs-12 text-center has-feedback">

        <span class="negrita">Monto</span> <br>

        <div class="input-group">
            <input id="monto" type="${type}" class="form-control solo-numeros"  placeholder="S/">
            <input type="hidden" id="tipo_mon_ori" value='${tipo_mon_ori}'>
            <div class="input-group-btn">   
                <button class="btn btn-borrar" type="submit">
                    <i style="color:#CC3333" class="fas fa-times-circle"></i>
                </button>
            </div>
        </div>

    </div>




    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a href="../pagos.jsp" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a  onclick="js_validar_pago_tarjeta();" class="btn btn-caja btn-block">Siguiente</a>
        </div>

    </div>




</div>