<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo">


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita" style="font-size:18px">Datos de Recarga</p>

    </div>

    <div class="col-xs-12 text-center">

        <div class="form-group">
 
            <select name="" id="operador" class="form-control">

                <c:forEach var="lista" items="${lista_operadores}">
                    <option value="${lista[0]}">${lista[1]}</option>
                </c:forEach>

            </select>

        </div>

    </div>

    <div class="col-xs-12 text-center">


        <div class="form-group">

            <input  type="tel" maxlength="9" id="celular" class="form-control solo-numeros" placeholder="Ingrese numero de celular">

        </div>

    </div>




    <div class="col-xs-12 text-center has-feedback">

        <span class="negrita">Ingrese el Monto</span> <br>

        <div class="input-group ">

            <input id="monto" type="${type}" class="form-control solo-numeros" placeholder="${moneda_celular}">
            
            <div class="input-group-btn">
                <button class="btn btn-borrar" type="submit">
                    <i style="color:#CC3333" class="fas fa-times-circle"></i>
                </button>
            </div>

        </div>

    </div>




    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a href="../pagos.jsp" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a onclick="confirmar();" class="btn btn-caja btn-block">Siguiente</a>
        </div>

    </div>




</div>