<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');


        var cel = "${inputs_AP10[6]}";

        if (cel.length !== 0) {
            $("#celular").val(cel.trim());
        }
    });

    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var operador = $("#operador").val();
        var celular = $("#celular").val();
        var monto = $("#monto").val();

        var flag = true;
        if (operador === undefined || operador === "0") {

            result += "<li>Ingrese operador</li>";
            flag = false;
        }

        if (celular.length === 0) {

            flag = false;
            result += "<li>Ingrese celular</li>";
        } else {
            if (celular.length !== 9) {
                flag = false;
                result += "<li>N�mero de celular debe ser de 9 d�gitos</li>";
            }
        }
        if (monto.length === 0) {

            flag = false;
            result += "<li>Ingrese monto</li>";
        } else {
            var reg = /^\d+(?:\.\d{1,2})?$/;
            if (!reg.test(monto)) {
                result += "<li>Ingrese monto correcto</li>";
                flag = false;
            }

        }

        result += "<ul>";

        array = [flag, result];

        return array;
    }
    function confirmar() {


        var array = validar_campos();
        if (array[0] === true) {
            confirmar_recarga();

        } else {

            message_req("Mensaje", array[1]);
        }
    }


    function confirmar_recarga() {

        var operador = $("#operador").val();
        var nombre_operador = $("#operador option:selected").text();
        var celular = $("#celular").val();
        var monto = $("#monto").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/PagosRecargasServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'ver_confirmacion',
                operador: operador,
                celular: celular,
                monto: monto,
                nombre_operador: nombre_operador
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }
</script>