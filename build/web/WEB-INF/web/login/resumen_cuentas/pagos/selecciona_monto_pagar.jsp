<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="selecciona_monto_pagar/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="selecciona_monto_pagar/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />
        
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="selecciona_monto_pagar/script.jsp" />


    </body
</html>