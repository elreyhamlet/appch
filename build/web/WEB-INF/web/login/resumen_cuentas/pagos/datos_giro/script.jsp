<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>

<script>

    $(function () {

        $("img#footer_pagos").attr('src','${pageContext.request.contextPath}/img/i-billetera.png');

        var dni = "${inputs_AP09[6]}";
        if (dni.length !== 0) {
            $("#dni").val(dni.trim());
        }

    });



    function validarCampos() {
        var apellido_paterno = $("#apellido_paterno").val();
        var apellido_materno = $("#apellido_materno").val();
        var nombres = $("#nombres").val();
        var dni = $("#dni").val();
        var telefono = $("#telefono").val();
        var agencia = $("#agencia").val();
        var monto = $("#monto").val();
        var result = [];
        var mensaje = "<ul>";
        var flag = true;
        if (apellido_paterno.length === 0) {
            mensaje += "<li>Ingrese apellido paterno</li>";
            flag = false;
        }
        if (apellido_materno.length === 0) {
            mensaje += "<li>Ingrese apellido materno</li>";
            flag = false;
        }
        if (nombres.length === 0) {
            mensaje += "<li>Ingrese nombres</li>";
            flag = false;
        }
        if (dni.length === 0) {
            mensaje += "<li>Ingrese DNI</li>";
            flag = false;
        }
        if (telefono.length === 0) {
            mensaje += "<li>Ingrese tel�fono</li>";
            flag = false;
        }
        if (agencia.length === 0) {
            mensaje += "<li>Ingrese agencia</li>";
            flag = false;
        }
        if (monto.length === 0) {
            mensaje += "<li>Ingrese monto</li>";
            flag = false;

        } else {
            var reg = /^\d+(?:\.\d{1,2})?$/;
            if (!reg.test(monto)) {
                mensaje += "<li>Ingrese monto correcto</li>";
                flag = false;
            }
        }

        mensaje += "</ul>";
        result = [flag, mensaje];
        return result;

    }
    function js_validar_giro() {

        var array = validarCampos();
        if (array[0] === true) {
            funcion_mostrar_constancia();
        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function funcion_mostrar_constancia() {

        var apellido_paterno = $("#apellido_paterno").val();
        var apellido_materno = $("#apellido_materno").val();
        var nombres = $("#nombres").val();
        var dni = $("#dni").val();
        var telefono = $("#telefono").val();
        var agencia = $("#agencia").val();
        var nombre_agencia = $("#agencia option:selected").text();
        var monto = $("#monto").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/EnvioGirosServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'ver_confirmacion',
                apellido_paterno: apellido_paterno,
                apellido_materno: apellido_materno,
                nombres: nombres,
                dni: dni,
                telefono: telefono,
                agencia: agencia,
                nombre_agencia: nombre_agencia,
                monto: monto
            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                        
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }




</script>