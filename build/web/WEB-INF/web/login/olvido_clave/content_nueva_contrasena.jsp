<div class="row" style="margin: 20px 8px;">

    <div class="col-xs-12 col-md-12 text-center" style="margin-top: 10%">

        <img src="${pageContext.request.contextPath}/img/i-tarjeta-configurar.png" alt="" style="max-width: 100px;width: 100%">

    </div>

    <div class="col-xs-12 col-md-12" style="margin-top: 20px">
        <p class="text-center" style="font-weight: 300;font-size: 20px">
            Modifica tu contrase�a
        </p>
    </div>

        
    <div class="col-xs-12 col-md-12"  style="margin-top: 20px">

        <div class="form-group has-feedback">

            <input type="text" class="form-control keyboard validar-input" placeholder="Ingresa una nueva clave de 6 d�gitos" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-candado.png">

        </div>

        <div class="form-group has-feedback">

            <input type="text" class="form-control keyboard2 validar-input" placeholder="Confirma tu clave" />
            <img class="form-control-feedback img-icon" src="${pageContext.request.contextPath}/img/i-check.png">


        </div>

    </div>
            

    <div class="col-xs-12 col-md-12  text-center" style="margin-top:60px">

        <button id="continuar" class="btn btn-block btn-caja tam1 cambiar_pagina" style="border-radius: 20px">Continuar</button>

    </div>


</div>

<jsp:include page="/web/glb/script_general.jsp" />

<script> 

$(function(){
    $(".keyboard").numKey({
        limit: 4,
        disorder: true
    });

    $(".keyboard").click(function () {

        $(".keyboard").numKey({
            limit: 4,
            disorder: true
        });

    });
    
    $(".keyboard2").numKey({
        limit: 4,
        disorder: true
    });

    $(".keyboard2").click(function () {

        $(".keyboard2").numKey({
            limit: 4,
            disorder: true
        });

    });
    
    $(".validar-input").on("change keyup",function(){
        
        
    
        var value = $('.validar-input').filter(function () {
            return this.value === '';
        });
        
        if (value.length === 0) {
            
            if($(".keyboard").val() === $(".keyboard2").val()){
                $("#continuar").attr('disabled',false);
            }
            else{
                $("#continuar").attr('disabled',true);
            }
            
            
            
        } else if (value.length > 0) {
            
            console.log($(".keyboard").val());
            
            $("#continuar").attr('disabled',true);
            
        }
        
    });
    
});

</script>

