<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
            <span class="text-center negrita titulo"> Pago de cr�dito propio:</span><br/>
            <span  id="titulo"class="negrita">Selecciona la cuenta de cr�dito</span>

        </div>

    </div>



    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">
        <c:forEach  var="lista" items="${pago_propio_credito}">

            <a onclick="elegir_cuenta_cargo('${lista[5]}');" style="color: black">

                <div class="col-xs-12 lista-caja" >

                    <span  class="negrita" style="font-size: 15px">${lista[1]} : <span  class="negrita">${lista[0]}</span></span> 
                    <span class="negrita pull-right" style="display: inline-block;">${lista[2]} <span class="formato">${lista[3]}</span><span class="glyphicon glyphicon-play text-color-1" aria-hidden="true"></span></span>

                </div>

            </a>

        </c:forEach>

    </div>




</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>



    $(function () {
        $(".formato").number(true, 2);

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
    });

    function elegir_cuenta_cargo(indicador_cuenta) {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: '${accion}',
                indicador_cuenta: indicador_cuenta


            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {

                         crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }


</script>