<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="elegir_cuota_monto/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("/elegir_cuota_monto/content_pago_servicio.jsp") == 0) {// poner de donde viene
                content = "elegir_cuota_monto/content_pago_servicio.jsp";
            } else if (referrer.compareTo("PagoCreditoPropioServlet") == 0) {
                content = "elegir_cuota_monto/content_pago_creditos_propios.jsp";

            }
            else if (referrer.compareTo("PagoCreditoTerceroServlet") == 0) {
                content = "elegir_cuota_monto/content_pago_creditos_terceros.jsp";

            }
            else if (referrer.compareTo("PagoInstitucionesServlet") == 0) {
                content = "elegir_cuota_monto/content_pago_instituciones.jsp";

            }
            
            else {
                content = "elegir_cuota_monto/content_pago_servicio.jsp";
            }
        %>
        <jsp:include page="<%=content%>" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="elegir_cuota_monto/script.jsp" />


    </body
</html>