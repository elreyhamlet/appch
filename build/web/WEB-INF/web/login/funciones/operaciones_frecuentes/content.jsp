<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 " style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <span>Tiene ${cantidad_oper} operaciones frecuentes </span>
            <span class="pull-right">
                <button class="btn btn-caja" id="btn-editar">Editar</button>
            </span>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">
        <c:forEach var="lista" items="${opera_frecuentes}">

            <div class="input-group">

                <a class="frecuentes_edicion" onclick="ir_operacion_frecuente('${lista[0]}');">


                    <div class=" col-xs-12 pregunta_frecuente redirigir" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

                        <span>Alias: <span>${lista[8]}</span> </span>
                        <br>
                        <span>${lista[6]}</span>
                    </div>

                </a> 



                <div class="frecuentes_borrar col-xs-12 pregunta_frecuente redirigir" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080;display:none">

                    <span>Alias: <span>${lista[8]}</span> </span>
                    <br>
                    <span>${lista[6]}</span>
                </div>



                <div class="input-group-btn" style="padding:0px 20px;border-bottom: 1px solid #808080">

                    <span id="xxx"  class="pull-right editar_campo" style="line-height:40px">



                        <input name="radioButton" id="ids__${lista[0]}" style="display: none;" class="operaciones" type="checkbox" >




                        <a onclick="ir_operacion_frecuente('${lista[0]}');" class="imagen_show">

                            <img class="imagen_show" style="width: 15px" src="${pageContext.request.contextPath}/img/i-right.png">

                        </a>

                    </span>



                </div>

            </div>

        </c:forEach>
    </div>

    <div class="col-xs-12 text-center">
        <button onclick="borrar_oper_frec();" class="btn btn-caja borrar oculto"><img class="pull-left" src="${pageContext.request.contextPath}/img/i-basura-b.png" style="max-height:25px;vertical-align: middle" ></button>
    </div>

</div>